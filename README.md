# SRobot: A Versatile Educational Robotic Platform

![image](LaTeX/Files/Images/Robot_comparison/SRobot/1.png){width=60%}

<br/><br/>

This repository contains all the resources related to the master's thesis project - **Design and Construction of an Omnidirectional Base for a Student Robotics Platform**. The project aims to provide a versatile and affordable educational platform for university students to learn about robotics from both hardware and software perspectives.

## Overview

The SRobot is an indoor mobile robotic platform designed specifically for educational purposes. It features a modular design, extended operational lifetime, and a capacitive tactile bumper that grants the platform true omnidirectional capabilities. The use of BLDC motors ensures enhanced precision, smoother movement, and energy efficiency. The platform's affordability, with a significant portion of the cost attributed to the Intel NUC and RPLidar A3 components, makes it an accessible tool for educational institutions.

## Video Demonstration

![](SRobot.mp4){width=60%}

## Key Features
- Modular design for easy assembly and customization.
- Extended operational lifetime for prolonged use.
- Capacitive tactile bumper for true omnidirectional capabilities.
- Use of BLDC motors for enhanced precision and energy efficiency.
- Affordable construction, with a significant portion of the cost attributed to Intel NUC and RPLidar A3 components.

![](Assembly_top.gif){width=49%}
![](Assembly_side.gif){width=49%}

## Folder Structure

The repository contains all the files and resources related to the master's thesis. The files are organized into the following folders:

- **3D models**: Contains 3D models and CAD designs of the omniwheel robot platform.
- **Altium**: Contains PCB design files and schematics for the robot's electronics.
- **Firmware**: Contains firmware code for the microcontroller that controls the robot's BLDC motors.
- **LaTeX**: Contains LaTeX files for the thesis document.
- **Lesson plan**: Contains the lesson plan designed for bachelor students, explaining a basic concept of mobile robotics using the omniwheel robot platform.
- **Software**: Contains software code for the high-level control of the robot, based on the ROS (Robot Operating System) framework.

## Master's thesis abstract

The SRobot is a comprehensive indoor mobile robotic platform, meticulously designed as an educational tool for university students, covering fundamental robotics concepts encompassing both hardware and software aspects.

Distinct advantages of the SRobot include its modular design, extended operational lifetime, and innovative capacitive tactile bumper that grants true omnidirectional capabilities. The integration of BLDC motors ensures precision, smoother movement, and energy efficiency. Remarkably cost-effective, the SRobot stands out among comparable options.
	
The construction process is meticulously detailed, providing a replicable guide, including component selection, functionality, and battery management. The PCB design, integrated components, and electronics are thoroughly described, offering a clear assembly blueprint.
	
The firmware section offers insights into STM32 microcontrollers, code upload instructions, robot connection diagrams, BLDC motor control, PID controllers, various interrupts provided by ISR, communication protocols, and protective features. The platform's flexibility with connectors for diverse modules is highlighted.
	
The software section explores ROS packages, topics, messages, the startup sequence, robot model creation, odometry implementation, Gazebo simulation, and RVIZ application, providing a comprehensive understanding of the robot's functionality.
	
The platform's educational suitability is emphasized, showcasing a range of use cases, a dedicated robotics curriculum, and example lessons covering firmware, SLAM algorithms, and more, making the SRobot an ideal educational platform.
	
The conclusion points out the SRobot to be an exemplary indoor mobile robotic platform that effectively fulfills its master's thesis assignment. Its modular design, hardware capabilities, sophisticated firmware, and versatile software make it a valuable educational tool, ready to teach essential robotics concepts to university students.

## Keywords

**SRobot, Indoor mobile robotic platform, Omnidirectional robot, Modular design, Capacitive tactile bumper, BLDC motors, STM32, Field-oriented control, Interrupt service routine, Touch Sensing Controller, LiDAR, ROS packages, Gazebo simulation, SLAM algorithms, Educational robotic platform, Kinematics and dynamics of omnidirectional robots**

## How to Use

To replicate or build upon the SRobot platform, refer to the [Master's thesis document](LaTeX/Masters_thesis.pdf) for detailed instructions on design, construction, firmware development, and software implementation. The provided CAD models, PCB designs, firmware, and software codes are available under BSD and MIT licenses. The PCB design falls under the Creative Commons license.

To utilize this repository effectively, follow these steps:

1. Clone the repository to your local machine using the following command:

```
git clone https://gitlab.com/WenceslawCZ/design-and-construction-of-an-omnidirectional-base-for-student-robotics-platform.git
```

2. Navigate to the relevant directories for accessing the CAD models, PCB files, Hardware, Software or Lesson plan.

3. Modify or enhance the files as needed for your specific use case. Ensure to document any changes made.


## Future Improvements

The project acknowledges some imperfections in the current design and suggests potential enhancements. These include refining battery charging and balancing, optimizing STM32 microcontroller functionalities, and incorporating additional features like an inertial measurement unit.


## Contribution Guidelines

Contributions to this repository are welcome. If you would like to contribute, please follow these guidelines:

1. Fork the repository to your GitLab account.

2. Create a new branch for your feature or bug fix:

```
git checkout -b feature/your-feature
```

3. Make your modifications and commit your changes with descriptive commit messages.

4. Push your branch to your forked repository.

5. Submit a pull request detailing the changes you have made.

Please note that all contributions are subject to review and approval.

## Contact Information

For any inquiries or further information, please contact:

Ing. Václav Veselý
Email: vaclav@vesely.pro


## Acknowledgements
Special thanks to the project supervisor, **Mgr. Martin Pecka, Ph.D.**, and Supervisor-specialist **Ing. Bedřich Himmel**, for their invaluable guidance and expertise throughout the project.
