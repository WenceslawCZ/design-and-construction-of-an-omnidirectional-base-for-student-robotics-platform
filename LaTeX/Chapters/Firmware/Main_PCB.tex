\chapter{Main board}

\begin{figure}
    \centering
    \includegraphics[width=1.0\linewidth]{Files/Images/Firmware/Main_board_pinout.png}
    \caption{Main board pinout}
    \label{fig:main_board_pinout}
\end{figure}


\section{Power managment} \label{firmware_main_board_power_managment_section}

The robot's ability to measure the current consumption of the Intel NUC provides valuable information about the NUC's power state. This feature allows the robot to detect whether the NUC is turned on, off, or in sleep mode, enhancing its overall control and autonomy.

With this knowledge, the robot can take various actions based on the NUC's power state. For example, if the NUC is turned off or in sleep mode, the robot may initiate a shutdown sequence to conserve power and prevent unnecessary operation. On the other hand, if the robot detects some unexpected movement or external stimuli through its sensors, it can wake up the NUC to respond to the situation appropriately.

By effectively monitoring the NUC's power consumption and incorporating it into the decision-making process, the robot gains the ability to respond to changing conditions and improve its efficiency and responsiveness. This feature enhances the robot's overall intelligence and adaptability, making it a more sophisticated and capable platform for various tasks and applications.

\subsection{Momentary switch} \label{firmware_momentary_switch_subsection}

The momentary switch on the robot serves as the primary control for turning the robot on and off. However, it is designed to prevent unintended shutdowns and provides different functions depending on how long it is pressed. 

To turn the robot on, the momentary switch requires the battery voltage to exceed 12.4 volts as mentioned in the subsection \ref{main_board_power_managmnet_subsection}. Once this condition is met, the microcontroller on the Main board takes control and holds the robot turned on until the user initiates the power-off sequence or until the battery voltage drops below 12 volts.

To turn the robot off, the same momentary switch offers three different states to prevent unintended shutdowns. Each state corresponds to a different duration of pressing the switch. When the switch is pressed briefly, the robot initiates the reset sequence. In this state, the Intel NUC will terminate all existing nodes and reset the \gls{ROS} with all the starting nodes.

If the switch is pressed for a longer duration, between one and three seconds, the robot will attempt to turn off the Intel NUC using the front panel connector. It will wait for the NUC to completely turn off by monitoring the consumed current. Once the current of the Intel NUC drops, the robot will proceed to turn itself off. If the NUC does not turn off within two minutes, the robot will automatically turn off itself.

The last state of the momentary switch occurs when it is pressed for more than three seconds. In this case, the robot will directly turn itself off without waiting for the NUC to shut down. To provide feedback to the user, the momentary switch will indicate the duration of pressing through colored signalization and the sound produced by the robot. This multi-state functionality of the momentary switch provides a user-friendly and intuitive way to control the robot's power status.

\subsection{Front panel connector}

As mentioned in section \ref{main_board_digital_control_subsection}, the robot is connected to the Intel NUC front panel connector. This enables the robot to monitor the robot’s power status and provide the robot with the ability to turn the NUC on and off as needed. When the momentary switch is pressed, the robot uses this connection to turn the Intel NUC on. 

To monitor the power status of the Intel NUC, the robot reads the power status LED of the NUC. If the front panel connector is properly connected to the NUC, the robot can directly check the power status from the LED. This method provides an accurate and reliable way to determine whether the Intel NUC is turned on or off.

However, in situations where the front panel connector is not connected to the NUC, the robot needs an alternative method to detect the NUC's power status. In such cases, the robot utilizes the current consumption readings from the XT60 connector mentioned in section \ref{body_upper_part_subsection}. By monitoring the current flow through this connector, the robot can determine if the Intel NUC or any other electronics connected to it are powered on, and only in such cases it will initiate the startup process.

\section{Analog data processing}

The STM32 on board measures the current and voltage values every 10 microseconds. The temperature measurements are done every 100 microseconds. All these measurements are performed by a circular \gls{DMA} which is started by an external trigger, in this case, a timer. This ensures efficient and continuous data acquisition without putting additional strain on the microcontroller's resources.

Once the measurements are completed, the \gls{ADC} conversion complete callback is triggered, allowing the STM32 to further process the measured data. During this callback, the raw data are converted into meaningful representations of current, voltage, and temperature values.

For the capacitive tactile sensors, which cannot be measured using \gls{DMA}, the STM32 uses an \gls{ISR} for data acquisition. The measurement of capacitive sensors is initialized at the start of the robot. After the measurement is finished, the \gls{TSC}  conversion complete callback occurs. In this callback, the measured raw data from the capacitive sensors are processed and evaluated to determine if the sensors are pressed or not. Since the capacitive sensors work by detecting changes in capacitance when pressed, an applied force to the sensors can be directly measured. After processing the data, the measurement readings are initialized again, and the whole process repeats. 

To ensure accurate and stable data, a low-pass filter is applied to the measured values. The low-pass filter is designed to smooth out noise while maintaining responsiveness to changes. This filtering technique is essential for obtaining reliable and steady readings, especially in scenarios where noise or fluctuations in measurements may occur \cite{cite:low_pass_filter}.

By implementing these techniques, the STM32 can efficiently and accurately monitor current consumption, robot voltages, temperatures, and capacitive tactile sensor inputs. This information is crucial for the robot to make informed decisions and respond effectively to its surroundings and user interactions. The combination of these features contributes to the robot's overall performance and enhances its ability to operate reliably and safely in various situations.

\section{Communication}

\subsection{Driver boards}

The communication between the Main board and the Driver boards is established through \gls{SPI}. Each Driver board is connected to a dedicated \gls{SPI} channel on the Main board. In this setup, the Main board acts as the master, and the Driver boards act as slaves. The communication occurs only when the Main board needs to read data from the Driver boards or change their velocity settings. The communication occurs in 12-byte packets and is described in more detail in section \ref{firmware_communication_section}.

To ensure that the Driver boards are operating correctly, the Main board actively monitors their status using \gls{ISR}. If something unexpected or erroneous happens with any of the Driver boards, the Main board can detect it through the respective \inlineCode{Mx_MCU_FAULT} pins. This allows the Main board to react to any potential issues promptly and take appropriate actions if needed.

Moreover, the Main board can synchronize velocity changes of the motors through a dedicated \inlineCode{SYNC} pin. This synchronization ensures that all motors change their velocity simultaneously, enhancing the robot's coordinated movement. Additionally, in case of any unforeseen circumstances or emergencies, the Main board can initiate synchronized braking of all motors using the \inlineCode{BRAKE_MOTORS} pin. This feature, fully described in section \ref{firmware_ISR_section}, allows the robot to halt quickly and safely in response to unexpected events.

By using \gls{SPI} communication, interrupts for monitoring Driver board status, and dedicated synchronization pins, the Main board maintains effective and reliable control over the motor drivers and the overall robot's motion. This level of control ensures precise and coordinated movements, enhanced safety, and efficient operation, making the robot a reliable and intelligent autonomous system.

\subsection{Intel NUC} \label{firmware_cummunication_intel_NUC_subsection}

The communication between the robot and the Intel NUC is facilitated through a USB-C port. The robot has full control over this USB connection and can handle any interruptions that may occur. If communication is interrupted, the robot can disconnect and reconnect the USB to re-establish communication with the NUC. This reconnection process prompts the NUC to restart its communication scripts, ensuring a reliable and consistent connection.

Since the USB communication is directly handled by the STM32 on the Main board without the need for any additional \gls{UART} to USB module, there is no requirement to set specific baud rates. The communication between the robot and the NUC occurs at the maximum USB bitrate of 12Mbps for Full-speed mode \cite{cite:USB}. Therefore, the set baud rate in any script will not affect the communication speed between the robot and the NUC.

To ensure effective and readable communication, the messages sent by the robot are in a specific format with the following structure "$M;x;y;z$". The "$M$" represents the type of message, and the values "$x;y;z$" represent the data sent in that message. Different types of messages are used to send various data, including current, voltage, temperature, capacitive tactile button status, motor position, motor velocity, and reset messages. Each message type contains a specific number of values and is sent at different frequencies. The frequency at which each message is sent can be adjusted to optimize the communication.

The messages coming from the Intel NUC are in a similar format "$M \; \; x;y;z$". The Intel NUC has significant control over the robot's functionalities and components. It can synchronize the stopping of all motors simultaneously or stop them individually. The NUC has full control over motor velocity, acceleration, and power for each motor, as well as the ability to disable the power to the Driver boards, effectively disconnecting the motors. It can set different \gls{PID} constant values for each motor to optimize its performance. Furthermore, the NUC has control over the robot's signaling elements, such as the buzzer frequency and the color of each LED located on the robot. The robot is equipped with 27 LEDs, three of them are on the Main board and the rest under the NUC in the diffuser. 

The communication between the robot and the NUC enables the NUC to initiate resets of various components, including the capacitive tactile buttons, motor positions, or encoder positions relative to the motors. The NUC has full control over the power state of the robot and can initiate its own reset or a complete shutdown sequence when necessary. This level of communication and control empowers the Intel NUC to effectively manage the robot's operations and adapt to different scenarios and tasks.

\section{Robot protection}

The Main board's direct control over the power of the Driver board provides an added layer of safety and protection for the robot. If the Main board detects increased current consumption, potentially caused by a stuck wheel or other issues with the motors, it can disconnect the DRV8311 motor driver from the power supply, preventing any further damage or overheating. This functionality can also be transferred to the Driver board itself since it shares the same \inlineCode{MOTOR_NSLEEP} pin, providing additional redundancy and reliability in motor control.

The robot's ability to measure the current coming from each battery and the power consumption of the Intel NUC offers valuable insights into overall power usage and system health. By continuously monitoring these parameters, the robot can detect anomalies or potential hazards, such as excessive power consumption similar to short circuits, that could lead to critical failures. In such situations, the robot can take precautionary actions, such as turning itself off, to prevent any damage to the batteries or other components.

Moreover, the robot's temperature measurements play a crucial role in its safety and protection mechanisms. By monitoring the temperature of each component, the robot can detect overheating issues. If the robot detects overheating of the Driver board or the motors, it can take preventive measures such as disconnecting the motors from power to allow them to cool down. In more extreme cases, if the internal temperature of the robot reaches unsafe levels, the robot can initiate a shutdown sequence to prevent any further heat-related issues.

Additionally, the robot continuously monitors the battery voltage to ensure it stays within safe operating limits. If the battery voltage drops below 12.3 volts, the robot will notify the user through LED signalization and sound alerts. This serves as a warning to the user that the battery is reaching a low state and may need recharging.

However, if the robot detects a critical voltage level lower than 12 volts, it will automatically initiate a turn-off sequence. During this sequence, the robot will utilize the front panel connector to safely shut down the Intel NUC before completely powering off. This mechanism ensures that the robot will not operate in a state where the battery voltage is too low, preventing potential damage to the batteries and to the robot itself.

By implementing these safety measures and intelligent control mechanisms, the robot can proactively respond to various situations and ensure the safety and reliability of its operations. These features provide an added level of protection and peace of mind when operating the robot in various environments and scenarios.

\section{Extensions}

The high flexibility and extensive range of connectors provided by the robot's Main board make it a highly versatile and customizable platform. With three unique \glspl{UART}, one \gls{I2C}, one \gls{CAN} bus, and three \glspl{SPI}, the robot can easily interface with various sensors and modules, allowing for seamless integration of new functionalities and expanding its capabilities. This adaptability enables users to tailor the robot to their specific needs and applications, making it suitable for a wide range of tasks and projects.

Additionally, the availability of a dedicated port for an ethernet \gls{SPI} module enhances the robot's communication capabilities. With pre-connected ethernet interrupts and ground and logic voltage pins, integrating an ethernet module becomes straightforward and efficient. This allows the robot to communicate over an Ethernet network, opening up possibilities for more sophisticated control and data exchange with other devices or systems.

Furthermore, the provision of a dedicated connector for a CH340E USB to Serial adapter offers a backup solution in case the USB-C connector on the robot becomes non-functional. This redundancy ensures that the robot can maintain its communication capabilities even in the face of potential hardware issues, enhancing its overall reliability and resilience.

Overall, the Main board's extensive connector options and flexibility in accepting various modules and sensors empower the robot with the adaptability needed to meet diverse requirements and applications in research, education, or any other field where a customizable robotic platform is advantageous.