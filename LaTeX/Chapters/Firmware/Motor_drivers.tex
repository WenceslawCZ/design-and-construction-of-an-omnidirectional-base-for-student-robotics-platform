\chapter{Driver board}

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{Files/Images/Firmware/Driver_board_pinout.png}
    \caption{Driver board pinout}
    \label{fig:driver_board_pinout}
\end{figure}

\section{Motor control}

The STM32 microcontroller on the Driver board is connected to the DRV8311 motor driver in a way that enables the control of both the high-side and low-side integrated MOSFETs of the half-H-bridges. This configuration results in a 6-channel \gls{PWM} control interface. The \gls{PWM} channels are controlled at a frequency of 32 kHz. Although the DRV8311 motor driver is capable of sensing the current of different phases of the motor and is customized to achieve optimum \gls{FOC} and current-control system implementation, these features were not fully implemented in the current setup. The DRV8311 driver measures the current on the low-side of the half-H-bridges. Therefore, the STM32 must measure the current only at the moment when the low-side MOSFET is on and the current flows through it. This type of synchronization may result in the integrated STM32 on the Driver board not being fast enough to measure the current at the right moment, leading to inaccuracies in current sensing.

As a result, the motor control is implemented using sinusoidal control with voltage torque control. This control method assumes that the torque, proportional to the set currents, is also proportional to the set voltage. While this assumption may not be entirely accurate, it is suitable for small current applications \cite{cite:velocity_torque_control}.

To address the current measurement issue and improve motor control accuracy and efficiency, several potential solutions may be available. One approach is to measure the current directly on the motor phases, which eliminates the need for synchronized measurements. Another option is to use a faster microcontroller or one with a higher number of \glspl{ADC} to improve the measurement timing. Alternatively, the control frequency can be decreased to increase the synchronization window and enhance accuracy. 

\section{Encoder} \label{firmware_encoder_section}

The motor is equipped with an integrated AS5048A 14-bit magnetic rotary encoder. This encoder provides the robot with precise and accurate absolute angle information. The encoder can output this angle data either through \gls{SPI} or by \gls{PWM}. While both options are available on the Driver board, the \gls{SPI} interface has proven to be faster and more reliable in practice. Therefore, the Driver board communicates with the magnetic encoder using the \gls{SPI} protocol, with a baud rate of 16 MBits per second. The microcontroller then uses a low-pass filter to process the raw data from the encoder and calculate the motor velocity. This process helps smooth out any noise or fluctuations in the encoder readings, resulting in more accurate velocity calculations. 

The encoder readings, along with the \gls{PID} controller for motor control, are performed in the main loop of the microcontroller. This implementation has proven to be the most effective and efficient solution for achieving precise motor control based on the encoder feedback.

\section{PID controllers}

The \gls{PID} controller used for the \gls{BLDC} motor differs from that used for DC motors. This difference arises because the velocity of the \gls{BLDC} motor is not solely dependent on the applied voltage, but also on its relative position. In the \gls{BLDC} motor, the magnetic force applied to the rotor should have a 90-degree offset from its permanent magnetic field to ensure maximum torque.

As a result, the feedback loop for the \gls{PID} controller is not run periodically by a timer. Instead, it runs each time the encoder reads new data, as mentioned in section \ref{firmware_encoder_section}. This approach allows the controller to take into account the motor's position and adjust the control signals accordingly.

Since the sinusoidal voltage control requires trigonometric functions, a lookup table is employed to speed up the calculations. By utilizing this lookup table, the main loop with all the required calculations runs at an average frequency of 35 kHz. This high-frequency operation ensures smooth and precise control of the \gls{BLDC} motor, resulting in optimal performance and efficiency.

The Driver board provides two types of controllers for different operational scenarios. During normal operations, the board employs the velocity \gls{PID} controller to regulate the motor's velocity and ensure smooth and accurate movements. In situations where the robot needs to brake the motors or hold a specific position, the board switches to using the position \gls{PID} controller. This controller is specifically designed to provide precise control over the motor's position, enabling the robot to brake instantly and remain stationary when required.

Furthermore, the Driver board allows for dynamic adjustment of the \gls{PID} controller's constants, as mentioned in the section \ref{firmware_communication_section}. This feature enables users to fine-tune the motor control parameters to suit specific requirements or optimize the robot's performance under varying conditions. The ability to modify the \gls{PID} constants provides a higher level of flexibility and adaptability, making the robot more versatile and capable of handling different tasks and environments.

\section{Interrupt service routine} \label{firmware_ISR_section}

The Driver board effectively controls the motor by utilizing an \gls{ISR} to handle various interrupts. The most critical interrupt managed by the \gls{ISR} is the velocity synchronization interrupt, which occurs on the falling edge of the \inlineCode{SYNC} pin. Only when the Driver boards detect this specific interrupt, do they adjust the motor's velocity according to the control command received from the Main board, as explained in section \ref{firmware_communication_section}. This synchronization mechanism ensures coordinated and accurate motor movements across all the motors.

Another important interrupt handled by the \gls{ISR} is the brake interrupt. When the microcontroller detects the rising edge of the \inlineCode{MOTOR_BRAKE} pin, it switches to using the position \gls{PID} controller with the reference to the motor's angle at the time the interrupt was received. This enables the motors to be instantaneously stopped and hold their positions effectively. All Driver boards share the same \inlineCode{SYNC} and \inlineCode{MOTOR_BRAKE} pins.

Additionally, the \gls{ISR} manages two other interrupts unique to each board. The first one checks whether the Main board turned the DRV8311 motor driver power on or off. This is detected on the \inlineCode{MOTOR_NSLEEP} pin. The second interrupt, taking place on the \inlineCode{MOTOR_NFAULT} pin, monitors the state of the motor driver. The DRV8311 motor driver can indicate overcurrent or high-temperature conditions through this pin, allowing for effective fault detection and handling.

By utilizing these interrupts, the Driver board can precisely control the motors, ensuring movement synchronization, quick braking, and effective motor state monitoring. This robust control mechanism enhances the robot's performance and reliability in various scenarios.

\section{Driver board faults}

The Driver board provides visual signalization of its states through the \inlineCode{MCU_FAULT} pin. This pin is connected to one of the red LEDs on the Driver board. Additionally, the pin is directly linked to the Main board, enabling the Main board to detect changes in the state of the Driver board.

The Driver board can signal various states using the \inlineCode{MCU_FAULT} pin. For example, it can indicate when the motor driver power is not enabled, when there is a significant velocity error, when the temperature of the Driver board is high, or when it detects overcurrent or high-temperature faults from the DRV8311 motor driver itself.

The DRV8311 motor driver faults may occur when the robot voltage exceeds 21 volts and the motor gets stuck for an extended period. In such cases, the control of the motor power can be switched to the Driver board itself, as the \inlineCode{MOTOR_NSLEEP} pin is connected to both the Main board and the Driver board.

By providing visual feedback through the \inlineCode{MCU_FAULT} pin, the Driver board enhances the robot's diagnostics capabilities, allowing for better monitoring of the motor's state and the overall performance of the motor driver. This information is critical for identifying and addressing potential issues promptly, ensuring the robot operates efficiently and safely.

\section{Communication} \label{firmware_communication_section}


The communication between the Driver board and the Main board is accomplished through \gls{SPI} connections, with a baud rate set to 9 MBits per second. The message data size for this communication is 16 bits. The Driver board utilizes \gls{DMA} for \gls{SPI} communication, significantly reducing CPU overhead \cite{cite:DMA}. The \gls{DMA} is configured in circular mode, enabling continuous data transfer without CPU intervention.

The Driver board operates as a full-duplex slave in this communication setup, meaning it only sends and receives data when the Main board initiates communication with it. To optimize communication efficiency, the Driver board sends and receives data in 12-byte packets. The data sent by the Driver board consists of three float values, representing the motor's velocity, position, and the Driver board's temperature.

On the other hand, the Driver board receives two float values and one uint32\underline{ }t value from the Main board. The float values are used to set the mode of the motor and the requested motor velocity. The mode value serves to control various states of the motor, such as enabling or disabling it, setting motor power, applying brakes or setting the motor position to zero. Additionally, the uint32\underline{ }t value sets the acceleration time of the motor, determining how many milliseconds it takes to reach the required velocity.

To configure the \gls{PID} constants on the Driver board, special values are used. When the acceleration time is set to 3000000000, the P and I constants are set using the mode and velocity float values, in respective order. When the acceleration time is set to 4000000000, the mode value is used to set the D constant, while the velocity value sets the anti-windup value of the \gls{PID} controller.

This communication strategy was designed to minimize the size of messages exchanged between the boards, resulting in faster communication and more efficient robot operation. By properly initializing the \gls{DMA}, the Driver boards are relieved from handling the communication process actively. They only need to update the position, velocity, and temperature values periodically, while the \gls{DMA} takes care of the rest. When the Main board wishes to read data from the Driver board, it simply sends a command message and receives the values accordingly. For setting data, the Main board sends a command message with the \inlineCode{SYNC} pin connected to logic level voltage. After setting the pin back to the ground, the Driver boards execute the command message simultaneously, thanks to the \gls{ISR}. This efficient communication mechanism streamlines the control and operation of the robot, enabling precise motor control and smooth interaction between the boards.