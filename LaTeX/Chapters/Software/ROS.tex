\chapter{Robot Operating System}

\section{ROS packages} \label{software_ROS_packages}

\subsection{Robot packages} \label{software_robot_packages_subsection}

\subsubsection{cras\underline{\enskip}srobot\underline{\enskip}bringup}

This package includes the complete model of the robot, providing a comprehensive representation of its components and behaviors. The launch file, named \inlineCode{start.launch}, can initiate the robot's simulation within the custom-built Gazebo world or activate the real robot application. By default, the launch file initiates the real robot application. Additionally, this launch file is also responsible for initializing other packages referenced in this section, ensuring a coordinated and coherent startup process for the robot's functionality.

\subsubsection{cras\underline{\enskip}srobot\underline{\enskip}driver}

This package assumes the critical role of facilitating effective communication with the robot. It takes on the responsibility of transmitting control commands to the robot. Additionally, it publishes the received information about the robot state by a structured message called \inlineCode{Robot_state.msg}. 

\subsubsection{cras\underline{\enskip}srobot\underline{\enskip}control}

This package is responsible for computing the robot's odometry by processing the received motor velocities. Moreover, it provides the crucial ability to control the movement of the robot. The package achieves this functionality by subscribing to \inlineCode{PoseStamped} messages, which are obtained through the topics \inlineCode{/move_base_simple/goal} and \inlineCode{/cras_robot/goal}. Upon the reception of a goal message, this package generates the requisite control commands for the robot to navigate toward the specified destination. This capability with the help of other packages enables the robot to autonomously navigate and traverse the desired paths, enhancing its overall mobility and versatility.

\subsubsection{cras\underline{\enskip}srobot\underline{\enskip}frontier}

The primary role of this package centers on frontier exploration, with the ability to identify either the closest frontier in proximity to the robot or select a random frontier. This exploration data is shared visually through the use of \inlineCode{OccupancyGrid} messages, which are published via the \inlineCode{/cras_robot/occupied_space} and \inlineCode{/cras_robot/finded_frontiers} topics. This functionality enhances the robot's ability to explore and navigate its surroundings, making informed decisions based on the availability of frontiers and the occupancy status of the environment.

\subsubsection{cras\underline{\enskip}srobot\underline{\enskip}exploration}

The main purpose of this package is to facilitate the autonomous navigation and exploration of uncharted territory by the robot. It leverages the functionality of the \inlineCode{cras_srobot_frontier} package to identify potential frontiers within the environment and subsequently generates a path leading to the selected frontier. The package collaborates with the \inlineCode{cras_srobot_control} package to ensure effective navigation along the generated path. 

It's important to note that, as of the submission date, this package remains a work in progress and is not fully implemented. This indicates that further development is required to realize the complete functionality intended by this package.

\subsection{Other used packages}

\subsubsection{rplidar\underline{\enskip}ros}

The purpose of this package is to establish communication with the RPLidar A3 sensor that is mounted on the robot. This \gls{LIDAR} sensor offers a comprehensive 360-degree scanning field, allowing for rapid distance measurements at a rate exceeding 16,000 samples per second. The primary function of this package is to facilitate the transmission of the acquired \gls{LIDAR} data \cite{cite:rplidar}.

The package achieves this by publishing the \gls{LIDAR} scan data in the form of \inlineCode{LaserScan} messages, which are shared through the designated \gls{ROS} topic \inlineCode{/cras_robot/scan}. By publishing these messages, the package enables other components of the robot's software to access and utilize the \gls{LIDAR} data for various purposes, such as obstacle detection, environment mapping, and navigation.

\subsubsection{hector\underline{\enskip}mapping}

This package takes on the critical role of constructing an environmental map utilizing SLAM algorithms. Its core function is to build a representation of the surroundings, which is essential for the robot's spatial awareness and navigation capabilities \cite{cite:hector_mapping}.

To accomplish this, the package subscribes to the \inlineCode{/cras_robot/scan} and \inlineCode{/cras_robot/odom} topics. These topics are published in respective order by the \inlineCode{rplidar_ros} and \inlineCode{cras_srobot_control} packages. The package combines these two sets of data to generate a comprehensive global map of the environment. The map is represented using the \inlineCode{OccupancyGrid} message type and shared via the \inlineCode{/cras_robot/map} topic.


\section{Start up sequence} \label{software_start_up_sequence}

The Intel NUC is designed to power up only when it receives the appropriate signal from the robot's front panel connector. It does not automatically power on when connected to the power supply. Therefore, after the momentary switch is pressed, bringing the robot to life, the NUC remains powered off until the robot determines that the batteries have sufficient power.

After the robot decide that the batteries are charged enough, it initiates the NUC's startup process by activating the front panel connector. Once the NUC boots up, it automatically starts a robot start-up script. This script is responsible for initiating the functionality of the entire robotic platform. The start-up script is designed to demonstrate the capabilities of the robot and its various components, showcasing its potential applications and features.

\subsection{Demonstration} \label{software_demonstration_subsection}

The startup script, as detailed in section 3.2, initiates the operational phase of the robot. It accomplishes this by launching the \inlineCode{start.launch} launch file, which is provided by the \inlineCode{cras_srobot_bringup} package. This package serves as an automated mechanism for the initiation and configuration of all the \gls{ROS} packages mentioned.

The startup process involves the opening of multiple terminal windows, each serving a specific purpose in the robot's operation and monitoring:

\begin{enumerate}
  \item \textbf{Main Terminal}
  \begin{itemize}
      \item This terminal displays messages and output from the \inlineCode{rplidar_ros} package, which handles the communication with the \gls{LIDAR} sensor, and the \inlineCode{hector_mapping} package, responsible for SLAM-based mapping.
  \end{itemize}
  \item \textbf{Driver Terminal}
  \begin{itemize}
    \item This terminal presents messages related to the communication with the robot through the \inlineCode{cras_srobot_driver} package. It offers a real-time view of the communication exchanges between the system and the robot.
  \end{itemize}
  \item \textbf{Control Terminal}
  \begin{itemize}
    \item This terminal provides information from the \inlineCode{cras_srobot_control} package about the robot's odometry. Additionally, it presents the calculated motor velocities required to reach a selected goal, alongside the representation of the goal itself.
  \end{itemize}
  \item \textbf{Frontier Terminal}
  \begin{itemize}
    \item This display is dedicated for a \inlineCode{cras_srobot_frontier} package. In this terminal, the found frontiers and the selected frontier that the robot should explore are displayed. This provides insights into the robot's decision-making process during exploration.
  \end{itemize}
  \item \textbf{Exploration Path Terminal}
  \begin{itemize}
    \item The final terminal serves to display the discovered path generated by the \inlineCode{cras_srobot_exploration} package. This path represents the route the robot intends to follow during exploration activities.
  \end{itemize}
\end{enumerate}

By organizing these terminal windows, the startup script offers a comprehensive insight into various aspects of the robot's operation, from sensor data processing and mapping to control, exploration, and navigation. This setup enhances the ability to monitor and interact with the robot during its mission.

During the demonstration, the Intel NUC starts reading data from the \gls{LIDAR} sensor. Using SLAM algorithms, the NUC creates a map based on the \gls{LIDAR} data, enabling the robot to have a representation of its environment.

After generating the map, the robot starts searching the environment by identifying the nearest frontier and creating a path for the robot to follow toward that frontier. As mentioned in the subsection \label{software_robot_packages_subsection}, as of the submission date, the \inlineCode{cras_srobot_exploration} package remains a work in progress and is not fully implemented yet. Nevertheless, if the robot receives the correct path, it autonomously navigates along the path to that frontier using its odometry and the map generated by the SLAM algorithms.

The map search process can be stopped and started again using the capacitive buttons located on the robot. This provides the user with control over the robot's exploration behavior. Additionally, if the robot collides with an obstacle while searching the map, it triggers the braking mechanism to stop and avoid potential collisions. To continue the map search after encountering an obstacle, the user needs to press the START button on the robot. Once the robot completes the map search, it stops and signals this event with sound and light signalization.

Overall, the demonstration showcases the robot's autonomous exploration capabilities, map creation, and ability to navigate toward unexplored areas within its environment. The start-up script and the integrated \gls{ROS} packages make it possible to execute these complex operations and demonstrate the robot's functionality in a user-friendly and controlled manner.

\section{Robot model} \label{software_robot_model_section}

For simulation and visualization purposes, a complete model of the robot was developed using the \gls{XACRO} language. This model, depicted in figure \ref{fig:robot_model}, is designed to replicate both the kinematics and dynamics of the actual robot. It incorporates accurate values for inertia and friction to closely mimic the behavior of the real robot. Each wheel of the simulation model can be actuated using the JointGroupVelocityController, enabling control over their rotational speeds. Additionally, the model allows for setting the rotational speed of the \gls{LIDAR} through the JointVelocityController, which is used primarily for visualization purposes. Furthermore, the orientation of the virtual robot and its joints corresponds precisely to the real-world robot. The velocities of the motors within the simulation are set in the same units and directions as those in the physical robot. This level of fidelity ensures that the simulation provides an accurate representation of the robot's behavior.

The simulation model provides an invaluable tool for testing and validating the robot's behaviors and algorithms in a controlled and virtual environment before implementing them on the physical robot. This level of consistency between the simulation and the real-world robot streamlines the development process and contributes to the overall success of the educational robotic platform.

\begin{figure}
    \centering
    \includegraphics[width=1.0\linewidth]{Files/Images/Software/RViz/model1.png}
    \caption{The robot model displayed in the \gls{RVIZ} application}
    \label{fig:robot_model}
\end{figure}

\section{Robot messages}

The communication between the Intel NUC and the robot involves a specific set of messages outlined in subsection \ref{firmware_cummunication_intel_NUC_subsection}. Upon reception, these messages are processed and subsequently published to the \inlineCode{/cras_robot/robot_state} topic. This topic employs a custom \gls{ROS} message format called \inlineCode{/cras_srobot_driver/Robot_state.msg}, which comprehensively encompasses all the information the robot shares. This message format incorporates a wide range of data, including details about the robot's current consumption, battery and robot voltages, various temperature readings, capacitive tactile button states, and motor positions and velocities. 

The \inlineCode{frame_id} field within the message header offers insight into the content of the most recent message that the NUC received. This approach allows for the integration of real-time data from the robot into the \gls{ROS} ecosystem, enabling effective control, monitoring, and analysis of the robot's status and performance.

\section{Odometry}

The odometry system implemented in this robot is built upon the work conducted by Jordi Palacín from the University of Lleida, as detailed in \cite{cite:used_odometry}. The kinematics of the omnidirectional robotic platform is represented in figure \ref{fig:coordinate_system_of_the_robot} and mirrors the configuration of the omnidirectional mobile robot as described in the referenced article, with the only alteration being the arrangement of the motors. In essence, the robot calculates the angular rotational velocity for each wheel based on the motion vector $M = (v, \alpha, \omega)$. In this context, $v$ signifies the translational velocity of the displacement (in m/s), $\alpha$ represents the angular orientation of the displacement (in radians) with respect to the robot's reference frame, and $\omega$ indicates the angular rotational speed (in rad/s) applied to the center of the omnidirectional mobile robot. This calculation process is depicted in the equation \ref{software_odometry_angular_velocity_equation}, where the parameters $(\delta_a, \delta_b, \delta_c)$ represent the angular orientation of each omnidirectional wheel, relative to the robot reference frame. In this configuration, the angles are assigned as $(60\degree, 300\degree, 180\degree)$. Parameter $R = 0.108$ represents the radial distance of the wheels relative to the center of the robot and parameter $r = 0.041$ corresponds to the radius of the wheel.

\begin{equation}
  \begin{split}
    v_x &= v \cdot \cos(\alpha) \\
    v_y &= v \cdot \sin(\alpha)
  \end{split}
\end{equation}

\begin{equation}
  IK = 
  \begin{bmatrix}
    -\sin(\delta_a) & \cos(\delta_a) & R\\
    -\sin(\delta_b) & \cos(\delta_b) & R\\
    -\sin(\delta_c) & \cos(\delta_c) & R
  \end{bmatrix} 
\end{equation}

\begin{equation} \label{software_odometry_angular_velocity_equation}
  \begin{bmatrix}
    \omega_a \\
    \omega_b \\
    \omega_c
  \end{bmatrix} 
  = IK \cdot
  \begin{bmatrix}
    v_x \\
    v_y \\
    \omega
  \end{bmatrix} 
  \cdot \frac{1}{r}
\end{equation}

The robot then employs the received motor angular velocities $(\omega_a, \omega_b, \omega_c)$ to compute the coordinates $(x, y, \theta)$ of the robot within the global world frame. This calculation process is described by the equation \ref{software_odometry_coordinates_equation}, where parameters $(x_i, y_i, \theta_i)$ represent the actual position of the robot in the world reference frame and parameter $\Delta T$ symbolizes the time lapse between the calculation measurements. This odometry system enables an accurate estimate of the robot's position and orientation, enhancing its navigational capabilities and contributing to its overall control and movement precision. 

\begin{equation}
  \begin{bmatrix}
    v_x \\
    v_y \\
    \omega
  \end{bmatrix}_{robot}
  = IK^{-1} \cdot
  \begin{bmatrix}
    \omega_a \\
    \omega_b \\
    \omega_c
  \end{bmatrix}
  \cdot r
\end{equation}


\begin{equation}
  \begin{bmatrix}
    v_x \\
    v_y \\
    \omega
  \end{bmatrix}_{world}
  =
  \begin{bmatrix}
    \cos(\theta_i) & -\sin(\theta_i) & 0 \\
    \sin(\theta_i) & \cos(\theta_i) &  0\\
    0 & 0 & 1
  \end{bmatrix} 
  \cdot 
  \begin{bmatrix}
    v_x \\
    v_y \\
    \omega
  \end{bmatrix}_{robot}
\end{equation}

\begin{equation} \label{software_odometry_coordinates_equation}
  \begin{bmatrix}
    x \\
    y \\
    \theta
  \end{bmatrix}_{world}
  =
  \begin{bmatrix}
    x_i \\
    y_i \\
    \theta_i
  \end{bmatrix}_{world}
  + 
  \begin{bmatrix}
    v_x \\
    v_y \\
    \omega
  \end{bmatrix}_{world}
  \cdot \Delta T
\end{equation}


For a more comprehensive understanding of this methodology, in-depth information can be sourced from the original articles \cite{cite:used_odometry} and \cite{cite:main_odometry}.

\begin{figure}
    \centering
    \includegraphics[width=1.0\linewidth]{Files/Images/Software/coordinate_system_of_the_robot.pdf}
    \caption{Coordinate system of the robot with the respective position of the wheels}
    \label{fig:coordinate_system_of_the_robot}
\end{figure}

\section{RViz}

The figure \ref{fig:RViz} showcases the \gls{RVIZ} application interfaced with the robot's demonstration script running. Within the image, the robot model, along with its calculated odometry, is displayed. Furthermore, the figure incorporates the map data, which has been processed via the SLAM algorithms. The figure also integrates the output of the \inlineCode{cras_srobot_frontier} package, where the grey area denotes potential collision zones, the orange area signifies identified frontiers, and the green area represents the selected frontiers, with the central orange frontier being the one chosen for further exploration. Lastly, the visualization features real-time \gls{LIDAR} data obtained from the robot sensor.

\begin{figure}
    \centering
    \begin{subfigure}{.502\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Software/RViz/side.png}
      \caption{Side view}
      \label{fig:RViz_side}
    \end{subfigure}%
    \begin{subfigure}{.498\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Software/RViz/top.png}
      \caption{Top view}
      \label{fig:RViz_top}
    \end{subfigure}
    \caption{The robot visualization in the RViz application}
    \label{fig:RViz}
\end{figure}

\section{Gazebo simulator}

In the development process outlined in section \ref{software_robot_model_section}, a comprehensive model of the robot was constructed specifically for simulation applications. This simulated model accurately replicates both the kinematics and dynamics of the real robot. By utilizing this model, developers can first create and test their code within the Gazebo simulation environment before deploying it onto the physical robot. This approach offers several advantages, including streamlined development and the ability to identify and address potential issues in a controlled virtual setting.

To facilitate this simulation process, an entire Gazebo world was designed. Figure \ref{fig:gazebo_world} showcases this Gazebo world environment, while figure \ref{fig:gazebo_world_robot_closeup} provides a close-up view of the robot within this simulated environment. The blue region visible in the figure represents the Gazebo laser sensor plugin, which simulates the behavior of the \gls{LIDAR} sensor on the robot. By using Gazebo in conjunction with the robot model, developers gain the advantage of thoroughly validating their code in diverse scenarios, ensuring it operates as expected before being applied to the physical robot.

\begin{figure}
    \centering
    \begin{subfigure}{.55\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Software/Gazebo/world_side.png}
      \caption{Side view}
      \label{fig:gazebo_world_side}
    \end{subfigure}%
    \begin{subfigure}{.45\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Software/Gazebo/world_top.png}
      \caption{Top view}
      \label{fig:gazebo_world_top}
    \end{subfigure}
    \caption{The created Gazebo world for simulation purposes}
    \label{fig:gazebo_world}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.95\linewidth]{Files/Images/Software/Gazebo/robot1.png}
      \caption{Close-up view of the Gazebo world}
      \label{fig:gazebo_robot1}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.95\linewidth]{Files/Images/Software/Gazebo/robot2.png}
      \caption{Close-up view of the robot}
      \label{fig:gazebo_robot2}
    \end{subfigure}
    \caption{Close-up view of the robot within the Gazebo world}
    \label{fig:gazebo_world_robot_closeup}
\end{figure}