\chapter{Conclusions}

The master's thesis has comprehensively explored the design, construction, firmware development, and software implementation of the SRobot, a versatile indoor mobile robotic platform explicitly designed as an educational tool for university students. The platform aims to impart fundamental robotics concepts encompassing both hardware and software aspects.

The robot's overview highlighted numerous distinct advantages over comparable robots. Foremost among these are the robot's modular design, extended operational lifetime, and innovative capacitive tactile bumper, which endows the platform with true omnidirectional capabilities. The integration of \gls{BLDC} motors not only ensures enhanced precision and smoother movement but also contributes to energy efficiency. Remarkably, the SRobot stands out for its affordability, with a significant portion of the cost attributed to the Intel NUC and RPLidar A3 components.

The construction process provides a comprehensive guide for replicating the educational platform. It meticulously outlines the selection and functionality of each component. It elucidates the purpose and structure of the capacitive bumper and delves into battery selection, balancing, and charging processes. The detailed \gls{PCB} design with all integrated components is fully described and visually presented, providing a clear blueprint for assembly.

The firmware part offers insights into the roles of the STM32 microcontrollers, providing clear instructions for code upload to each board. The robot connection diagram is elucidated, enhancing the comprehension of the communication infrastructure. \gls{BLDC} motor control, including the use of built-in encoders, is thoroughly explained, as is the \gls{PID} controller's role in ensuring smooth motor control. The firmware part delves into the various interrupts managed by the \gls{ISR}, elucidates communication protocols between each board and underscores the value of multiple analog measurements. The purpose of the momentary switch and the front panel connector is clarified, along with the communication between the robot and the onboard computer. The thesis also highlights the protective features offered by the board and underscores the high flexibility and wide array of connectors available to accommodate diverse modules and sensors.

The software part provides insight into the \gls{ROS} packages employed by the robot, explaining topics and messages within each package. It outlines the startup sequence and launched terminals, presenting a comprehensive perspective on the robot's functioning. The creation and utilization of the robot model for simulation and visualization purposes were explained, along with the processing and publishing of received messages from the robot. The implementation of odometry is thoroughly explained, complete with the relevant equations and the established robot coordinate system. The \gls{RVIZ} application, running alongside the demonstration script was demonstrated and the importance of Gazebo simulation, depicted through the simulation world, was emphasized.

The last part emphasizes the suitability of the robot as an educational platform. It showcases a range of educational use cases and topics that could be taught using the platform and offers a comprehensive robotics curriculum focused on the robot as an educational tool. Additionally, the provided example lessons, covering topics from firmware implementation for each board to the utilization of SLAM algorithms for exploration purposes, solidify the value of the robotic platform as a versatile and effective educational platform.

In conclusion, the successful fulfillment of this master's thesis assignment has resulted in the creation of the SRobot, an exemplary indoor mobile robotic platform. Its modular design, hardware capabilities, sophisticated firmware, and versatile software make it an ideal educational tool. The inclusion of example lessons further enhances its value, making the SRobot a valuable resource for teaching robotics concepts to university students.

\section{Future improvements}

\subsection{Hardware}

The current design of the system exhibits certain imperfections, which require focused attention in future iterations. One prominent issue pertains to the battery charging and balancing mechanism, necessitating a redesign to ensure proper functionality. The placement of the Schottky diode directly on the board is crucial, and a separation of the power supply ground from the robot ground connection is essential. Implementing low voltage and current protection for each battery is also a recommended enhancement.

Furthermore, the STM32 on the Main board, responsible for USB communication with the onboard computer, necessitates a 1.5~$k\Omega$ pull-up resistor connected to the DM channel to operate properly. Additionally, the DM channel requires hardware pull-up post the STM32 initialization. This challenge could potentially be addressed by adopting different STM32 microcontroller versions that manage this autonomously, eliminating the need for specific enable pins.

Furthermore, enabling the \gls{TSC} necessitates the use of \gls{ISR}. Regrettably, the current interrupt allocation conflicts with one of the \gls{UART} peripherals. This problem would need to be put into consideration for the next design.

To streamline the design, the connector for the balancing circuit will be replaced with a 2~mm JST-PH connector, minimizing unnecessary cable crimping and enhancing reliability.

One significant enhancement planned for future designs is the inclusion of an onboard inertial measurement unit on the Main board, addressing the current lack of this feature. This addition will significantly improve the robot's capabilities, particularly improving the measured odometry accuracy.

Another pivotal modification in future designs involves replacing the \gls{PCI} Express connector with a more robust edge card connector. The current connector lacks secure connectivity and may result in pin disconnection due to deflection, primarily affecting larger boards. This issue does not impact the current lightweight Driver board setup.

Consideration is also given to potentially upgrading the STM32 microcontroller on the Driver boards or introducing current measurement sensors between the motor phases. This enhancement would enable \gls{FOC} control for the motors, enhancing their performance.

Lastly, relocating the battery current measurement sensors between the charger and the Schottky diode is envisioned. This alteration would enable the robot to distinguish between charging and non-charging states. During the charging process, the robot could measure charging current and battery voltages, leveraging LED visualization to provide insights into the ongoing charging process. This process would contribute to the system's overall usability and functionality.

\subsection{Software}

The software aspect of the robot also requires further attention to unlock its full potential. Key areas of focus for future development include enhancing the \inlineCode{rosserial} package, which will enable the robot to communicate over a standardized \gls{ROS} protocol. This enhancement is essential for seamless integration with \gls{ROS}, facilitating more efficient data exchange and interaction.

An essential improvement will involve the creation of a \gls{ROS} \inlineCode{move_base} for the robot. This addition would grant the robot access to various existing \gls{ROS} navigation and exploration packages, significantly expanding its capabilities.

Additionally, efforts will be directed toward enabling ssh communication and the option to attach to the tmux terminal sessions. This feature will provide a convenient means to monitor the robot's state and the status of each package, enhancing the debugging and management process.

The final planned update for the robot involves moving the computation of odometry directly onto the robot itself. Integrating onboard odometry calculations, combined with data from the IMU, will result in more precise measurements and reduced drift. This enhancement ensures the robot's navigational accuracy and enhances its overall performance.

\section{Resources}

All the resources, including CAD models, \gls{PCB} designs, firmware, and software codes related to this project, are readily accessible within the attached Git repository \cite{cite:git_repository}. These valuable assets are made available under the BSD and MIT licenses, ensuring openness and flexibility in their usage. The \gls{PCB} designs are covered by the Creative Commons license.

For those seeking to explore the repository further or access any specific details, please follow this link: \\ \url{https://gitlab.com/WenceslawCZ/design-and-construction-of-an-omnidirectional-base-for-student-robotics-platform}

For any additional information or potential inquiries regarding the SRobot, please feel free to contact me directly via email at \href{mailto:vaclav@vesely.pro}{\inlineCode{vaclav@vesely.pro}}. I am more than willing to provide insights and answer any questions about this educational platform, its design, capabilities, and potential applications.