\chapter{Batteries}

As a consequence of opting for the three-wheel platform as discussed in subsection \ref{wheels_subsection}, the robot's stability becomes a critical consideration, necessitating careful attention to its center of gravity, symmetry, and balance. Given this concern, the battery must be positioned either at the robot's center or evenly distributed across its structure. However, due to the center space being reserved for the Intel NUC and \gls{LIDAR} sensor, as mentioned in chapter \ref{electronics_and_control_systems_chapter}, the battery had to be distributed to ensure optimal weight distribution and maintain the robot's stability.

\section{Battery} \label{battery_section}

The robot's larger internal free space allowed for the batteries to be placed inside the robot. The primary considerations when selecting the batteries were easy maintenance, user-friendliness, robot safety, and overall lifetime. For meeting these requirements, the lithium-ion battery was chosen, particularly the 26650 battery, as depicted in figure \ref{fig:battery}. These batteries were preferred over the 18650 and 21700 options due to their extended lifetime and higher capacity, all while maintaining the same output current capabilities.

Specifically, this battery offers a nominal capacity of 5000mAh, is capable of enduring over 1000 charging cycles, and has a rated current of 30 amperes, making it a well-suited choice for the robot's power requirements.

The selection of a 4-cell battery was made to ensure the minimal operational voltage for the Intel NUC while adhering to the recommended maximum voltage for the motor's drivers. This choice precisely meets the minimum 12V requirement for powering the Intel NUC, while the maximal battery voltage of 16.8V remains comfortably below the recommended 20V for the motor driver, ensuring safe and efficient operation. With this setup, the battery achieves an energy capacity of 74 watt-hours. To further enhance the robot's energy storage capacity, three of these battery modules are connected in parallel within the robot, providing the robot with a combined total of 222 watt-hours of energy. 

The batteries are interconnected using Schottky diodes, allowing for safe battery exchange or replacement. While this method may not be the most ideal solution for battery-driven systems due to thermal losses, it provides a practical and manageable way to handle battery maintenance. However, this design is planned to be improved, aiming to implement a battery management system that achieves the same functionality without any energy losses. 

While a single battery module can power up the robot, it is strongly discouraged due to potential issues with thermal losses and overall system efficiency. When relying solely on one battery module, all the current flows through a single route on the \gls{PCB} and a single Schottky diode, leading to increased thermal losses and potential overheating. The circuit is intentionally designed to ensure that the batteries will balance themselves and provide an even distribution of current among all modules. By allowing the current to flow through each battery, the load is distributed, reducing the risk of overheating and maintaining a more stable and reliable power supply. The recommended current for each battery module that should not be exceeded is 10 amperes, providing an optimal operating range that ensures the battery's longevity and safe performance. 

\begin{figure} [h!]
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/battery.png}
      \caption{LiitoKala INR26650-50A battery}
      \label{fig:battery}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/Disassembled/side.png}
      \caption{Front view of the battery module}
      \label{fig:disassembled_battery_front}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/Disassembled/top.png}
        \caption{Top view of the battery module}
        \label{fig:disassembled_battery_top}
      \end{subfigure}%
      \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/Disassembled/bottom.png}
        \caption{Bottom view of the battery module}
        \label{fig:disassembled_battery_bottom}
      \end{subfigure}
    \caption{Disassembled battery module}
    \label{fig:real_robot_battery_disassembled}
\end{figure}

Figure \ref{fig:real_robot_battery_disassembled} shows a disassembled battery module, offering a clear view of how each cell is connected to the board containing the battery connector. On the other hand, figure \ref{fig:real_robot_battery} displays the battery module in its assembled form. The top of the battery module features a convenient handle, allowing for effortless removal from the robot when necessary. Additionally, the bottom part of the module is equipped with a 3~mm inserted nut, which serves a practical purpose during transport or other scenarios. This nut allows for direct mounting of the battery module to the robot, ensuring stability and ease of transport.

\begin{figure} [h!]
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/Assembled/side_without_heat_shrink_sleeve.png}
      \caption{Battery module without heat shrink sleeve}
      \label{fig:battery_without_heat_shrink_sleeve}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/Assembled/side.png}
      \caption{Front view}
      \label{fig:battery_front}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/Assembled/top.png}
        \caption{Top view}
        \label{fig:battery_top}
      \end{subfigure}%
      \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/Assembled/bottom.png}
        \caption{Bottom view}
        \label{fig:battery_bottom}
      \end{subfigure}
    \caption{Battery module}
    \label{fig:real_robot_battery}
\end{figure}

\subsection{Charger} \label{battery_charger_subsection}

To charge the robot, the user can utilize the XT60 connector, as mentioned in subsection \ref{body_upper_part_subsection}. This connector is directly linked to the battery charger modules, visible in figure \ref{fig:charger_module}. The input voltage required for these chargers falls within the range of 17 to 32 volts. Each battery has a dedicated charger module, and each charger can provide a charging power of 50 watts. Consequently, the robot can be fully charged in approximately one and a half hours.

The decision to employ battery charging modules instead of an onboard solution was driven by easier maintenance. In case of damage, a charger module can be readily replaced with a new one, preserving the robot's operability. Conversely, with an onboard solution, a malfunctioning charger could render the entire robot unusable.

However, using external charger modules led to an unexpected drawback. When directly connected to the battery, the charger draws some energy from it, causing battery discharge over time. This problem was solved by adding a Schottky diode between the charger and the battery. Another issue that was encountered involved ground connections. Although the charger appeared to share the input and output grounds when connected to the power supply, the grounds disconnected. It was determined that the charger required separate ground connections for the input voltage and the output voltage to work properly.

While those issues will be addressed in future versions, subsection \ref{battery_balancer_subsection} provides details on how to manage this concern in this current configuration, ensuring the safe and optimal use of the robot and its batteries.

\begin{figure}
    \centering
    \includegraphics[width=0.5\linewidth]{Files/Images/Real_robot/Battery/charger_module.png}
    \caption{XL4015 5A CC/CV battery charger}
    \label{fig:charger_module}
\end{figure}

\subsection{Balancer} \label{battery_balancer_subsection}

This robot was initially designed to feature an active cell balancing circuit, which effectively balances the cells by directly transferring energy from one battery to another using various capacitors. However, due to the problematic issue with the battery charger mentioned in subsection \ref{battery_charger_subsection}, the active cell balancing circuit had to be replaced with a battery protection and balancing module depicted in figure \ref{fig:balancer_module}.

In the new setup, the charger output ground is directly connected to the P- pad, which is shown in the figure of the battery protection module. This connection creates a virtual ground for the charger and also allows the protection circuit to disconnect the charger from the battery once it's no longer charging. The input ground of the charger is connected directly to the input power supply.

Moreover, the balancing part of the module only operates during charging and not continuously like the active cell balancing circuit would. While the replacement module serves its purpose by ensuring battery protection and balancing during charging, there are certain drawbacks compared to the original design.

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{Files/Images/Real_robot/Battery/passive.png}
    \caption{4S 30A battery protection and balancing module}
    \label{fig:balancer_module}
\end{figure}

\section{Battery connectors} \label{battery_connectors_section}

For the connection between the battery and the main board, the robot uses the connector shown in figure \ref{fig:real_robot_battery_connectors}. Each pin of this connector provides a current rating of 7 amperes. To efficiently power the robot, the battery employs three pins for ground connections, three pins for supplying power to the robot, and the remaining four pins for balancing the battery cells. Based on this configuration, it is crucial to ensure that the average current drawn from any single battery does not exceed 21 amperes, with the possibility of occasional peaks reaching a maximum of 30 amperes. By adhering to these current limits, the robot can operate safely and effectively, preventing any potential overload situations that may compromise its performance or damage the components.

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/connector_female.jpg}
      \caption{Battery connector}
      \label{fig:connector_female}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Battery/connector_male.jpg}
      \caption{Main board connector}
      \label{fig:connector_male}
    \end{subfigure}
    \caption{Connectors for connecting the battery to the Main board \cite{cite:battery_connector}}
    \label{fig:real_robot_battery_connectors}
\end{figure}