\chapter{Robot chassis}

\begin{figure}[h!]
  \centering
  \begin{subfigure}{1.0\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Robot_comparison/SRobot/1.png}
      \vspace{-1.25cm}
      \caption{Model of the educational robotic platform}
      \vspace{0.25cm}
      \label{fig:srobot_model}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Assembled_robot/1_ON.png}
    \caption{Side view}
    \label{fig:srobot1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/Assembled_robot/2_ON.png}
    \caption{Top view}
    \label{fig:srobot2}
  \end{subfigure}
  \caption{The fully assembled educational robotic platform}
  \label{fig:srobot_assembled_robot}
\end{figure}


The robot chassis is made only from 3D-printed parts. The primary emphasis during the design phase was placed on the platform's robustness, user-friendly attributes, ease of maintenance, and swift reparability.
The body of the robot has a circular shape. This shape was selected mainly due to the fact, that the robot should serve as an educational platform for students. This design enables effortless orientation and smooth navigation in complex environments, substantially facilitating path-planning tasks. 

Due to the circular shape and the requirement for omnidirectional capabilities, the robot uses three omni wheels, directly linked to three \gls{BLDC} motors. Further details regarding this configuration can be found in section \ref{motors_and_wheels_section}.

To enable the robot's operation without relying on additional sensors, the robot uses a capacitive bumper to detect any obstacles, more about that in section \ref{capacitive_bumper_section}.

\section{Body}

The robot body is divided into two sections. The lower part houses all essential components, including electronics, motors, wheels, and capacitive sensors. The upper part serves as an intermediary linking the robot to a single-board computer for high-level control, particularly connecting it to the Intel NUC in this instance. Additionally, the upper section serves as an interface between the user and the robot, providing real-time status updates and enabling seamless user control over its operations.

\subsection{Lower part}

Figure \ref{fig:real_robot_body} depicts the lower part of the robot, consisting of a single 3D-printed component. This part is equipped with capacitive electrodes affixed onto its surface, as elaborated in section \ref{capacitive_bumper_section}. Subsequent figures will showcase the integration of additional components such as electronics, motors, and wheels onto this lower part, demonstrating its gradual assembly.

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Body/1.png}
      \caption{Top view}
      \label{fig:body1}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Body/2.png}
      \caption{Bottom view}
      \label{fig:body2}
    \end{subfigure}
    \caption{Lower part of the robotic chassis}
    \label{fig:real_robot_body}
\end{figure}

\subsection{Upper part} \label{body_upper_part_subsection}

Figure \ref{fig:real_robot_top_cover} captures the upper part of the robot, showcasing its various connectors. The XT60 connectors serve a dual purpose. One of the XT60 connectors serves as the power supply for the Intel NUC, while the other functions as the charging port for the robot. The USB-C connector is designated for seamless communication with the robotic platform and can even power up the robot when sufficient power is available. The 3.5~mm jack connector enables the NUC to connect with its front panel connector. A momentary switch is also present, responsible for toggling the robot's power, as elaborated in section \ref{firmware_momentary_switch_subsection}. Furthermore, the yellow cables are linked to the START and STOP capacitive buttons. The upper part is equipped with a WS2812B LED ring that signalizes the robot's state through various visual signals.

\begin{figure}
    \centering
    \begin{subfigure}{1.0\textwidth}
      \centering
      \includegraphics[width=0.7\linewidth]{Files/Images/Real_robot/Upper_part/side.png}
      \caption{Side view}
      \label{fig:top_cover_side}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Upper_part/top.png}
      \caption{Top view}
      \label{fig:top_cover_top}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Upper_part/bottom.png}
      \caption{Bottom view}
      \label{fig:top_cover_bottom}
    \end{subfigure}
    \caption{Upper part of the robotic chassis}
    \label{fig:real_robot_top_cover}
\end{figure}



Figure \ref{fig:real_robot_top_cover_NUC} shows the side and top view of the upper part, fully equipped with all electronics. An Intel NUC is securely connected to the upper part through a user-friendly, easily detachable spring connector. The LED ring's light signalization is effectively diffused through a diffuser, situated between the upper part and the NUC. Additionally, positioned above the NUC is the RPLidar A3, a crucial component utilized for localization purposes. Furthermore, there is a 4-inch display mounted on the upper part. The display is featuring a capacitive touchscreen and is providing an interactive user interface for controlling and monitoring the robot's state.

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Upper_part_with_electronics/side.png}
      \caption{Side view}
      \label{fig:top_cover3}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Upper_part_with_electronics/top.png}
      \caption{Top view}
      \label{fig:top_cover4}
    \end{subfigure}
    \caption{Upper part of the robotic chassis with all electronics}
    \label{fig:real_robot_top_cover_NUC}
\end{figure}

\section{Motors and wheels} \label{motors_and_wheels_section}

Figure \ref{fig:real_robot_motor_with_wheel} represents the \gls{BLDC} motor module with the omnidirectional wheel. This module can be seamlessly installed between the lower and upper parts of the robot using 3~mm screws. In the event of any issues with the module, it can be conveniently detached from the robot and promptly replaced. The robot houses a total of three of these modules. Each module possesses three connectors, with one designated for motor connection to the robot, another for linking the AS5048A magnetic encoder to the robot, and the last one for an NTC thermistor that measures the motor's temperature.

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=1.0\linewidth]{Files/Images/Real_robot/Motor_with_wheel/1.png}
      \caption{Top view}
      \label{fig:motor_with_wheel1}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=1.0\linewidth]{Files/Images/Real_robot/Motor_with_wheel/2.png}
      \caption{Bottom view}
      \label{fig:motor_with_wheel2}
    \end{subfigure}
    \caption{Module of BLDC motor with omni wheel}
    \label{fig:real_robot_motor_with_wheel}
\end{figure}

Figure \ref{fig:real_robot_body_with_motors} showcases the \gls{BLDC} motor module with the omnidirectional wheels directly mounted onto the lower part of the robotic chassis.

\begin{figure}
  \centering
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Body_with_motors/side.png}
    \caption{Side view}
    \label{fig:body_with_motors1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Body_with_motors/top.png}
    \caption{Top view}
    \label{fig:body_with_motors2}
  \end{subfigure}
  \caption{Lower part of the robotic chassis with mounted BLDC motor modules}
  \label{fig:real_robot_body_with_motors}
\end{figure}

\subsection{Motor}

Figure \ref{fig:motor_parameters} shows the parameters and description of the \gls{BLDC} motor used in this educational robotic platform. This motor is commonly utilized as a gimbal motor, thus featuring a low motor velocity constant ($K_V = 55$). With a lower $K_V$ constant the motor is slower, but usually stronger. Due to its inherent characteristics, this motor does not necessitate a gearbox, allowing for direct mounting to the wheel. Furthermore, the motor is equipped with an integrated AS5048A 14-bit magnetic rotary encoder, which enables precise control of the motor position and velocity.

The choice of the \gls{BLDC} motor was primarily driven by its effectiveness, particularly when controlled using \gls{FOC}. This control technique allows the motor to deliver maximum strength with optimal efficiency, making it especially advantageous for battery-driven systems. Thanks to this type of control, the robot's operational time can be significantly extended, ensuring longer-lasting and more efficient performance.

\begin{figure}[H]
    \centering
    \includegraphics[width=1.0\linewidth]{Files/Images/Real_robot/Motor_with_wheel/motor_parameters.png}
    \caption{Motor parameters \cite{cite:selected_BLDC_motor}}
    \label{fig:motor_parameters}
\end{figure}

\subsection{Wheel} \label{wheels_subsection}

The omnidirectional robotic platform can be equipped with either Mecanum wheels or Omni wheels. Robots equipped with Mechanum wheels typically adopt a rectangular shape as they require a minimum of four wheels. Conversely, robots utilizing Omni wheels can function with a minimum of three wheels, which often leads to a circular configuration. The circular shape holds distinct advantages for students, providing seamless orientation and smooth navigation in complex environments, significantly simplifying path-planning tasks.
An additional benefit of the circular design is that it necessitates fewer motors, reducing the robot's current consumption, a critical aspect for battery-driven systems. Moreover, with fewer motors, the robot's internal space is larger, and the robot is lighter and cheaper. However, it is important to acknowledge certain drawbacks associated with this design. The robot's stability may be compromised due to only three contact points with the ground, requiring careful consideration of its center of gravity, symmetry, and balance. Furthermore, controlling a three-wheel-based robot proves more challenging compared to four-wheel-based counterparts, and the calculation of robot odometry becomes significantly more complex. Despite these challenges, the circular-shaped Omni-wheeled robot offers several compelling advantages for educational platforms, making it a favorable choice for this specific application.

The Omni wheel used in this design is displayed in figure \ref{fig:omniwheel}. It is constructed from a combination of aluminum alloy and rubber materials. The wheel has a diameter of 82 millimeters and is designed to withstand a load weight of up to 15 kilograms.

\begin{figure}
    \centering
    \begin{subfigure}{.7\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Motor_with_wheel/omniwheel_parameters.pdf}
      \caption{Omniwheel parameters}
      \label{fig:omniwheel_parameters}
    \end{subfigure}%
    \begin{subfigure}{.3\textwidth}
      \centering
      \raisebox{12mm}{
        \includegraphics[width=1.0\linewidth]{Files/Images/Real_robot/Motor_with_wheel/omniwheel.png}
      }
      \caption{Real omniwheel}
      \label{fig:omniwheel1}
    \end{subfigure}
    \caption{Omni directional wheel used in the robotic platform \cite{cite:omniwheel}}
    \label{fig:omniwheel}
\end{figure}

\section{Capacitive bumper} \label{capacitive_bumper_section}

The robot employs tactile sensors based on capacitance change to detect obstacles in its surrounding environment. Each tactile sensor consists of two conductive plates separated by an elastomeric separator, ensuring compliance. When pressure is exerted on the plates, there is a change in capacitance, which is proportional to the force applied. The robot is equipped with six tactile sensors in total, enabling it to perceive both the presence of obstacles and the direction of impact.

Figure \ref{fig:real_robot_body} displays all six capacitive directional electrodes, directly connected to the robot. The outer electrode is embedded within the silicone cover, as shown in figure \ref{fig:real_robot_capacitive_bumper}, and it is connected to the ground of the robot.

The robot measures the capacitance between these electrodes using its built-in \gls{TSC} that utilizes a charge transfer acquisition technique \cite{cite:TSC}. The connection of the outer electrode to the ground ensures that the capacitances of the directional electrodes remain unaffected when the robot is near people or other grounded objects. As a result, any change in capacitance is solely dependent on the pressure exerted on the capacitive bumper. This allows the robot to detect and respond to its surroundings, enhancing its ability to navigate safely in various environments.

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Capacitive_bumper/1.png}
      \caption{Side view}
      \label{fig:capacitive_bumper1}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.85\linewidth]{Files/Images/Real_robot/Capacitive_bumper/2.png}
      \caption{Top view}
      \label{fig:capacitive_bumper2}
    \end{subfigure}
    \vspace{0.2cm}
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Capacitive_bumper/3.png}
        \caption{Ground electrode}
        \label{fig:capacitive_bumper3}
      \end{subfigure}%
      \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Capacitive_bumper/4.png}
        \caption{Built in the robot}
        \label{fig:capacitive_bumper4}
      \end{subfigure}
    \caption{Capacitive bumper}
    \label{fig:real_robot_capacitive_bumper}
\end{figure}

The capacitive bumper is made from silicone using two mold forms. Between the molds is inserted copper fabric serving as the ground electrode. The bumper's inner wall features small pentahedrons, which serve both as springs and spacers. This component is then directly mounted onto the robot's body, providing an effective and resilient tactile sensing system.