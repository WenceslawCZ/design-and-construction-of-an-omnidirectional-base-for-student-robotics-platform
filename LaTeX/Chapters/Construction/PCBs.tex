\chapter{Printed Circuit Boards}

The \glspl{PCB} were designed using Altium Designer. The primary objective was to create a unified solution on a single board, eliminating the need for multiple modules connected by cables. This design approach resulted in a more robust, stable, and higher-frequency electronics system, while also simplifying maintenance procedures. By minimizing the number of cables, the robot's potential failure points were significantly reduced, enhancing overall reliability.

There are four custom-made \glspl{PCB} in total, but only three will be highlighted here. The central and most crucial \gls{PCB} is called the Main board. It contains all the essential electronics for the robot's operation and is responsible for the communication between the NUC and the motor drivers. The Driver board is the second \gls{PCB}, with three of them directly inserted into the Main board. The Driver boards are responsible for the precise control of the \gls{BLDC} motors. The third board is the Battery board, which acts as the connection interface between the batteries and the robot. The last \gls{PCB}, not mentioned in this section, is available in the git repository of this master's thesis. Its purpose is to facilitate testing of the Driver board with motors outside of the robot, providing a valuable tool for development and evaluation.

Since the robot is intended as an educational platform, special attention was given to facilitating easier debugging, signal processing, and signal visualization. Most of the pins on all the boards are accessible as outputs, making it simpler to visualize signals during debugging and analysis. Additionally, the \glspl{PCB} offer convenient access to all of their peripherals, allowing for the insertion of a wide range of sensors to enhance the robot's capabilities.

Figure \ref{fig:real_robot_body_with_motors_and_PCBs} showcases the created PCB boards mounted onto the lower part of the robotic chassis.

\begin{figure}
  \centering
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Body_with_motors_and_PCBs/1.png}
    \caption{Side view}
    \label{fig:body_with_motors_anc_PCBs1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{Files/Images/Real_robot/Body_with_motors_and_PCBs/2.png}
    \caption{Top view}
    \label{fig:body_with_motors_anc_PCBs2}
  \end{subfigure}
  \caption{Lower part of the robotic chassis with mounted BLDC motor modules, capacitive tactile bumper and created PCB boards}
  \label{fig:real_robot_body_with_motors_and_PCBs}
\end{figure}


\section{Main board PCB} \label{main_board_section}

\begin{figure}
    \centering
    \begin{subfigure}{1.0\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/PCBs/Main_board/side.png}
        \caption{Side view}
        \label{fig:main_board1}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/PCBs/Main_board/top.png}
      \caption{Top view}
      \label{fig:main_board2}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/PCBs/Main_board/bottom.png}
      \caption{Bottom view}
      \label{fig:main_board3}
    \end{subfigure}
    \caption{The Main board}
    \label{fig:real_robot_main_board}
\end{figure}


\subsection{Analog measurements} \label{PCB_analog_measurement}
The Main board performs multiple analog measurements to monitor crucial parameters of the robot. One of the crucial measurements includes monitoring the robot's voltage and battery voltages. These values are measured using a voltage divider, protected with a Zener diode. 

Additionally, the Main board is responsible for measuring seven current values. This includes current flowing from and to the batteries, current flowing to the Driver boards and motors, and the overall robot's current consumption. The overall consumption is measured with the help of the \gls{PROFET}, which is outputting current proportional to the actual consumption. The remaining currents are measured using Hall-Effect Linear Current Sensor ACS711, capable of measuring currents in the range of $\pm$ 15.5 amperes.

Temperature monitoring is another essential function of the Main board. It measures the temperatures of the motors, Driver boards, and the Main board itself. The temperatures of the motors are measured using NTC thermistors, while the temperature of the Main board is determined via a built-in thermometer inside the STM32 microcontroller. As for the Driver board temperatures, they are measured similarly but transmitted to the Main board using the \gls{SPI} protocol.

Furthermore, the Main board handles the measurement of eight capacitive tactile sensors. Six of these sensors are used for the capacitive bumper, allowing the robot to detect obstacles and respond to its surroundings effectively as discussed in section \ref{capacitive_bumper_section}. The remaining two sensors are utilized for the START and STOP buttons as mentioned in subsection \ref{body_upper_part_subsection}.

\subsection{Power managment} \label{main_board_power_managmnet_subsection}

As mentioned in the section \ref{battery_section}, the batteries are connected using Schottky diodes. Each Schottky diode can handle a current of up to 30 amperes. However, due to the significant heat generated at such high currents, the Schottky diodes require heatsinks for effective heat dissipation. To address this issue, the diodes are strategically placed close to each other and connected with a large common heatsink. This arrangement ensures efficient heat dissipation and prevents overheating. One route from the batteries could not be routed on the board and is instead connected with a cable, as shown in figure \ref{fig:main_board3}.

The current from the batteries then flows to the \gls{PROFET}, which serves as a smart high-side power switch. The \gls{PROFET} features reverse battery protection and has a load current capacity of 25 amperes, with a current limitation of 65 amperes. To manage thermal considerations, the \gls{PROFET} is also located under the heatsink for efficient heat dissipation.

As an additional safety measure, a 30-ampere fuse is positioned behind the \gls{PROFET}. This fuse serves to interconnect the \gls{PROFET} with the robot's voltage. From there, the voltage is distributed to the motors via the Driver board, to the step-down converters, which create lower voltages for the electrical components, and to the Intel NUC via the XT60 connector.

As highlighted in subsection \ref{battery_charger_subsection}, the other XT60 connector is intended for charging purposes and is connected to the battery chargers. The input voltage required for these chargers falls within the range of 17 to 32 volts. The charger is then connected to the battery via a battery protection and balancing board, ensuring safe and balanced charging.

For powering the robot on and off, a momentary switch is utilized. When the switch is pressed, the battery voltage is directed through a voltage divider to an \gls{NMOS}. The \gls{NMOS}, in turn, activates the \gls{PROFET} switch, allowing the robot to power on. Once powered on, the STM32 microcontroller, integrated into the Main board, takes over and maintains the \gls{PROFET} switch in the ON position until the user initiates the robot's power-off sequence, as described in section \ref{firmware_main_board_power_managment_section}. An important safety feature worth noting is that the voltage divider is designed to prevent the robot from turning itself on with voltages lower than 12.4 volts. This serves as a low-power protection mechanism for the batteries, ensuring that the robot does not activate with insufficient power, potentially causing damage to the batteries.


\subsection{Digital control} \label{main_board_digital_control_subsection}

To control the Intel NUC, the robot establishes a connection with NUC's front panel connector. This connection involves three pins, which include the front panel power LED for monitoring the robot's power status, a power switch for turning the NUC on and off as needed, and the NUC ground. This arrangement grants the robot greater control over the high-level control system.

For communication between the robot and the Intel NUC, a USB-C port is utilized. The robot has full control over the USB connection, and in case of any interruptions, it can disconnect and reconnect the USB to re-establish communication with the NUC. This reconnection process prompts the NUC to restart its communication scripts, ensuring a reliable connection.

The Driver boards are connected to the Main board through \gls{PCI} Express connectors. Each Driver board communicates with the Main board via its own \gls{SPI}. Additionally, the boards share a common clock source in the form of a precise 8 MHz crystal located on the Main board. The Main board provides this clock source to the Driver boards with its clock output capabilities \cite{cite:RCC_OUT}.

For signaling purposes, the Main board features a passive electromagnetic buzzer with a sound pressure level of 85 decibels, allowing the robot to audibly communicate its states. Furthermore, The Main board is also equipped with three WS2812B LEDs, strategically placed next to the Driver boards. This provides a visual indication of the states of each board. The Main board also includes a connector for adding more LEDs to the robot, as mentioned via the LED color ring in subsection \ref{body_upper_part_subsection}.

In addition to the above features, the Main board offers multiple connectors for various peripherals, including three unique \glspl{UART}, one \gls{I2C}, one \gls{CAN} bus, and three \glspl{SPI} used by the Driver boards. This versatility enables the direct connection of a wide range of sensors and modules to the robot board, further expanding its capabilities and potential applications.

\subsection{PCB schematic}

Figure \ref{fig:main_board_PCB_schematic} presents the schematic of the Main board \gls{PCB}. Due to its size and complexity, it is recommended, for a more detailed and clearer view, to refer to the digital version of this document, available in the git repository. The digital version provides a more comprehensive view of the schematic, facilitating a better understanding of the Main board's design and functionality.

% \clearpage

\begin{figure}[p]
    % \vspace{1.2cm}
    \begin{minipage}[c][0.94\textheight][t]{\linewidth}
        \hspace{0.14cm}
        \includegraphics[trim=0 0 395 0, clip, width=1.22\linewidth]{Files/Images/PCBs/SRobot_main/SRobot_main_schematic.pdf}
    \end{minipage}
\end{figure}    
\begin{figure}[p]
    \vspace{0.66cm}
    \begin{minipage}[c][0.94\textheight][t]{\linewidth}
        \hspace{-3.15cm}
        \includegraphics[trim=395 0 0 0, clip, width=1.22\linewidth]{Files/Images/PCBs/SRobot_main/SRobot_main_schematic.pdf}
        \caption{A schematic of the Main board}
        \label{fig:main_board_PCB_schematic}
    \end{minipage}
\end{figure}    

\subsection{PCB design}

The Main board is constructed as a four-layer \gls{PCB}, with each layer serving specific purposes. In figure \ref{fig:main_board_PCB_layers}, you can observe the different layers of the board. The top and middle-top layers are dedicated to the robot's ground connection, ensuring a robust and stable electrical grounding throughout the board. This ground connection helps minimize noise and interference, promoting smoother and more reliable operation. The middle-bottom layer is poured with 3.3 volts, providing a consistent power supply to the relevant components. Lastly, the bottom layer is poured with the battery power voltage coming from the fuse, serving as the primary power distribution layer for the board. This layer supplies power to the critical components such as the motors, Intel NUC, and other essential elements of the Main board.

\begin{figure}
    \centering
    \begin{subfigure}{1.0\textwidth}
      \centering
        \includegraphics[width=0.7\linewidth]{Files/Images/PCBs/SRobot_main/SRobot_main_PCB_TOP_LAYER.pdf}
        \vspace{0.1cm}
        \caption{Top layer}
        \label{fig:main_board_top_layer}
    \end{subfigure}\vspace{0.8cm}
    \begin{subfigure}{1.0\textwidth}
      \centering
        \includegraphics[width=0.7\linewidth]{Files/Images/PCBs/SRobot_main/SRobot_main_PCB_MIDDLE_TOP_LAYER.pdf}
        \vspace{0.1cm}
        \caption{Middle-top layer}
        \label{fig:main_board_middle-top_layer}
    \end{subfigure}
\end{figure}
\begin{figure}\ContinuedFloat
    \begin{subfigure}{1.0\textwidth}
        \centering
        \includegraphics[width=0.7\linewidth]{Files/Images/PCBs/SRobot_main/SRobot_main_PCB_MIDDLE_BOTTOM_LAYER.pdf}\vspace{0.1cm}
        \caption{Middle-bottom layer}
        \label{fig:main_board_middle-bottom_layer}
    \end{subfigure}\vspace{0.8cm}
    \begin{subfigure}{1.0\textwidth}
        \centering
        \includegraphics[width=0.7\linewidth]{Files/Images/PCBs/SRobot_main/SRobot_main_PCB_BOTTOM_LAYER.pdf}
        \vspace{0.1cm}
        \caption{Bottom layer}
        \label{fig:main_board_bottom_layer}
    \end{subfigure}
    \caption{Different layers of the Main board PCB}
    \label{fig:main_board_PCB_layers}
\end{figure}

\clearpage


\section{Driver board PCB} \label{driver_board_section}

\begin{figure}
    \centering
    \begin{subfigure}{1.0\textwidth}
        \centering
        \includegraphics[width=0.6\linewidth]{Files/Images/Real_robot/PCBs/Driver_board/top.png}
        \caption{Top view}
        \label{fig:driver_board_top_view}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/PCBs/Driver_board/bottom.png}
      \caption{Bottom view}
      \label{fig:driver_board_bottom_view}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/Real_robot/PCBs/Driver_board/bottom_with_heatsink.png}
      \caption{Bottom view with heatsink}
      \label{fig:driver_board_bottom_view_with_heatsink}
    \end{subfigure}
    \caption{The Driver board}
    \label{fig:real_robot_driver_board}
\end{figure}

The Driver board utilizes a DRV8311H three-phase \gls{PWM} motor driver for controlling the \gls{BLDC} motor. This motor driver features three integrated MOSFET half-H-bridges specifically designed for driving a three-phase brushless DC motor. The DRV8311H is equipped with three current-sense amplifiers with integrated current sensing, enabling accurate sensing of the three phase currents of the \gls{BLDC} motors. This feature ensures optimal \gls{FOC} and accurate current-control system implementation \cite{cite:DRV8311}. The DRV8311H motor driver operates within a recommended voltage range of 3V to 20V, with an absolute maximum voltage of 24V. It is capable of delivering high output current, with a peak current drive of up to 5 amperes. Additionally, the DRV8311H driver offers various built-in protection features, among the most important belongs overcurrent protection and thermal warning and shutdown protection. These protection mechanisms enhance the safety and reliability of the driver during operation, safeguarding the motor from potential overloads and overheating.

Moreover, the functionality of the DRV8311H motor driver can be modified by the resistors located at the bottom of the Driver board visible in the figure \ref{fig:driver_board_bottom_view}. These settings include modifying the gain of the current-sense amplifier, adjusting the slew rate of the integrated MOSFET half-H-bridges, and selecting different control modes of the motor driver. 

The Driver board is designed as a 32-pin \gls{PCI} Express card, which can be directly inserted into the corresponding \gls{PCI} Express slot on the Main board. This vertical insertion layout not only saves space but also offers convenient repairability. If any unexpected issues arise the Driver board can be easily unplugged and replaced with another, minimizing downtime and simplifying maintenance procedures. Additionally, this design choice contributes to cost-effectiveness, as using the \gls{PCI} Express slot as the connection interface is more economical compared to using other dedicated \gls{PCB} edge connectors. However, it is essential to acknowledge one potential drawback, which occurs when a large and heavy card is inserted. In such cases, the connector may become slightly unstable, leading to occasional disconnection of some pins. Fortunately, this is not an issue for the Driver board since it has a similar size to the \gls{PCI} Express connector and is lightweight, which ensures a stable and reliable connection.

As discussed in section \ref{main_board_section}, communication between each Driver board and the Main board is achieved through individual \gls{SPI} connections. Additionally, the boards share several common pins for synchronization and control purposes. One of these shared pins provides the Driver boards with a precise 8 MHz clock source from the Main board. This clock source ensures accurate and synchronized timing for their operations. Furthermore, two pins are dedicated to synchronization between the Driver boards. The first pin facilitates velocity synchronization, ensuring that all the motors change the speed at the same time. The second pin enables synchronized brake functionality, allowing all the motors to stop simultaneously. In addition, the boards share a reset pin, allowing for resetting of all boards simultaneously. This simplifies the reset process and enhances the overall functionality of the system. Lastly, each board shares a unique sleep pin with the Main board. This sleep pin allows the Main board to control the power supply of the motors, enabling the robot to turn the motor power on and off as needed.

\subsection{PCB schematic}

Figure \ref{fig:driver_board_PCB_schematic} presents the schematic of the Driver board \gls{PCB}. Due to its size and complexity, it is recommended, for a more detailed and clearer view, to refer to the digital version of this document, available in the git repository. The digital version provides a more comprehensive view of the schematic, facilitating a better understanding of the Main board's design and functionality. 

\begin{figure}[p]
    % \vspace{1.2cm}
    \begin{minipage}[c][0.98\textheight][t]{\linewidth}
        \includegraphics[trim=0 0 385 0, clip, width=1.2\linewidth]{Files/Images/PCBs/SRobot_driver/SRobot_driver_schematic.pdf}
    \end{minipage}
\end{figure}    
\begin{figure}[p]
    \vspace{0.222cm}
    \begin{minipage}[c][0.98\textheight][t]{\linewidth}
        \hspace{-2.6cm}
        \includegraphics[trim=385 0 0 0, clip, width=1.2\linewidth]{Files/Images/PCBs/SRobot_driver/SRobot_driver_schematic.pdf}
        \caption{A schematic of the Driver board}
        \label{fig:driver_board_PCB_schematic}
    \end{minipage}
\end{figure}    

\subsection{PCB design}

To enhance the cooling efficiency of the DRV8311 motor driver, the thermal pad of the driver is connected to the ground plane of the \gls{PCB}, which is poured over all four layers of the board visible in figure \ref{fig:driver_board_PCB_layers}. This ground plane acts as a large thermal conductor, aiding in the dissipation of heat generated by the motor driver. Additionally, the board has a dedicated space reserved for an 8.8~mm heatsink, positioned directly beneath the DRV8311 driver, as seen in figure \ref{fig:real_robot_driver_board}. Although the current setup may not require the use of the heatsink, it becomes crucial when dealing with input voltages above 20V, as it helps further dissipate heat and maintain optimal performance. The connection of the ground plane across all layers ensures efficient thermal distribution, which enables the integrated STM32 to monitor the temperature of the DRV8311 motor driver using its built-in thermometer. This temperature monitoring mechanism helps maintain the stability and reliability of the motor driver during operation.

\begin{figure}
    \centering
    \begin{subfigure}{0.5\textwidth}
      \centering
        \includegraphics[width=0.94\linewidth]{Files/Images/PCBs/SRobot_driver/SRobot_driver_PCB_TOP_LAYER.pdf}
        \caption{Top layer}
        \vspace{0.4cm}%
        \label{fig:driver_board_top_layer}
    \end{subfigure}%
    \begin{subfigure}{0.5\textwidth}
      \centering
        \includegraphics[width=0.94\linewidth]{Files/Images/PCBs/SRobot_driver/SRobot_driver_PCB_MIDDLE_TOP_LAYER.pdf}
        \caption{Middle-top layer}
        \vspace{0.4cm}%
        \label{fig:driver_board_middle-top_layer}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \centering
        \includegraphics[width=0.94\linewidth]{Files/Images/PCBs/SRobot_driver/SRobot_driver_PCB_MIDDLE_BOTTOM_LAYER.pdf}
        \caption{Middle-bottom layer}
        \label{fig:driver_board_middle-bottom_layer}
    \end{subfigure}%
    \begin{subfigure}{0.5\textwidth}
        \centering
        \includegraphics[width=0.94\linewidth]{Files/Images/PCBs/SRobot_driver/SRobot_driver_PCB_BOTTOM_LAYER.pdf}
        \caption{Bottom layer}
        \label{fig:driver_board_bottom_layer}
    \end{subfigure}
    \caption{Different layers of the Driver board PCB}
    \label{fig:driver_board_PCB_layers}
\end{figure}

\clearpage

\section{Battery board PCB}

The Battery board, shown in figure \ref{fig:real_robot_battery_board}, serves as a crucial link between the battery and the Main board. The board features a single connector that allows the battery to connect to the Main board. As discussed in section \ref{battery_connectors_section}, this connector is capable of delivering an average current of 21 amperes, with occasional peaks reaching a maximum of 30 amperes, ensuring stable and sufficient power delivery for the robot's operation.

Once connected, the Battery board serves as an intermediary, linking the battery to both the battery balancer and the battery charger, as mentioned in section \ref{battery_section}. The Battery board is securely mounted onto the battery holder using two 3~mm screws. The battery power and ground connections are welded into the Battery board using a nickel sheet plate, ensuring robust and reliable connections. Additionally, the remaining battery cells are soldered with cables to the board, completing the electrical circuit.

Apart from the main connector, there are also XT60 and XH2.54 connectors visible on the board. Although they do not have a current use in this specific configuration, they were intended for potential future expansions where the battery may serve different purposes or be connected to other components.

\begin{figure}
    \centering
    \includegraphics[width=0.4\linewidth]{Files/Images/Real_robot/PCBs/Battery_board/top.png}
    \caption{The Battery board}
    \label{fig:real_robot_battery_board}
\end{figure}

\subsection{PCB design}

As mentioned, the Battery board serves a straightforward purpose of connecting the battery cells to the connector that supplies power to the robot. Due to its simple functionality, the \gls{PCB} design for the Battery board is kept relatively uncomplicated. The board consists of only two layers, as depicted in figure \ref{fig:driver_board_PCB_layers}. One side of the board is dedicated to power, while the other side is dedicated to ground

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/PCBs/SRobot_battery/SRobot_battery_PCB_TOP_LAYER.pdf}
      \caption{Top layer}
      \label{fig:driver_board_top_layer}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.8\linewidth]{Files/Images/PCBs/SRobot_battery/SRobot_battery_PCB_BOTTOM_LAYER.pdf}
      \caption{Bottom layer}
      \label{fig:driver_board_bottom_layer}
    \end{subfigure}
    \caption{Different layers of the Battery board PCB}
    \label{fig:driver_board_PCB_layers}
\end{figure}