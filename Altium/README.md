# Altium

This folder contains the Altium files and resources for the master's thesis project titled "Design and Construction of an Omnidirectional Base for Student Robotics Platform." The Altium files pertain to the PCB design and schematics of the electronics for the omnidirectional robot base.
