# Firmware

This folder contains the firmware code and resources for the master's thesis project titled "Design and Construction of an Omnidirectional Base for Student Robotics Platform." The firmware code is responsible for controlling the microcontroller that manages the motors and other electronic components of the robot base.

