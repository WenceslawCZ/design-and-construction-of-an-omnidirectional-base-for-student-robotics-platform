/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCU_FAULT_Pin GPIO_PIN_9
#define MCU_FAULT_GPIO_Port GPIOB
#define MOTOR_BRAKE_Pin GPIO_PIN_15
#define MOTOR_BRAKE_GPIO_Port GPIOC
#define MOTOR_BRAKE_EXTI_IRQn EXTI4_15_IRQn
#define SOA_Pin GPIO_PIN_0
#define SOA_GPIO_Port GPIOA
#define SOB_Pin GPIO_PIN_1
#define SOB_GPIO_Port GPIOA
#define SOC_Pin GPIO_PIN_2
#define SOC_GPIO_Port GPIOA
#define SYNC_Pin GPIO_PIN_3
#define SYNC_GPIO_Port GPIOA
#define SYNC_EXTI_IRQn EXTI2_3_IRQn
#define ENCODER_SPI_MOSI_Pin GPIO_PIN_4
#define ENCODER_SPI_MOSI_GPIO_Port GPIOA
#define ENCODER_SPI_CS_Pin GPIO_PIN_5
#define ENCODER_SPI_CS_GPIO_Port GPIOA
#define ENCODER_PWM_Pin GPIO_PIN_6
#define ENCODER_PWM_GPIO_Port GPIOA
#define INLA_Pin GPIO_PIN_7
#define INLA_GPIO_Port GPIOA
#define INLB_Pin GPIO_PIN_0
#define INLB_GPIO_Port GPIOB
#define INLC_Pin GPIO_PIN_1
#define INLC_GPIO_Port GPIOB
#define ENCODER_SPI_MISO_Pin GPIO_PIN_2
#define ENCODER_SPI_MISO_GPIO_Port GPIOB
#define INHA_Pin GPIO_PIN_8
#define INHA_GPIO_Port GPIOA
#define INHB_Pin GPIO_PIN_9
#define INHB_GPIO_Port GPIOA
#define MOTOR_GAIN_Pin GPIO_PIN_6
#define MOTOR_GAIN_GPIO_Port GPIOC
#define INHC_Pin GPIO_PIN_10
#define INHC_GPIO_Port GPIOA
#define MOTOR_NSLEEP_Pin GPIO_PIN_11
#define MOTOR_NSLEEP_GPIO_Port GPIOA
#define MOTOR_NSLEEP_EXTI_IRQn EXTI4_15_IRQn
#define MOTOR_NFAULT_Pin GPIO_PIN_12
#define MOTOR_NFAULT_GPIO_Port GPIOA
#define MOTOR_NFAULT_EXTI_IRQn EXTI4_15_IRQn
#define MAIN_BOARD_SPI_CS_Pin GPIO_PIN_15
#define MAIN_BOARD_SPI_CS_GPIO_Port GPIOA
#define MAIN_BOARD_SPI_SCK_Pin GPIO_PIN_3
#define MAIN_BOARD_SPI_SCK_GPIO_Port GPIOB
#define MAIN_BOARD_SPI_MISO_Pin GPIO_PIN_4
#define MAIN_BOARD_SPI_MISO_GPIO_Port GPIOB
#define MAIN_BOARD_SPI_MOSI_Pin GPIO_PIN_5
#define MAIN_BOARD_SPI_MOSI_GPIO_Port GPIOB
#define ENCODER_SPI_SCK_Pin GPIO_PIN_8
#define ENCODER_SPI_SCK_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

#define TS_CAL1_CAL_ADDR     ((uint16_t*) (0x1FFF75A8U))
#define VREFINT_CAL_ADDR     ((uint16_t*) (0x1FFF75AAU))


#define TWO_PI 6.28318530718
#define	TWO_THIRD_PI	2.09439510239  // (2.0/3.0)*M_PI

#define PID_VELOCITY_K_P	25.0
#define	PID_VELOCITY_K_I	2500.0
#define	PID_VELOCITY_K_D	0.002
#define	PID_VELOCITY_ANTIWINDUP	5.0

#define PID_POSITION_K_P	6000.0
#define	PID_POSITION_K_I	3500.0
#define	PID_POSITION_K_D	0.0
#define	PID_POSITION_ANTIWINDUP	0.1

#define VELOCITY_LOW_PASS_FILTER	0.95

#define MOTOR_NUMBER_OF_POLES	11.0
#define MOTOR_PWM_HALF_COUNTER	500.0

#define SECONDS(x) (x*0.000001000000000000)

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
