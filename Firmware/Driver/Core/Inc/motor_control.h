/*
 * motor_control.h
 *
 *  Created on: Jul 7, 2023
 *      Author: vaclav
 */

#ifndef INC_MOTOR_CONTROL_H_
#define INC_MOTOR_CONTROL_H_

#define SIN_TWO_THIRD_PI	0.86602540378
#define COS_TWO_THIRD_PI	-0.5

#define	VELOCITY_CONTROLLER	0
#define	POSITION_CONTROLLER	1

#include "main.h"
#include "trigonometrical_tables.h"
#include "encoder.h"
#include <stdbool.h>
#include <math.h>



struct Motor_control_struct {
	double motor_position;
	double max_power;
	bool   motor_software_brake;
	bool   motor_hardware_brake;
	double angle_reference;
	double velocity_reference;
	double PID_error;
	double PID_previous_error;
	double PID_error_sum;
	double PID_k_p;
	double PID_k_i;
	double PID_k_d;
	double PID_antiwindup;
	double motor_power;
};

void calibrate_motor_encoder_position();
void set_motor_power(float angle, float power) ;
void set_controller(uint8_t controller_type);

#endif /* INC_MOTOR_CONTROL_H_ */
