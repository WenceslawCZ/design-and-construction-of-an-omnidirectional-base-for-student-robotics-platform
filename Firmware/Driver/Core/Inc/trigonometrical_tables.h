/*
 * trigonometrical_tables.h
 *
 *  Created on: Jul 7, 2023
 *      Author: vaclav
 */

#ifndef INC_TRIGONOMETRICAL_TABLES_H_
#define INC_TRIGONOMETRICAL_TABLES_H_

#define	MAX_SINE_TABLE_VALUES 1000

#include "main.h"
#include <math.h>

void fill_sine_table();

float find_sine_value(double angle);

float find_cosine_value(double angle);

void find_sine_cosine_value(double angle, float* sinus, float* cosinus);

#endif /* INC_TRIGONOMETRICAL_TABLES_H_ */
