/*
 * encoder.h
 *
 *  Created on: Jan 22, 2023
 *      Author: vaclav
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include "main.h"
#include <math.h>
#include "stdint.h"
#include <stdbool.h>

extern SPI_HandleTypeDef hspi2;
extern TIM_HandleTypeDef htim3;

#define	AS5048A_CS_PORT	ENCODER_SPI_CS_GPIO_Port
#define	AS5048A_CS_PIN	ENCODER_SPI_CS_Pin

#define		AS5048A_READ					1<<14
#define		AS5048A_PARITY					1<<15
#define		AS5048A_NOP						AS5048A_PARITY | AS5048A_READ | 0x0000
#define		AS5048A_CLEAR_ERROR_FLAG		AS5048A_READ | 0x0001
#define		AS5048A_PROGRAMMING_CONTROL		0x0003										// This is not set up properly
#define		AS5048A_OTP_HIGH				0x0016										// This is not set up properly
#define		AS5048A_OTP_LOW					0x0017										// This is not set up properly
#define		AS5048A_DIAGNOSTICS				0x0003										// This is not set up properly
#define		AS5048A_MAGNITUDE				AS5048A_READ | 0x3FFE
#define		AS5048A_ANGLE					AS5048A_PARITY | AS5048A_READ | 0x3FFF
#define		AS5048A_ERROR_FLAG				1<<14
#define		AS5048A_PARITY_ERROR			1<<2
#define		AS5048A_COMMAND_INVALID			1<<1
#define		AS5048A_FLAMING_ERROR			1<<0

struct Encoder_struct {
	uint32_t IC_value;
	double previos_angle;
	double actual_angle;
	double motor_offset;
	int32_t number_of_rotation;
	double reset_position;
	double average_velocity;
	double velocity_low_pass_filter;
};

void update_position(double time_period);
double encoder_read_angle();

uint16_t AS5048A_receive(uint16_t address);
bool checkParity(uint16_t data);
bool checkFlag(uint16_t data);

void initialize_AS5048A_PWM_readings_timer();
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim);

#endif /* ENCODER_H_ */
