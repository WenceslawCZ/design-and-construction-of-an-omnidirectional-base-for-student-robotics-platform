/*
 * trigonometrical_tables.c
 *
 *  Created on: Jul 7, 2023
 *      Author: vaclav
 */

#include "Trigonometrical_tables.h"

float sine_table[MAX_SINE_TABLE_VALUES];

void fill_sine_table() {
	for (int i = 0; i < MAX_SINE_TABLE_VALUES; i++) {
		sine_table[i] = sin(((double)i/(double)MAX_SINE_TABLE_VALUES)*M_PI_2);
	}
}

float find_sine_value(double angle) {
	while (angle > TWO_PI) {
		angle -= TWO_PI;
	}
	while (angle < 0) {
		angle += TWO_PI;
	}
	if (angle > M_PI) {
		angle -= M_PI;
		if (angle > M_PI_2) {
			return -sine_table[(int16_t)(((M_PI - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
		else {
			return -sine_table[(int16_t)((angle / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
	}
	else {
		if (angle > M_PI_2) {
			return sine_table[(int16_t)(((M_PI - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
		else {
			return sine_table[(int16_t)((angle / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
	}
}

float find_cosine_value(double angle) {
	while (angle > TWO_PI) {
		angle -= TWO_PI;
	}
	while (angle < 0) {
		angle += TWO_PI;
	}
	if (angle > M_PI) {
		angle -= M_PI;
		if (angle > M_PI_2) {
			return sine_table[(int16_t)(((angle - M_PI_2) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
		else {
			return -sine_table[(int16_t)(((M_PI_2 - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
	}
	else {
		if (angle > M_PI_2) {
			return -sine_table[(int16_t)(((angle - M_PI_2) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
		else {
			return sine_table[(int16_t)(((M_PI_2 - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
		}
	}
}

void find_sine_cosine_value(double angle, float* sinus, float* cosinus) {
	while (angle > TWO_PI) {
		angle -= TWO_PI;
	}
	while (angle < 0) {
		angle += TWO_PI;
	}
	if (angle > M_PI) {
		angle -= M_PI;
		if (angle > M_PI_2) {
			*sinus = -sine_table[(int16_t)(((M_PI - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			*cosinus = sine_table[(int16_t)(((angle - M_PI_2) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			return;
		}
		else {
			*sinus = -sine_table[(int16_t)((angle / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			*cosinus = -sine_table[(int16_t)(((M_PI_2 - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			return;
		}
	}
	else {
		if (angle > M_PI_2) {
			*sinus = sine_table[(int16_t)(((M_PI - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			*cosinus = -sine_table[(int16_t)(((angle - M_PI_2) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			return;
		}
		else {
			*sinus = sine_table[(int16_t)((angle / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			*cosinus = sine_table[(int16_t)(((M_PI_2 - angle) / M_PI_2) * (MAX_SINE_TABLE_VALUES-1))];
			return;
		}
	}
}
