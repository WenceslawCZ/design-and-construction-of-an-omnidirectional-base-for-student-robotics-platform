/*
 * motor_control.c
 *
 *  Created on: Jul 7, 2023
 *      Author: vaclav
 */

#include "motor_control.h"

struct Motor_control_struct motor_control;
extern struct Encoder_struct encoder;

extern uint32_t acceleration_time;
extern float received_velocity_reference;
extern double velocity_increment;


void calibrate_motor_encoder_position() {
	// Calibrate motor and encoder position
	for (int i = 0; i  < MOTOR_PWM_HALF_COUNTER/2; i++) {
		set_motor_power(0.0, i);
		HAL_Delay(2);
	}

	HAL_Delay(100);

	double encoder_offset = 0.0;
	for (int i = 0; i < 1000; i++) {
		set_motor_power(0.0, MOTOR_PWM_HALF_COUNTER/2);
		HAL_Delay(1);

		encoder_offset += encoder_read_angle();
	}

	encoder.motor_offset = encoder_offset / 1000.0;
	encoder.actual_angle = encoder.motor_offset;
	encoder.reset_position = encoder.actual_angle;
	encoder.previos_angle = encoder.actual_angle;
	encoder.number_of_rotation = 0;

	// Check if the motor was calibrated correctly
	TIM16->CNT = 0;
	double update_time_period = 0.0;
	for (int i = 0; i < 500; i+=5) {
		encoder.actual_angle = encoder_read_angle();
		motor_control.motor_position = encoder.actual_angle + TWO_PI * encoder.number_of_rotation - encoder.reset_position;
		update_time_period = SECONDS(TIM16->CNT);
		TIM16->CNT = 0;
		update_position(update_time_period);
		float angle = - encoder.motor_offset*MOTOR_NUMBER_OF_POLES + encoder.actual_angle*MOTOR_NUMBER_OF_POLES;
		set_motor_power(angle, i);
		HAL_Delay(1);

	}

	if (fabs(motor_control.motor_position) > 0.3) {
		calibrate_motor_encoder_position();
	}
}

void set_motor_power(float angle, float power) {
	float sinus_of_angle, cosinus_of_angle;
	find_sine_cosine_value(angle, &sinus_of_angle, &cosinus_of_angle);

	float first_motor_power = power*sinus_of_angle;
	float two_third_motor_power = power * cosinus_of_angle * SIN_TWO_THIRD_PI;

	TIM1->CCR1 = MOTOR_PWM_HALF_COUNTER + first_motor_power;
	TIM1->CCR2 = MOTOR_PWM_HALF_COUNTER + first_motor_power * COS_TWO_THIRD_PI + two_third_motor_power;
	TIM1->CCR3 = MOTOR_PWM_HALF_COUNTER + first_motor_power * COS_TWO_THIRD_PI - two_third_motor_power;

//	double dir = -M_PI_2 - motor_encoder.motor_offset*NUMBER_OF_POLES + motor_encoder.actual_angle*NUMBER_OF_POLES;
//	double u_a = MIDDLE_VOLTAGE_PWM + output*sin(dir);
//	double u_b = MIDDLE_VOLTAGE_PWM + output*sin(dir + TWO_THIRD_PI);
//	double u_c = MIDDLE_VOLTAGE_PWM + output*sin(dir - TWO_THIRD_PI);
//	TIM1->CCR1 = u_a;
//	TIM1->CCR2 = u_b;
//	TIM1->CCR3 = u_c;
}

void set_controller(uint8_t controller_type) {
	switch(controller_type) {
		case VELOCITY_CONTROLLER:
			motor_control.PID_k_p = PID_VELOCITY_K_P;
			motor_control.PID_k_i = PID_VELOCITY_K_I;
			motor_control.PID_k_d = PID_VELOCITY_K_D;
			motor_control.PID_antiwindup = PID_VELOCITY_ANTIWINDUP;
			motor_control.max_power = MOTOR_PWM_HALF_COUNTER;
			motor_control.velocity_reference = 0.0;
			motor_control.PID_error_sum = 0;
			encoder.average_velocity = 0;
			acceleration_time = 0;
			received_velocity_reference = 0.0;
			velocity_increment = 0.0;
			break;

		case POSITION_CONTROLLER:
			motor_control.PID_k_p = PID_POSITION_K_P;
			motor_control.PID_k_i = PID_POSITION_K_I;
			motor_control.PID_k_d = PID_POSITION_K_D;
			motor_control.PID_antiwindup = PID_POSITION_ANTIWINDUP;
			motor_control.max_power = MOTOR_PWM_HALF_COUNTER;
			motor_control.angle_reference = motor_control.motor_position;
			motor_control.PID_error_sum = 0;
			break;
		default:
			set_controller(VELOCITY_CONTROLLER);
			break;
	}
}
