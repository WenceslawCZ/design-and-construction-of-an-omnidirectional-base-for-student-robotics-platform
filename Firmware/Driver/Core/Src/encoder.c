/*
 * encoder.c
 *
 *  Created on: Jan 22, 2023
 *      Author: vaclav
 */

#include <encoder.h>

struct Encoder_struct encoder;

void update_position(double time_period) {
	double angle_difference = encoder.previos_angle - encoder.actual_angle;
	encoder.previos_angle = encoder.actual_angle;

	double motor_velocity;

	if (angle_difference > M_PI) {
		encoder.number_of_rotation++;
		motor_velocity = (TWO_PI - angle_difference)/time_period;
	}
	else if (angle_difference < -M_PI) {
		encoder.number_of_rotation--;
		motor_velocity = (-TWO_PI - angle_difference)/time_period;
	}
	else {
		motor_velocity = angle_difference/time_period;
	}

	encoder.average_velocity = encoder.velocity_low_pass_filter * encoder.average_velocity + (1.0 - encoder.velocity_low_pass_filter) * motor_velocity;
}

double encoder_read_angle() {
	uint16_t data = AS5048A_receive(AS5048A_ANGLE);
//	while (checkFlag(data) || checkParity(data)) {
//		data = AS5048A_receive(AS5048A_CLEAR_ERROR_FLAG);
//		uint16_t error_flag = AS5048A_receive(AS5048A_ANGLE);
//		if (error_flag & AS5048A_PARITY_ERROR) {
//			printf("\r AS5048A ERROR: Parity error \n");
//		}
//		else if (error_flag & AS5048A_COMMAND_INVALID) {
//			printf("\r AS5048A ERROR: Command invalid \n");
//		}
//		else if (error_flag & AS5048A_FLAMING_ERROR) {
//			printf("\r AS5048A ERROR: Framing error \n");
//		}
//	}
	return (TWO_PI/16383.0)*((double)(data & 0x3FFF));
}

uint16_t AS5048A_receive(uint16_t address) {
	HAL_GPIO_WritePin(ENCODER_SPI_CS_GPIO_Port, ENCODER_SPI_CS_Pin, GPIO_PIN_RESET);

	uint8_t pTxData[2], pRxData[2];
	pTxData[0] = address >> 8;
	pTxData[1] = address & 0xFF;

	HAL_SPI_TransmitReceive(&hspi2, pTxData, pRxData, 2, 5);

	HAL_GPIO_WritePin(ENCODER_SPI_CS_GPIO_Port, ENCODER_SPI_CS_Pin, GPIO_PIN_SET);

	return pRxData[0]<<8 | pRxData[1];
}

bool checkParity(uint16_t data) {
    bool parity = false;
    while (data) {
        parity = !parity;
        data = data & (data - 1);
    }
    return parity;
}

bool checkFlag(uint16_t data) {
    return (data & AS5048A_ERROR_FLAG);
}


void initialize_AS5048A_PWM_readings_timer () {
	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_IC_Start_IT(&htim3, TIM_CHANNEL_1);
    HAL_TIM_IC_Start(&htim3, TIM_CHANNEL_2);
}

// This measurement is done by AS5048A PWM - Timer 3 needs to be initialized, SPI readings disabled (function - encoder_read_angle())
// Beware that this is less precise measurements that SPI measurements
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM3) {
		if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
			encoder.IC_value = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);

			if (encoder.IC_value != 0) {
				uint32_t IC_value2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
				encoder.actual_angle = ((double)IC_value2 * TWO_PI)/((double)encoder.IC_value);
			}
		}
	}
}
