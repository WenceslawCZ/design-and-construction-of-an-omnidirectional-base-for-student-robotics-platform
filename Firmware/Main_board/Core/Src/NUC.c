/*
 * NUC.c
 *
 *  Created on: Jul 20, 2023
 *      Author: vaclav
 */

#import "NUC.h"

uint32_t current_update_time;
uint32_t voltage_update_time;
uint32_t temperature_update_time;
uint32_t capacitive_buttons_update_time;
uint32_t motor_position_update_time;
uint32_t motor_velocity_update_time;

char data[USB_SEND_BUFFER_SIZE];
char USB_receive_buffer[USB_RECEIVE_BUFFER_SIZE];

extern struct driver_SPI M1_driver_SPI;
extern struct driver_SPI M2_driver_SPI;
extern struct driver_SPI M3_driver_SPI;
extern struct capacitive_values capacitive_buttons;
extern struct current_values currents;
extern struct voltage_values voltages;
extern struct temperature_values temperatures;

void USB_connection(uint8_t state) {
	switch(state) {
		case USB_DISABLED:
			HAL_GPIO_WritePin(USB_EN_GPIO_Port, USB_EN_Pin, GPIO_PIN_RESET);
			break;
		case USB_ENABLED:
			HAL_GPIO_WritePin(USB_EN_GPIO_Port, USB_EN_Pin, GPIO_PIN_SET);
			break;
		default:
			break;
	}
}

void send_status() {
	if((uint32_t)(HAL_GetTick() - current_update_time) > CURRENT_UPDATE_PERIOD){
		current_update_time = HAL_GetTick();
		sprintf(data, "C;%0.3f;%0.3f;%0.3f;%0.3f;%0.3f;%0.3f;%0.3f\n", currents.current[0], currents.current[1], currents.current[2], currents.current[3], currents.current[4], currents.current[5], currents.current[6], currents.current[7]);
		CDC_Transmit_FS((uint8_t *)data, strlen(data));
	}

	if((uint32_t)(HAL_GetTick() - voltage_update_time) > VOLTAGE_UPDATE_PERIOD){
		voltage_update_time = HAL_GetTick();
		sprintf(data, "V;%0.3f;%0.3f;%0.3f;%0.3f\n", voltages.voltage[0], voltages.voltage[1], voltages.voltage[2], voltages.voltage[3]);
		CDC_Transmit_FS((uint8_t *)data, strlen(data));
	}

	if((uint32_t)(HAL_GetTick() - temperature_update_time) > TEMPERATURE_UPDATE_PERIOD){
		temperature_update_time = HAL_GetTick();
		sprintf(data, "T;%0.3f;%0.3f;%0.3f;%0.3f;%0.3f;%0.3f;%0.3f\n", temperatures.motors[0], temperatures.motors[1], temperatures.motors[2], temperatures.drivers[0], temperatures.drivers[1], temperatures.drivers[2], temperatures.robot);
		CDC_Transmit_FS((uint8_t *)data, strlen(data));
	}

	if((uint32_t)(HAL_GetTick() - capacitive_buttons_update_time) > CAPACITIVE_BUTTONS_UPDATE_PERIOD){
		capacitive_buttons_update_time = HAL_GetTick();
		sprintf(data, "B;%0.2f;%0.2f;%0.2f;%0.2f;%0.2f;%0.2f;%0.2f;%0.2f\n", capacitive_buttons.pressure[0], capacitive_buttons.pressure[1], capacitive_buttons.pressure[2], capacitive_buttons.pressure[3], capacitive_buttons.pressure[4], capacitive_buttons.pressure[5], capacitive_buttons.pressure[6], capacitive_buttons.pressure[7]);
		CDC_Transmit_FS((uint8_t *)data, strlen(data));
	}

	if((uint32_t)(HAL_GetTick() - motor_position_update_time) > MOTOR_POSITION_UPDATE_PERIOD){
		motor_position_update_time = HAL_GetTick();
		sprintf(data, "P;%0.3f;%0.3f;%0.3f\n", M1_driver_SPI.receive_message.message.position, M2_driver_SPI.receive_message.message.position, M3_driver_SPI.receive_message.message.position);
		CDC_Transmit_FS((uint8_t *)data, strlen(data));
	}

	if((uint32_t)(HAL_GetTick() - motor_velocity_update_time) > MOTOR_VELOCITY_UPDATE_PERIOD){
		motor_velocity_update_time = HAL_GetTick();
		sprintf(data, "S;%0.3f;%0.3f;%0.3f\n", M1_driver_SPI.receive_message.message.velocity, M2_driver_SPI.receive_message.message.velocity, M3_driver_SPI.receive_message.message.velocity);
		CDC_Transmit_FS((uint8_t *)data, strlen(data));
	}
}

void reset_NUC_connection() {
	sprintf(data, "RESET;MOMENTARY_SWITCH\n");
	CDC_Transmit_FS((uint8_t *)data, strlen(data));
}

int NUC_turned_on() {
	if (HAL_GPIO_ReadPin(NUC_FRONT_PANEL_LED_GPIO_Port, NUC_FRONT_PANEL_LED_Pin) || currents.current[CURRENT_NUC_IDX] > 0.2) {
		return 1;
	}
	return 0;
}
