/*
 * Capacitive_buttons.c
 *
 *  Created on: Aug 2, 2023
 *      Author: vaclav
 */

#include "Capacitive_buttons.h"


struct capacitive_values capacitive_buttons;
bool buzzer_playing = false;
bool crash_detected = false;
bool buttons_pressed = true;
double bumper_max_pressure_value = 0.0;
double capacitance_sum = 0.0;

void start_capacitance_readings() {
	HAL_TSC_IODischarge(&htsc, ENABLE);
	HAL_Delay(1);

	if (HAL_TSC_Start_IT(&htsc) != HAL_OK) {
		Error_Handler();
	}
}


void read_capacitance(TSC_HandleTypeDef *htsc) {
	// Read the value
	capacitance_sum = 0.0;
	for (int sensor_index = 0; sensor_index < NUMBER_OF_CAPACITIVE_SENSORS; sensor_index++) {
		if (HAL_TSC_GroupGetStatus(htsc, sensor_index) == TSC_GROUP_COMPLETED) {
			capacitive_buttons.TSC_value[sensor_index] = HAL_TSC_GroupGetValue(htsc, sensor_index);
			capacitive_buttons.capacitance[sensor_index] = capacitive_buttons.filter * capacitive_buttons.capacitance[sensor_index] + (1.0 - capacitive_buttons.filter) * ((double) capacitive_buttons.TSC_value[sensor_index]);
			capacitive_buttons.capacitance_offset[sensor_index] = capacitive_buttons.offset_filter * capacitive_buttons.capacitance_offset[sensor_index] + (1.0 - capacitive_buttons.offset_filter) * capacitive_buttons.capacitance[sensor_index];
			capacitive_buttons.pressure[sensor_index] = 3.0 * (capacitive_buttons.capacitance_offset[sensor_index] - capacitive_buttons.capacitance[sensor_index]) - 200.0;
			if (capacitive_buttons.pressure[sensor_index] < 0) {
				capacitive_buttons.pressure[sensor_index] = 0;
			}
			else if (capacitive_buttons.pressure[sensor_index] > 1000.0) {
				capacitive_buttons.pressure[sensor_index] = 1000.0;
			}
		}
		capacitance_sum += capacitive_buttons.pressure[sensor_index];
		if (capacitive_buttons.pressure[sensor_index] > 200) {
			if (sensor_index < NUMBER_OF_BUMPER_SENSORS) {
				crash_detected = true;
				if (bumper_max_pressure_value < capacitive_buttons.pressure[sensor_index]) {
					bumper_max_pressure_value = capacitive_buttons.pressure[sensor_index];
				}
			}
			else {
				buttons_pressed = true;
			}

		}
	}

	if (crash_detected) {
		crash_detected = false;
		HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_SET);
		play_note(NOTE_E3, bumper_max_pressure_value/1000.0);
		bumper_max_pressure_value = 0.0;
		for (int i = 3; i < NUMBER_OF_LEDS; i++) {
			RGB(i, capacitive_buttons.pressure[((i-1)/4)%NUMBER_OF_BUMPER_SENSORS] * 0.255, 0, 0);
		}
		show_LEDs();
		buzzer_playing = true;
		if (capacitance_sum >= 4000.0) {
			// There are some error in the data -> recalibrite the sensor
			for (int sensor_index = 0; sensor_index < NUMBER_OF_CAPACITIVE_SENSORS; sensor_index++) {
				capacitive_buttons.capacitance_offset[sensor_index] = capacitive_buttons.capacitance[sensor_index];
			}

		}
	}
	else {
		if (buttons_pressed) {
			buttons_pressed = false;
			if (capacitive_buttons.pressure[START_BUTTON_IDX] >= 800.0) {
				if (capacitive_buttons.pressure[STOP_BUTTON_IDX] < 400) {
					capacitive_buttons.pressure[STOP_BUTTON_IDX] = 0.0;
					all_RGB(0, 255, 0);
					play_note(NOTE_E5, 1.0);
					buzzer_playing = true;
				}
				else {
					all_RGB(255, 255, 0);
					play_note(NOTE_C5, 1.0);
					buzzer_playing = true;
				}
			}

			if (capacitive_buttons.pressure[STOP_BUTTON_IDX] >= 800.0) {
				if (capacitive_buttons.pressure[START_BUTTON_IDX] < 400) {
					capacitive_buttons.pressure[START_BUTTON_IDX] = 0.0;
					all_RGB(0, 0, 255);
					play_note(NOTE_A4, 1.0);
					buzzer_playing = true;
				}
				else {
					all_RGB(255, 255, 0);
					play_note(NOTE_C5, 1.0);
					buzzer_playing = true;
				}
			}
		}
		else {
			if (buzzer_playing) {
				play_note(NO_NOTE, 1.0);
				buzzer_playing = false;
				all_RGB(0, 0, 0);
			}
		}
	}
}


void HAL_TSC_ConvCpltCallback(TSC_HandleTypeDef *htsc) {
	// Discharge the touch-sensing IOs
	HAL_TSC_IODischarge(htsc, ENABLE);

	read_capacitance(htsc);

	// Re-start the acquisition process
	HAL_TSC_Start_IT(htsc);
}

void HAL_TSC_ErrorCallback(TSC_HandleTypeDef *htsc) {
	// Discharge the touch-sensing IOs
	HAL_TSC_IODischarge(htsc, ENABLE);

	read_capacitance(htsc);

	// Re-start the acquisition process
	HAL_TSC_Start_IT(htsc);
}
