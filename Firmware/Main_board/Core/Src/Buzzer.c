/*
 * Buzzer.c
 *
 *  Created on: Jun 13, 2023
 *      Author: vaclav
 */

#include "Buzzer.h"


int melody_sound[] = {
  NOTE_A4, 4, NO_NOTE, 8, NOTE_A4, 4, NO_NOTE, 8, NOTE_E5, 1,

  NO_NOTE,1,NO_NOTE,1,NO_NOTE,1,NO_NOTE,1,

  NOTE_A4, 4, NO_NOTE, 4, NOTE_A4, 4, NO_NOTE, 4, NOTE_E5, 2,

  NO_NOTE,1,NO_NOTE,1,NO_NOTE,1,NO_NOTE,1,

  NOTE_E5, 8, NO_NOTE, 8, NOTE_E5, 8, NO_NOTE, 8, NOTE_A4, 2,

  NO_NOTE,1,NO_NOTE,1,NO_NOTE,1,NO_NOTE,1,

  NOTE_E5, 4, NO_NOTE, 4, NOTE_E5, 4, NO_NOTE, 4, NOTE_A4, 1,

};

const int melody_turn_on[] = {
  	NO_NOTE, 2,
  	NOTE_A4, 4, NO_NOTE, 8, NOTE_A4, 4, NO_NOTE, 8, NOTE_E5, 1
};

const int melody_turn_off[] = {
	NO_NOTE, 2,
	NOTE_E5, 4, NO_NOTE, 8, NOTE_E5, 4, NO_NOTE, 8, NOTE_A4, 1
};

const int melody_reset_NUC[] = {
	NOTE_A4, 4, NO_NOTE, 8, NOTE_E5, 4, NO_NOTE, 8, NOTE_C5, 1,
};

const int melody_tell_to_turn_off[] = {
	NOTE_C5, 4, NO_NOTE, 8, NOTE_E5, 4, NO_NOTE, 8, NOTE_A4, 1,
};


void play_note(int note, double percentage) {
	if (note <= 0) {
		TIM3->CCR1 = 0;
	}
	else {
		int period = 1000000/note;
		__HAL_TIM_SET_AUTORELOAD(&htim3, period);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, percentage * (period/2.0));
	}
}

void play_melody(int melody) {
	int *melody_to_play;
	size_t array_size = 0;
	switch (melody){
		case MELODY_START:
			melody_to_play = melody_turn_on;
			array_size = MELODY_ARRAY_SIZE(melody_turn_on);
			break;
		case MELODY_TURN_OFF:
			melody_to_play = melody_turn_off;
			array_size = MELODY_ARRAY_SIZE(melody_turn_off);
			break;
		case MELODY_RESET_NUC:
			melody_to_play = melody_reset_NUC;
			array_size = MELODY_ARRAY_SIZE(melody_reset_NUC);
			break;
		case MELODY_TELL_TO_TURN_OFF:
			melody_to_play = melody_tell_to_turn_off;
			array_size = MELODY_ARRAY_SIZE(melody_tell_to_turn_off);
			break;
		default:
			melody_to_play = melody_reset_NUC;
			array_size = MELODY_ARRAY_SIZE(melody_reset_NUC);
			break;
	}

	for (int i = 0; i < array_size; i++) {
		int time = NOTE_DURATION/(abs(melody_to_play[2*i + 1]));
		if (melody_to_play[2*i + 1] < 0) {
			time *= 1.5;
		}

		play_note(melody_to_play[2*i], 1.0);
		HAL_Delay(time);
	}
	TIM3->CCR1 = 0;
}


void play_sound() {
	size_t freq_array_size = (sizeof(melody_sound) / sizeof(melody_sound[0]))/2;
	for (int i = 0; i < freq_array_size; i++) {
		  int ARR = 1000000/melody_sound[2*i];
		  __HAL_TIM_SET_AUTORELOAD(&htim3, ARR);
		  __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1,ARR/2);
		  if (melody_sound[2*i + 1] < 0) {
			  int time = NOTE_DURATION/abs(melody_sound[2*i + 1]);
			  time *= 1.5;
			  HAL_Delay(time);
		  }
		  else {
			  int time = NOTE_DURATION/melody_sound[2*i + 1];
			  HAL_Delay(time);
		  }
	 }
	  TIM3->CCR1 = 0;
	  HAL_Delay(1000);
}
