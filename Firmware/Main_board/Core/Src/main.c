/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "usbd_cdc_if.h"
#include "string.h"
#include <stdbool.h>

#include "ADCs.h"
#include "WS2812b.h"
#include "Buzzer.h"
#include "Drivers_SPI.h"
#include "NUC.h"
#include "Capacitive_buttons.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define		BLINK_PERIOD		5.0
#define		BLINK_FREQUENCY		(1.0/BLINK_PERIOD)

uint8_t new_message_received = false;

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

CAN_HandleTypeDef hcan;

CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c1;

SDADC_HandleTypeDef hsdadc1;
SDADC_HandleTypeDef hsdadc2;
DMA_HandleTypeDef hdma_sdadc1;
DMA_HandleTypeDef hdma_sdadc2;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
SPI_HandleTypeDef hspi3;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim17;
DMA_HandleTypeDef hdma_tim5_ch1;

TSC_HandleTypeDef htsc;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TSC_Init(void);
static void MX_ADC1_Init(void);
static void MX_SDADC1_Init(void);
static void MX_SDADC2_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM17_Init(void);
static void MX_CAN_Init(void);
static void MX_CRC_Init(void);
static void MX_I2C1_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);
static void MX_SPI3_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

extern struct driver_SPI M1_driver_SPI;
extern struct driver_SPI M2_driver_SPI;
extern struct driver_SPI M3_driver_SPI;
extern struct capacitive_values capacitive_buttons;

struct momentary_swich_values {
	double actual_time;
	double prev_time;
	double temporary_time;
	double pressed_time;
	uint8_t button_state;

} momentary_switch;

uint8_t robot_state = 0;

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM2){
		if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
			momentary_switch.actual_time = ((double)HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1))/10000.0;
			momentary_switch.button_state = HAL_GPIO_ReadPin(SWITCH_VALUE_GPIO_Port, SWITCH_VALUE_Pin);

			if (momentary_switch.actual_time-momentary_switch.prev_time > 0.2) { //DEBOUNCING - 200ms
				if (momentary_switch.button_state == true) {
					momentary_switch.temporary_time = momentary_switch.actual_time;
				}
				else {
					momentary_switch.pressed_time = momentary_switch.actual_time - momentary_switch.temporary_time;
				}
			}

			momentary_switch.prev_time = momentary_switch.actual_time;
		}
	}
}

void turn_off_the_robot(){
	HAL_GPIO_WritePin(M1_MOTOR_NSLEEP_GPIO_Port, M1_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(M2_MOTOR_NSLEEP_GPIO_Port, M2_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(M3_MOTOR_NSLEEP_GPIO_Port, M3_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
	all_RGB(255, 0, 0);
	play_melody(MELODY_TURN_OFF);
	HAL_GPIO_WritePin(POWER_ON_GPIO_Port, POWER_ON_Pin, GPIO_PIN_RESET);
	HAL_Delay(5000);
	play_note(NOTE_C6, 1.0);
	while(1) {
		HAL_Delay(10);
	}
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_TSC_Init();
	MX_ADC1_Init();
	MX_SDADC1_Init();
	MX_SDADC2_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_TIM5_Init();
	MX_TIM17_Init();
	MX_CAN_Init();
	MX_CRC_Init();
	MX_I2C1_Init();
	MX_SPI1_Init();
	MX_SPI2_Init();
	MX_SPI3_Init();
	MX_USART1_UART_Init();
	MX_USART3_UART_Init();
	MX_USB_DEVICE_Init();
	MX_TIM4_Init();
	/* USER CODE BEGIN 2 */

	// Capacitive buttons
	capacitive_buttons.filter = CAPACITANCE_FILTER_VALUE;
	start_capacitance_readings();

	/* ----------------------------------------------- */
	// Momentary switch
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim17, TIM_CHANNEL_1);
	TIM17->CCR1 = 0;

	/* ----------------------------------------------- */
	// Buzzer
	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	TIM3->CCR1 = 0;

	/* ----------------------------------------------- */
	// Start ADCs measuring
	start_ADC_readings();

	/* ----------------------------------------------- */
	// Setting up motor drivers SPI

	M1_driver_SPI.ID = 0;
	M1_driver_SPI.hspi = &hspi1;
	M1_driver_SPI.SPI_SS_port = M1_SPI_SS_GPIO_Port;
	M1_driver_SPI.SPI_SS_pin = M1_SPI_SS_Pin;

	M2_driver_SPI.ID = 1;
	M2_driver_SPI.hspi = &hspi2;
	M2_driver_SPI.SPI_SS_port = M2_SPI_SS_GPIO_Port;
	M2_driver_SPI.SPI_SS_pin = M2_SPI_SS_Pin;

	M3_driver_SPI.ID = 2;
	M3_driver_SPI.hspi = &hspi3;
	M3_driver_SPI.SPI_SS_port = M3_SPI_SS_GPIO_Port;
	M3_driver_SPI.SPI_SS_pin = M3_SPI_SS_Pin;

	M1_driver_SPI.send_message.message.mode = 100.0;
	M1_driver_SPI.send_message.message.acceleration_time = 250;
	M2_driver_SPI.send_message.message.mode = 100.0;
	M2_driver_SPI.send_message.message.acceleration_time = 250;
	M3_driver_SPI.send_message.message.mode = 100.0;
	M3_driver_SPI.send_message.message.acceleration_time = 250;

	HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_RESET);

	// Turn the NUC on
	HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_SET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_RESET);

	/* ----------------------------------------------- */
	// Starting WS2812B timer
	HAL_TIM_Base_Start(&htim5);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */

	HAL_GPIO_WritePin(M1_MOTOR_NSLEEP_GPIO_Port, M1_MOTOR_NSLEEP_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(M2_MOTOR_NSLEEP_GPIO_Port, M2_MOTOR_NSLEEP_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(M3_MOTOR_NSLEEP_GPIO_Port, M3_MOTOR_NSLEEP_Pin, GPIO_PIN_SET);

	uint32_t turn_off_time = HAL_GetTick();
	uint32_t reset_time = HAL_GetTick();
	uint32_t actual_time = HAL_GetTick();
	double HSV_value = 0;
	while (1) {
		actual_time = HAL_GetTick();
		if ((uint32_t)(actual_time - turn_off_time) < 120000) {
			if ((uint32_t)(actual_time - turn_off_time) > 5000) {
				if (NUC_turned_on()) {
					// The NUC is ON
					break;
				}
				else {
					// Try to turn the NUC on again
					if ((uint32_t)(actual_time - reset_time) > 1000) {
						reset_time = HAL_GetTick();
						HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_SET);
						HAL_Delay(100);
						HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_RESET);
					}
				}
			}

			if (momentary_switch.button_state == true) {
				momentary_switch.pressed_time = ((double)TIM2->CNT)/10000.0 - momentary_switch.temporary_time;
				if (momentary_switch.pressed_time > 1) {
					/* ----------------- SHUTDOWN THE ROBOT ----------------- */
					double blink_time = momentary_switch.pressed_time - (int)momentary_switch.pressed_time;

					while (blink_time > BLINK_FREQUENCY) {
						blink_time -= BLINK_FREQUENCY;
					}
					double percentage = 1.0 - fabs(blink_time*2.0 - BLINK_FREQUENCY)/BLINK_FREQUENCY;
					robot_state = 3;
					play_note(NOTE_E5, 1.0);
					all_HSV(0, 1.0, percentage);
					TIM17->CCR1 = (1000.0 * percentage);
				}
				else {
					all_HSV(0, 1.0, momentary_switch.pressed_time);
					TIM17->CCR1 = (1000.0 * momentary_switch.pressed_time);
				}
			}
			else {
				if (robot_state == 3) {
					turn_off_the_robot();
				}

				for (int i = 3; i < NUMBER_OF_LEDS; i++) {
					HSV(i, HSV_value, 1.0, 1.0);
					HSV_value += 14.6;
					if (HSV_value > 360.0) {
						HSV_value -= 360.0;
					}
				}
				show_LEDs();
				TIM17->CCR1 = 1000 - abs((actual_time%2000) - 1000);
			}
		}
		else {
			turn_off_the_robot();
		}
		HAL_Delay(30);
	}

	for (int i = 0; i < 256; i++){
		all_RGB(0, i, 0);
		HAL_Delay(2);
		TIM17->CCR1 = i * 4;
	}
	TIM17->CCR1 = 1000;
	play_melody(MELODY_START);

	USB_connection(USB_ENABLED);
	capacitive_buttons.offset_filter = CAPACITANCE_OFFSET_FILTER_VALUE;

	while (1)
	{
		if (capacitive_buttons.pressure[STOP_BUTTON_IDX] >= 800.0) {
			HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(M1_MOTOR_NSLEEP_GPIO_Port, M1_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(M2_MOTOR_NSLEEP_GPIO_Port, M2_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(M3_MOTOR_NSLEEP_GPIO_Port, M3_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
		}
		else if (capacitive_buttons.pressure[START_BUTTON_IDX] >= 800.0){
			HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(M1_MOTOR_NSLEEP_GPIO_Port, M1_MOTOR_NSLEEP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(M2_MOTOR_NSLEEP_GPIO_Port, M2_MOTOR_NSLEEP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(M3_MOTOR_NSLEEP_GPIO_Port, M3_MOTOR_NSLEEP_Pin, GPIO_PIN_SET);
		}

		if (capacitive_buttons.pressure[STOP_BUTTON_IDX] >= 800.0 && capacitive_buttons.pressure[START_BUTTON_IDX] >= 800.0) {
			// RESET CAPACITIVE BUMPER
			for (int sensor_index = 0; sensor_index < 6; sensor_index++) {
				capacitive_buttons.capacitance_offset[sensor_index] = capacitive_buttons.capacitance[sensor_index];
			}
		}

		/* ----------------- MAIN CODE ----------------- */
		if (momentary_switch.button_state != true) {
			switch(robot_state) {
			case 0:
				break;
			case 1:
				// RESET NUC CODE
				robot_state = 0;
				HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_SET);
				reset_NUC_connection();
				play_melody(MELODY_RESET_NUC);
				USB_connection(USB_DISABLED);

				for (int t = 8; t > 0; t-= 1) {
					all_RGB(0, 0, 0);
					for (int i = 0; i < 3; i++) {
						for(int c = 0; c < 256; c+=20) {
							RGB(5-i, 0, c, 0);
							RGB(6+i, 0, c, 0);
							RGB(11-i, 0, c, 0);
							RGB(12+i, 0, c, 0);
							RGB(17-i, 0, c, 0);
							RGB(18+i, 0, c, 0);
							RGB(23-i, 0, c, 0);
							RGB(24+i, 0, c, 0);
							HAL_Delay(t);
							show_LEDs();
						}
					}
				}

				all_RGB(0, 0, 0);
				HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_RESET);
				USB_connection(USB_ENABLED);
				break;
			case 2:
				// TELL NUC TO SHUTDOWN

				HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_SET);

				HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_SET);
				all_RGB(5, 5, 5);
				play_melody(MELODY_TELL_TO_TURN_OFF);
				HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_RESET);

				HAL_GPIO_WritePin(M1_MOTOR_NSLEEP_GPIO_Port, M1_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2_MOTOR_NSLEEP_GPIO_Port, M2_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M3_MOTOR_NSLEEP_GPIO_Port, M3_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);

				// WAIT FOR NUC TO SHUTDOWN
				turn_off_time = HAL_GetTick();
				actual_time = HAL_GetTick();
				int led_index = 0;
				while (1) {
					actual_time = HAL_GetTick();
					if ((uint32_t)(actual_time - turn_off_time) < 120000) {
						for (int i = 0; i < NUMBER_OF_LEDS; i++) {
							if (i == led_index + 3) {
								RGB(i, 255, 255, 255);
							}
							else {
								RGB(i, 0, 0, 0);
							}
						}
						led_index = (led_index+1)%24;
						show_LEDs();
						TIM17->CCR1 = 1000 - 4 * abs((actual_time%500) - 250);
						HAL_Delay(15);
						if (NUC_turned_on()) {
							if ((uint32_t)(actual_time - turn_off_time) > 10000) {
								// TURNING THE NUC OFF
								HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_SET);
							}
						}
						else {
							break;
						}
					}
					else {
						break;
					}
				}
				// SHUTDOWN THE ROBOT
				turn_off_the_robot();
				break;
			case 3:
				// SHUTDOWN THE ROBOT
				HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_SET);
				turn_off_the_robot();
				break;
			default:
				break;
			}

			// TODO: Write main code
			//	4. Measure analog signals -> currents, voltages -> react to that

			if (new_message_received) {
				HAL_GPIO_WritePin(MOTOR_SYNC_GPIO_Port, MOTOR_SYNC_Pin, GPIO_PIN_SET);
				Driver_TransmitReceive(&M1_driver_SPI);
				Driver_TransmitReceive(&M2_driver_SPI);
				Driver_TransmitReceive(&M3_driver_SPI);
				HAL_GPIO_WritePin(MOTOR_SYNC_GPIO_Port, MOTOR_SYNC_Pin, GPIO_PIN_RESET);
				new_message_received = 0;
			}
			else {
				// Read state of the motors
				Driver_TransmitReceive(&M1_driver_SPI);
				Driver_TransmitReceive(&M2_driver_SPI);
				Driver_TransmitReceive(&M3_driver_SPI);
			}

			check_voltage_level();
			send_status();

			momentary_switch.pressed_time = 0.0;
		}
		/* ----------------- SHUTDOWN ROBOT CODE ----------------- */
		else {
			momentary_switch.pressed_time = ((double)TIM2->CNT)/10000.0 - momentary_switch.temporary_time;

			/* ----------------- RESET NUC CODE ----------------- */
			if (momentary_switch.pressed_time < 1.0) {
				robot_state = 1;
				double percentage = momentary_switch.pressed_time;
				play_note(NOTE_A4, percentage);
				all_HSV(120 - percentage*60, 1.0, percentage);
			}
			/* ----------------- TELL NUC TO SHUTDOWN ----------------- */
			else if (momentary_switch.pressed_time < 3) {
				robot_state = 2;
				double percentage = (momentary_switch.pressed_time-1.0)/2.0;
				play_note(NOTE_C5, percentage);
				all_HSV(60 - percentage*60, 1.0, percentage);
			}
			/* ----------------- SHUTDOWN THE ROBOT ----------------- */
			else {
				double blink_time = momentary_switch.pressed_time - (int)momentary_switch.pressed_time;

				while (blink_time > BLINK_FREQUENCY) {
					blink_time -= BLINK_FREQUENCY;
				}
				double percentage = 1.0 - fabs(blink_time*2.0 - BLINK_FREQUENCY)/BLINK_FREQUENCY;

				robot_state = 3;
				play_note(NOTE_E5, percentage);
				all_HSV(0, 1.0, percentage);
				TIM17->CCR1 = (1000.0 * percentage);
			}
		}


		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART1
			|RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_I2C1
			|RCC_PERIPHCLK_ADC1|RCC_PERIPHCLK_SDADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
	PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
	PeriphClkInit.USBClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
	PeriphClkInit.SdadcClockSelection = RCC_SDADCSYSCLK_DIV36;
	PeriphClkInit.Adc1ClockSelection = RCC_ADC1PCLK2_DIV2;

	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler();
	}
	HAL_PWREx_EnableSDADC(PWR_SDADC_ANALOG1);
	HAL_PWREx_EnableSDADC(PWR_SDADC_ANALOG2);
	HAL_RCC_MCOConfig(RCC_MCO, RCC_MCO1SOURCE_HSE, RCC_MCODIV_1);
}

/**
 * @brief ADC1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_ADC1_Init(void)
{

	/* USER CODE BEGIN ADC1_Init 0 */

	/* USER CODE END ADC1_Init 0 */

	ADC_ChannelConfTypeDef sConfig = {0};

	/* USER CODE BEGIN ADC1_Init 1 */

	/* USER CODE END ADC1_Init 1 */

	/** Common config
	 */
	hadc1.Instance = ADC1;
	hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.DiscontinuousConvMode = ENABLE;
	hadc1.Init.NbrOfDiscConversion = 1;
	hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T4_CC4;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 5;
	if (HAL_ADC_Init(&hadc1) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_11;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_12;
	sConfig.Rank = ADC_REGULAR_RANK_2;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_13;
	sConfig.Rank = ADC_REGULAR_RANK_3;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
	sConfig.Rank = ADC_REGULAR_RANK_4;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_VREFINT;
	sConfig.Rank = ADC_REGULAR_RANK_5;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN ADC1_Init 2 */

	/* USER CODE END ADC1_Init 2 */

}

/**
 * @brief CAN Initialization Function
 * @param None
 * @retval None
 */
static void MX_CAN_Init(void)
{

	/* USER CODE BEGIN CAN_Init 0 */

	/* USER CODE END CAN_Init 0 */

	/* USER CODE BEGIN CAN_Init 1 */

	/* USER CODE END CAN_Init 1 */
	hcan.Instance = CAN;
	hcan.Init.Prescaler = 16;
	hcan.Init.Mode = CAN_MODE_NORMAL;
	hcan.Init.SyncJumpWidth = CAN_SJW_1TQ;
	hcan.Init.TimeSeg1 = CAN_BS1_1TQ;
	hcan.Init.TimeSeg2 = CAN_BS2_1TQ;
	hcan.Init.TimeTriggeredMode = DISABLE;
	hcan.Init.AutoBusOff = DISABLE;
	hcan.Init.AutoWakeUp = DISABLE;
	hcan.Init.AutoRetransmission = DISABLE;
	hcan.Init.ReceiveFifoLocked = DISABLE;
	hcan.Init.TransmitFifoPriority = DISABLE;
	if (HAL_CAN_Init(&hcan) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN CAN_Init 2 */

	/* USER CODE END CAN_Init 2 */

}

/**
 * @brief CRC Initialization Function
 * @param None
 * @retval None
 */
static void MX_CRC_Init(void)
{

	/* USER CODE BEGIN CRC_Init 0 */

	/* USER CODE END CRC_Init 0 */

	/* USER CODE BEGIN CRC_Init 1 */

	/* USER CODE END CRC_Init 1 */
	hcrc.Instance = CRC;
	hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
	hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
	hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
	hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
	hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
	if (HAL_CRC_Init(&hcrc) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN CRC_Init 2 */

	/* USER CODE END CRC_Init 2 */

}

/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void)
{

	/* USER CODE BEGIN I2C1_Init 0 */

	/* USER CODE END I2C1_Init 0 */

	/* USER CODE BEGIN I2C1_Init 1 */

	/* USER CODE END I2C1_Init 1 */
	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing = 0x2000090E;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c1) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Analogue filter
	 */
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Digital filter
	 */
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN I2C1_Init 2 */

	/* USER CODE END I2C1_Init 2 */

}

/**
 * @brief SDADC1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SDADC1_Init(void)
{

	/* USER CODE BEGIN SDADC1_Init 0 */

	/* USER CODE END SDADC1_Init 0 */

	SDADC_ConfParamTypeDef ConfParamStruct = {0};

	/* USER CODE BEGIN SDADC1_Init 1 */

	/* USER CODE END SDADC1_Init 1 */

	/** Configure the SDADC low power mode, fast conversion mode,
  slow clock mode and SDADC1 reference voltage
	 */
	hsdadc1.Instance = SDADC1;
	hsdadc1.Init.IdleLowPowerMode = SDADC_LOWPOWER_NONE;
	hsdadc1.Init.FastConversionMode = SDADC_FAST_CONV_DISABLE;
	hsdadc1.Init.SlowClockMode = SDADC_SLOW_CLOCK_DISABLE;
	hsdadc1.Init.ReferenceVoltage = SDADC_VREF_EXT;
	hsdadc1.InjectedTrigger = SDADC_EXTERNAL_TRIGGER;
	hsdadc1.ExtTriggerEdge = SDADC_EXT_TRIG_BOTH_EDGES;
	if (HAL_SDADC_Init(&hsdadc1) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure the Injected Mode
	 */
	if (HAL_SDADC_SelectInjectedDelay(&hsdadc1, SDADC_INJECTED_DELAY_NONE) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_SDADC_SelectInjectedExtTrigger(&hsdadc1, SDADC_EXT_TRIG_TIM4_CC1, SDADC_EXT_TRIG_BOTH_EDGES) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_SDADC_SelectInjectedTrigger(&hsdadc1, SDADC_EXTERNAL_TRIGGER) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_SDADC_InjectedConfigChannel(&hsdadc1, SDADC_CHANNEL_1|SDADC_CHANNEL_2
			|SDADC_CHANNEL_3|SDADC_CHANNEL_4
			|SDADC_CHANNEL_5|SDADC_CHANNEL_6
			|SDADC_CHANNEL_7|SDADC_CHANNEL_8, SDADC_CONTINUOUS_CONV_OFF) != HAL_OK)
	{
		Error_Handler();
	}

	/** Set parameters for SDADC configuration 0 Register
	 */
	 ConfParamStruct.InputMode = SDADC_INPUT_MODE_SE_ZERO_REFERENCE;
	 ConfParamStruct.Gain = SDADC_GAIN_1;
	 ConfParamStruct.CommonMode = SDADC_COMMON_MODE_VSSA;
	 ConfParamStruct.Offset = 0;
	 if (HAL_SDADC_PrepareChannelConfig(&hsdadc1, SDADC_CONF_INDEX_0, &ConfParamStruct) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_1, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_2, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_3, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_4, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_5, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_6, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_7, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }

	 /** Configure the Injected Channel
	  */
	 if (HAL_SDADC_AssociateChannelConfig(&hsdadc1, SDADC_CHANNEL_8, SDADC_CONF_INDEX_0) != HAL_OK)
	 {
		 Error_Handler();
	 }
	 /* USER CODE BEGIN SDADC1_Init 2 */

	 /* USER CODE END SDADC1_Init 2 */

}

/**
 * @brief SDADC2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SDADC2_Init(void)
{

	/* USER CODE BEGIN SDADC2_Init 0 */

	/* USER CODE END SDADC2_Init 0 */

	SDADC_ConfParamTypeDef ConfParamStruct = {0};

	/* USER CODE BEGIN SDADC2_Init 1 */

	/* USER CODE END SDADC2_Init 1 */

	/** Configure the SDADC low power mode, fast conversion mode,
  slow clock mode and SDADC1 reference voltage
	 */
	hsdadc2.Instance = SDADC2;
	hsdadc2.Init.IdleLowPowerMode = SDADC_LOWPOWER_NONE;
	hsdadc2.Init.FastConversionMode = SDADC_FAST_CONV_DISABLE;
	hsdadc2.Init.SlowClockMode = SDADC_SLOW_CLOCK_DISABLE;
	hsdadc2.Init.ReferenceVoltage = SDADC_VREF_EXT;
	hsdadc2.InjectedTrigger = SDADC_SYNCHRONOUS_TRIGGER;
	if (HAL_SDADC_Init(&hsdadc2) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure the Injected Mode
	 */
	if (HAL_SDADC_SelectInjectedDelay(&hsdadc2, SDADC_INJECTED_DELAY_NONE) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_SDADC_SelectInjectedTrigger(&hsdadc2, SDADC_SYNCHRONOUS_TRIGGER) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_SDADC_InjectedConfigChannel(&hsdadc2, SDADC_CHANNEL_0|SDADC_CHANNEL_1
			|SDADC_CHANNEL_2|SDADC_CHANNEL_3, SDADC_CONTINUOUS_CONV_OFF) != HAL_OK)
	{
		Error_Handler();
	}

	/** Set parameters for SDADC configuration 0 Register
	 */
	ConfParamStruct.InputMode = SDADC_INPUT_MODE_SE_ZERO_REFERENCE;
	ConfParamStruct.Gain = SDADC_GAIN_1;
	ConfParamStruct.CommonMode = SDADC_COMMON_MODE_VSSA;
	ConfParamStruct.Offset = 0;
	if (HAL_SDADC_PrepareChannelConfig(&hsdadc2, SDADC_CONF_INDEX_0, &ConfParamStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure the Injected Channel
	 */
	if (HAL_SDADC_AssociateChannelConfig(&hsdadc2, SDADC_CHANNEL_0, SDADC_CONF_INDEX_0) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure the Injected Channel
	 */
	if (HAL_SDADC_AssociateChannelConfig(&hsdadc2, SDADC_CHANNEL_1, SDADC_CONF_INDEX_0) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure the Injected Channel
	 */
	if (HAL_SDADC_AssociateChannelConfig(&hsdadc2, SDADC_CHANNEL_2, SDADC_CONF_INDEX_0) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure the Injected Channel
	 */
	if (HAL_SDADC_AssociateChannelConfig(&hsdadc2, SDADC_CHANNEL_3, SDADC_CONF_INDEX_0) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN SDADC2_Init 2 */

	/* USER CODE END SDADC2_Init 2 */

}

/**
 * @brief SPI1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI1_Init(void)
{

	/* USER CODE BEGIN SPI1_Init 0 */

	/* USER CODE END SPI1_Init 0 */

	/* USER CODE BEGIN SPI1_Init 1 */

	/* USER CODE END SPI1_Init 1 */
	/* SPI1 parameter configuration*/
	hspi1.Instance = SPI1;
	hspi1.Init.Mode = SPI_MODE_MASTER;
	hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	hspi1.Init.DataSize = SPI_DATASIZE_16BIT;
	hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi1.Init.NSS = SPI_NSS_SOFT;
	hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
	hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi1.Init.CRCPolynomial = 7;
	hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
	hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
	if (HAL_SPI_Init(&hspi1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN SPI1_Init 2 */

	/* USER CODE END SPI1_Init 2 */

}

/**
 * @brief SPI2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI2_Init(void)
{

	/* USER CODE BEGIN SPI2_Init 0 */

	/* USER CODE END SPI2_Init 0 */

	/* USER CODE BEGIN SPI2_Init 1 */

	/* USER CODE END SPI2_Init 1 */
	/* SPI2 parameter configuration*/
	hspi2.Instance = SPI2;
	hspi2.Init.Mode = SPI_MODE_MASTER;
	hspi2.Init.Direction = SPI_DIRECTION_2LINES;
	hspi2.Init.DataSize = SPI_DATASIZE_16BIT;
	hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi2.Init.NSS = SPI_NSS_SOFT;
	hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
	hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi2.Init.CRCPolynomial = 7;
	hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
	hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
	if (HAL_SPI_Init(&hspi2) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN SPI2_Init 2 */

	/* USER CODE END SPI2_Init 2 */

}

/**
 * @brief SPI3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI3_Init(void)
{

	/* USER CODE BEGIN SPI3_Init 0 */

	/* USER CODE END SPI3_Init 0 */

	/* USER CODE BEGIN SPI3_Init 1 */

	/* USER CODE END SPI3_Init 1 */
	/* SPI3 parameter configuration*/
	hspi3.Instance = SPI3;
	hspi3.Init.Mode = SPI_MODE_MASTER;
	hspi3.Init.Direction = SPI_DIRECTION_2LINES;
	hspi3.Init.DataSize = SPI_DATASIZE_16BIT;
	hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi3.Init.NSS = SPI_NSS_SOFT;
	hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
	hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi3.Init.CRCPolynomial = 7;
	hspi3.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
	hspi3.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
	if (HAL_SPI_Init(&hspi3) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN SPI3_Init 2 */

	/* USER CODE END SPI3_Init 2 */

}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void)
{

	/* USER CODE BEGIN TIM2_Init 0 */

	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	TIM_IC_InitTypeDef sConfigIC = {0};

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 7200-1;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 4294967295;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
	{
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_TIM_IC_Init(&htim2) != HAL_OK)
	{
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
	{
		Error_Handler();
	}
	sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_BOTHEDGE;
	sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
	sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
	sConfigIC.ICFilter = 15;
	if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TIM2_Init 2 */

	/* USER CODE END TIM2_Init 2 */

}

/**
 * @brief TIM3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM3_Init(void)
{

	/* USER CODE BEGIN TIM3_Init 0 */

	/* USER CODE END TIM3_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	TIM_OC_InitTypeDef sConfigOC = {0};

	/* USER CODE BEGIN TIM3_Init 1 */

	/* USER CODE END TIM3_Init 1 */
	htim3.Instance = TIM3;
	htim3.Init.Prescaler = 72-1;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 1000;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
	{
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
	{
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
	{
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TIM3_Init 2 */

	/* USER CODE END TIM3_Init 2 */
	HAL_TIM_MspPostInit(&htim3);

}

/**
 * @brief TIM4 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM4_Init(void)
{

	/* USER CODE BEGIN TIM4_Init 0 */

	/* USER CODE END TIM4_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	TIM_OC_InitTypeDef sConfigOC = {0};

	/* USER CODE BEGIN TIM4_Init 1 */

	/* USER CODE END TIM4_Init 1 */
	htim4.Instance = TIM4;
	htim4.Init.Prescaler = 720-1;
	htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim4.Init.Period = 1000;
	htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
	{
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_TIM_OC_Init(&htim4) != HAL_OK)
	{
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
	{
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TIM4_Init 2 */

	/* USER CODE END TIM4_Init 2 */

}

/**
 * @brief TIM5 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM5_Init(void)
{

	/* USER CODE BEGIN TIM5_Init 0 */

	/* USER CODE END TIM5_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	TIM_OC_InitTypeDef sConfigOC = {0};

	/* USER CODE BEGIN TIM5_Init 1 */

	/* USER CODE END TIM5_Init 1 */
	htim5.Instance = TIM5;
	htim5.Init.Prescaler = 0;
	htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim5.Init.Period = 110-1;
	htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
	{
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_TIM_PWM_Init(&htim5) != HAL_OK)
	{
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
	{
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TIM5_Init 2 */

	/* USER CODE END TIM5_Init 2 */
	HAL_TIM_MspPostInit(&htim5);

}

/**
 * @brief TIM17 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM17_Init(void)
{

	/* USER CODE BEGIN TIM17_Init 0 */

	/* USER CODE END TIM17_Init 0 */

	TIM_OC_InitTypeDef sConfigOC = {0};
	TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

	/* USER CODE BEGIN TIM17_Init 1 */

	/* USER CODE END TIM17_Init 1 */
	htim17.Instance = TIM17;
	htim17.Init.Prescaler = 72-1;
	htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim17.Init.Period = 1000;
	htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim17.Init.RepetitionCounter = 0;
	htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
	{
		Error_Handler();
	}
	if (HAL_TIM_PWM_Init(&htim17) != HAL_OK)
	{
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
	sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	if (HAL_TIM_PWM_ConfigChannel(&htim17, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
	sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
	sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
	sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
	sBreakDeadTimeConfig.DeadTime = 0;
	sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
	sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
	sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
	if (HAL_TIMEx_ConfigBreakDeadTime(&htim17, &sBreakDeadTimeConfig) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TIM17_Init 2 */

	/* USER CODE END TIM17_Init 2 */
	HAL_TIM_MspPostInit(&htim17);

}

/**
 * @brief TSC Initialization Function
 * @param None
 * @retval None
 */
static void MX_TSC_Init(void)
{

	/* USER CODE BEGIN TSC_Init 0 */

	/* USER CODE END TSC_Init 0 */

	/* USER CODE BEGIN TSC_Init 1 */

	/* USER CODE END TSC_Init 1 */

	/** Configure the TSC peripheral
	 */
	htsc.Instance = TSC;
	htsc.Init.CTPulseHighLength = TSC_CTPH_2CYCLES;
	htsc.Init.CTPulseLowLength = TSC_CTPL_2CYCLES;
	htsc.Init.SpreadSpectrum = ENABLE;
	htsc.Init.SpreadSpectrumDeviation = 64;
	htsc.Init.SpreadSpectrumPrescaler = TSC_SS_PRESC_DIV2;
	htsc.Init.PulseGeneratorPrescaler = TSC_PG_PRESC_DIV4;
	htsc.Init.MaxCountValue = TSC_MCV_16383;
	htsc.Init.IODefaultMode = TSC_IODEF_OUT_PP_LOW;
	htsc.Init.SynchroPinPolarity = TSC_SYNC_POLARITY_FALLING;
	htsc.Init.AcquisitionMode = TSC_ACQ_MODE_NORMAL;
	htsc.Init.MaxCountInterrupt = ENABLE;
	htsc.Init.ChannelIOs = TSC_GROUP1_IO2|TSC_GROUP2_IO2|TSC_GROUP3_IO2|TSC_GROUP4_IO2
			|TSC_GROUP5_IO2|TSC_GROUP6_IO2|TSC_GROUP7_IO2|TSC_GROUP8_IO2;
	htsc.Init.ShieldIOs = 0;
	htsc.Init.SamplingIOs = TSC_GROUP1_IO1|TSC_GROUP2_IO1|TSC_GROUP3_IO1|TSC_GROUP4_IO1
			|TSC_GROUP5_IO1|TSC_GROUP6_IO1|TSC_GROUP7_IO1|TSC_GROUP8_IO1;
	if (HAL_TSC_Init(&htsc) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TSC_Init 2 */

	/* USER CODE END TSC_Init 2 */

}

/**
 * @brief USART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART1_UART_Init(void)
{

	/* USER CODE BEGIN USART1_Init 0 */

	/* USER CODE END USART1_Init 0 */

	/* USER CODE BEGIN USART1_Init 1 */

	/* USER CODE END USART1_Init 1 */
	huart1.Instance = USART1;
	huart1.Init.BaudRate = 115200;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN USART1_Init 2 */

	/* USER CODE END USART1_Init 2 */

}

/**
 * @brief USART3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART3_UART_Init(void)
{

	/* USER CODE BEGIN USART3_Init 0 */

	/* USER CODE END USART3_Init 0 */

	/* USER CODE BEGIN USART3_Init 1 */

	/* USER CODE END USART3_Init 1 */
	huart3.Instance = USART3;
	huart3.Init.BaudRate = 115200;
	huart3.Init.WordLength = UART_WORDLENGTH_8B;
	huart3.Init.StopBits = UART_STOPBITS_1;
	huart3.Init.Parity = UART_PARITY_NONE;
	huart3.Init.Mode = UART_MODE_TX_RX;
	huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart3.Init.OverSampling = UART_OVERSAMPLING_16;
	huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart3) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN USART3_Init 2 */

	/* USER CODE END USART3_Init 2 */

}

/**
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void)
{

	/* DMA controller clock enable */
	__HAL_RCC_DMA2_CLK_ENABLE();
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA1_Channel1_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
	/* DMA2_Channel3_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Channel3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA2_Channel3_IRQn);
	/* DMA2_Channel4_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Channel4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA2_Channel4_IRQn);
	/* DMA2_Channel5_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Channel5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA2_Channel5_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOE, SPI_CS1_Pin|SPI_CS2_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(NUC_POWER_SWITCH_GPIO_Port, NUC_POWER_SWITCH_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(M3_MOTOR_NSLEEP_GPIO_Port, M3_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOF, POWER_ON_Pin|M3_SPI_SS_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(USB_EN_GPIO_Port, USB_EN_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOD, M2_MOTOR_NSLEEP_Pin|MOTOR_SYNC_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOD, BRAKE_MOTORS_Pin|M2_SPI_SS_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(M1_SPI_SS_GPIO_Port, M1_SPI_SS_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(M1_MOTOR_NSLEEP_GPIO_Port, M1_MOTOR_NSLEEP_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pins : SPI_CS1_Pin SPI_CS2_Pin */
	GPIO_InitStruct.Pin = SPI_CS1_Pin|SPI_CS2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	/*Configure GPIO pin : NUC_CURRENT_FAULT_Pin */
	GPIO_InitStruct.Pin = NUC_CURRENT_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(NUC_CURRENT_FAULT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : NUC_POWER_SWITCH_Pin */
	GPIO_InitStruct.Pin = NUC_POWER_SWITCH_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(NUC_POWER_SWITCH_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : NUC_FRONT_PANEL_LED_Pin */
	GPIO_InitStruct.Pin = NUC_FRONT_PANEL_LED_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(NUC_FRONT_PANEL_LED_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : M3_CURRENT_FAULT_Pin */
	GPIO_InitStruct.Pin = M3_CURRENT_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(M3_CURRENT_FAULT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : M3_MOTOR_NSLEEP_Pin POWER_ON_Pin */
	GPIO_InitStruct.Pin = M3_MOTOR_NSLEEP_Pin|POWER_ON_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	/*Configure GPIO pin : M3_MCU_FAULT_Pin */
	GPIO_InitStruct.Pin = M3_MCU_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(M3_MCU_FAULT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : CAPACITIVE_PINS_INT_Pin */
	GPIO_InitStruct.Pin = CAPACITIVE_PINS_INT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(CAPACITIVE_PINS_INT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : USB_EN_Pin */
	GPIO_InitStruct.Pin = USB_EN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(USB_EN_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : B3_CURRENT_FAULT_Pin */
	GPIO_InitStruct.Pin = B3_CURRENT_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B3_CURRENT_FAULT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : B2_CURRENT_FAULT_Pin */
	GPIO_InitStruct.Pin = B2_CURRENT_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B2_CURRENT_FAULT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : B1_CURRENT_FAULT_Pin */
	GPIO_InitStruct.Pin = B1_CURRENT_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_CURRENT_FAULT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : M2_MOTOR_NSLEEP_Pin BRAKE_MOTORS_Pin MOTOR_SYNC_Pin */
	GPIO_InitStruct.Pin = M2_MOTOR_NSLEEP_Pin|BRAKE_MOTORS_Pin|MOTOR_SYNC_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	/*Configure GPIO pins : M2_MCU_FAULT_Pin M2_CURRENT_FAULT_Pin M1_CURRENT_FAULT_Pin */
	GPIO_InitStruct.Pin = M2_MCU_FAULT_Pin|M2_CURRENT_FAULT_Pin|M1_CURRENT_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	/*Configure GPIO pin : M1_SPI_SS_Pin */
	GPIO_InitStruct.Pin = M1_SPI_SS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(M1_SPI_SS_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : MOTOR_OSC_Pin */
	GPIO_InitStruct.Pin = MOTOR_OSC_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
	HAL_GPIO_Init(MOTOR_OSC_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : M3_SPI_SS_Pin */
	GPIO_InitStruct.Pin = M3_SPI_SS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(M3_SPI_SS_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : M2_SPI_SS_Pin */
	GPIO_InitStruct.Pin = M2_SPI_SS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(M2_SPI_SS_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : M1_MOTOR_NSLEEP_Pin */
	GPIO_InitStruct.Pin = M1_MOTOR_NSLEEP_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(M1_MOTOR_NSLEEP_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : M1_MCU_FAULT_Pin */
	GPIO_InitStruct.Pin = M1_MCU_FAULT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(M1_MCU_FAULT_GPIO_Port, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI2_TSC_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_TSC_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
