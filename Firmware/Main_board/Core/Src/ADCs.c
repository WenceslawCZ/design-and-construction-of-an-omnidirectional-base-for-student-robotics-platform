/*
 * ADCs.c
 *
 *  Created on: Jul 6, 2023
 *      Author: vaclav
 */

#include <ADCs.h>


double VOLTAGE_3V3 = 3.3;
double ROBOT_TEMPERATURE_SCOPE;
double ROBOT_TEMPERATURE_OFFSET;

int16_t ADC_Values[ADC_BUFFER_SIZE];

struct current_values currents;

struct voltage_values voltages;

struct temperature_values temperatures;

bool low_voltage_notification = false;

void HAL_SDADC_InjectedConvCpltCallback(SDADC_HandleTypeDef* hsdadc) {
	if (hsdadc->Instance == currents.instance) {
		for (int i = 0; i < currents.size-1; i++) {
			currents.current[i] = currents.filter * (currents.current[i]) + (1 - currents.filter) * (15.5*((currents.ADC_value[i] - currents.sensor_offset[i])/32767.0));
		}
		currents.current[currents.size-1] = 8200.0 * (((currents.ADC_value[currents.size-1] - currents.sensor_offset[7] + 32768.0) / 65535.0) * VOLTAGE_3V3) / 1000.0;
	}
	else if (hsdadc->Instance == voltages.instance) {
		for (int i = 0; i < voltages.size; i++) {
			double voltage = (124.0/24.0) * ((voltages.ADC_value[i] + 32768.0)/65535.0)*VOLTAGE_3V3; // The curve is somehow not linear
			voltages.voltage[i] = voltages.filter * (voltages.voltage[i]) + (1 - voltages.filter) * (0.0255 * voltage * voltage + 0.5907 * voltage + 1.6554);
//			voltages.voltage[i] = voltages.filter * (voltages.voltage[i]) + (1 - voltages.filter) * (voltages.GAIN * (124.0/24.0) * ((voltages.ADC_value[i] - voltages.SDADC_offset + 32768.0)/65535.0)*VOLTAGE_3V3);
		}
	}
}

void check_voltage_level() {
	for (int i = 0; i < voltages.size-1; i++) {
		if (voltages.voltage[i] > 6) {
			// Battery is connected
			if (voltages.voltage[i] < 12.3) {
				// Battery reached a critical voltage
				if (voltages.voltage[i] > voltages.voltage[VOLTAGE_ROBOT_IDX]) {
					// Battery has a higher voltage than is the ROBOT voltage -> all the batteries are discharged
					if (voltages.voltage[i] < 12.0) {
						// Turn the robot off
						turn_off_the_robot();
					}
					else {
						if (low_voltage_notification == false) {
							// The voltage just dropped to the critical voltage of 12.3 volts -> notify user

							low_voltage_notification = true;
							HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_SET);
							play_note(NOTE_F5, 1.0);

							for (int t = 20; t > 0; t-= 1) {
								for (int i = 0; i < 100; i+=5) {
									all_HSV(10, 1.0, ((double)i)/100.0);
									HAL_Delay(t);
								}
							}

							all_RGB(0, 0, 0);
							play_note(NO_NOTE, 1.0);
							HAL_GPIO_WritePin(BRAKE_MOTORS_GPIO_Port, BRAKE_MOTORS_Pin, GPIO_PIN_RESET);
						}
					}
				}
			}
			else if (voltages.voltage[i] > 14.0) {
				if (low_voltage_notification) {
					low_voltage_notification = false;
				}
			}
		}
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	VOLTAGE_3V3 = ADC_LOW_PASS_FILTER * VOLTAGE_3V3 + (1.0 - ADC_LOW_PASS_FILTER) * (3300.0 * *VREFINT_CAL_ADDR / ADC_Values[4] / 1000.0);

	for (int i = 0; i < 3; i++) {
		double voltage = (ADC_Values[i]/4095.0)*VOLTAGE_3V3;
		double resistance = (VOLTAGE_3V3*10000.0)/voltage - 10000.0;
		temperatures.motors[i] = ADC_LOW_PASS_FILTER * temperatures.motors[i] + (1.0 - ADC_LOW_PASS_FILTER) * (1.0/(log(resistance/10000.0)/3950.0 + 1.0/298.15) - 273.15);
	}
	temperatures.robot = ADC_LOW_PASS_FILTER * temperatures.robot + (1.0 - ADC_LOW_PASS_FILTER) * ((3.3 / VOLTAGE_3V3) * ROBOT_TEMPERATURE_SCOPE * ADC_Values[3] + ROBOT_TEMPERATURE_OFFSET);
}

void start_ADC_readings() {
	// Starting SDADC timer - one measurement each 10ms
	  HAL_TIM_OC_Start(&htim4, TIM_CHANNEL_1);

	  // Starting SDADC1 - measuring currents
	  currents.sensor_offset[0] = 496;
	  currents.sensor_offset[1] = 768;
	  currents.sensor_offset[2] = 764;
	  currents.sensor_offset[3] = 926;
	  currents.sensor_offset[4] = 673;
	  currents.sensor_offset[5] = 570;
	  currents.sensor_offset[6] = 836;
	  currents.sensor_offset[7] = 119;
	  currents.instance = SDADC1;
	  currents.size = CURRENTS_BUFFER_SIZE;
	  currents.filter = 0.95;
	  HAL_SDADC_CalibrationStart(&hsdadc1, SDADC_CALIBRATION_SEQ_1);
	  HAL_Delay(20);
	  HAL_SDADC_PollForCalibEvent(&hsdadc1, 2000);
//	  currents.SDADC_offset = READ_SDADC1_OFFSET;
	  if (HAL_SDADC_InjectedStart_DMA(&hsdadc1, (uint32_t*)currents.ADC_value, currents.size) != HAL_OK) {
	  	  Error_Handler();
	  }

	  // Starting SDADC2 - measuring voltages
	  voltages.instance = SDADC2;
	  voltages.size = VOLTAGES_BUFFER_SIZE;
	  voltages.filter = 0.95;
	  HAL_SDADC_CalibrationStart(&hsdadc2, SDADC_CALIBRATION_SEQ_1);
	  HAL_Delay(20);
//	  voltages.SDADC_offset = READ_SDADC2_OFFSET;
	  HAL_SDADC_PollForCalibEvent(&hsdadc2, 2000);

	  if (HAL_SDADC_InjectedStart_DMA(&hsdadc2, (uint32_t*)voltages.ADC_value, voltages.size) != HAL_OK) {
		  Error_Handler();
	  }

	  // Starting ADC timer - one measurement each 100ms
	  HAL_TIM_OC_Start(&htim4, TIM_CHANNEL_4);

	  ROBOT_TEMPERATURE_SCOPE = 80.0 / (*TS_CAL2_CAL_ADDR - *TS_CAL1_CAL_ADDR);
	  ROBOT_TEMPERATURE_OFFSET = 30.0 - ROBOT_TEMPERATURE_SCOPE * *TS_CAL1_CAL_ADDR;

	  // Starting ADC - measuring temperatures and voltage reference
	  HAL_ADCEx_Calibration_Start(&hadc1);
	  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)ADC_Values, ADC_BUFFER_SIZE);
}
