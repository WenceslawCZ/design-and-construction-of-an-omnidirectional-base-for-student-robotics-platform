/*
 * Drivers_SPI.c
 *
 *  Created on: Jul 6, 2023
 *      Author: vaclav
 */

#include "Drivers_SPI.h"

struct driver_SPI M1_driver_SPI;
struct driver_SPI M2_driver_SPI;
struct driver_SPI M3_driver_SPI;

void Driver_TransmitReceive(struct driver_SPI* motor_driver_SPI) {
	HAL_GPIO_WritePin(motor_driver_SPI->SPI_SS_port, motor_driver_SPI->SPI_SS_pin, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(motor_driver_SPI->hspi, motor_driver_SPI->send_message.message_content, motor_driver_SPI->receive_message.message_content, 6, 4);

	HAL_GPIO_WritePin(motor_driver_SPI->SPI_SS_port, motor_driver_SPI->SPI_SS_pin, GPIO_PIN_SET);

	if (fabs(motor_driver_SPI->receive_message.message.temperature) > 500.0 || fabs(motor_driver_SPI->receive_message.message.velocity) > 500.0) {
		motor_driver_SPI->receive_message.message.position = -1000;
		motor_driver_SPI->receive_message.message.velocity = -1000;
		motor_driver_SPI->receive_message.message.temperature = -1000;
	}

	temperatures.drivers[motor_driver_SPI->ID] = motor_driver_SPI->receive_message.message.temperature;
}
