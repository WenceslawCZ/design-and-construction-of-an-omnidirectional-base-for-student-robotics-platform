/*
 * WS2812b.cpp
 *
 *  Created on: Jun 6, 2023
 *      Author: vaclav
 */


#include "WS2812b.h"

struct WS2812b_leds LEDs;

void RGB(uint16_t index, uint8_t R, uint8_t G, uint8_t B)
{
	LEDs.COLOR[index][0] = G;
	LEDs.COLOR[index][1] = R;
	LEDs.COLOR[index][2] = B;
}

void all_RGB(uint8_t R, uint8_t G, uint8_t B)
{
	for (int i = 0; i < NUMBER_OF_LEDS; i++) {
		RGB(i, R, G, B);
	}
	show_LEDs();
}

void show_LEDs()
{
	for (int i = 0; i < NUMBER_OF_LEDS; i++) {
		for (int color = 0; color < 3; color++) {
			uint16_t value = LEDs.COLOR[i][color];
			for (int bit = 0; bit < 8; bit++) {
				if (value & (1<<bit)) {
					LEDs.DATA[i*24 + color*8 + (7-bit)] = WS2812B_ONE;
				}
				else {
					LEDs.DATA[i*24 + color*8 + (7-bit)] = WS2812B_ZERO;
				}
			}
		}
	}

	for (int i = 0; i < RESET_PERIOD; i++) {
		LEDs.DATA[NUMBER_OF_LEDS*24 + i] = 0;
	}

	HAL_TIM_PWM_Start_DMA(&htim5, TIM_CHANNEL_1, (uint32_t *)LEDs.DATA, NUMBER_OF_LEDS*24 + RESET_PERIOD);
}

void HSV(uint16_t index, double H, double S, double V)
{
	double hue, a, b, c, d;

	hue = H/60.0;
	int i = (int)trunc(hue);

	a = hue - (double)i;

	b = V * (1.0 - S);
	c = V * (1.0 - (S * a));
	d = V * (1.0 - (S * (1.0 - a)));

	double R, G, B;

	switch(i) {
		case 0:
			R = V;
			G = d;
			B = b;
			break;
		case 1:
			R = c;
			G = V;
			B = b;
			break;
		case 2:
			R = b;
			G = V;
			B = d;
			break;
		case 3:
			R = b;
			G = c;
			B = V;
			break;

		case 4:
			R = d;
			G = b;
			B = V;
			break;
		case 5:
			R = V;
			G = b;
			B = c;
			break;
		default:
			R = 0.0;
			G = 0.0;
			B = 0.0;
			break;
	}
	RGB(index, (uint8_t)(R*255), (uint8_t)(G*255), (uint8_t)(B*255));
}

void all_HSV(double H, double S, double V)
{
	for (int i = 0; i < NUMBER_OF_LEDS; i++) {
		HSV(i, H, S, V);
	}
	show_LEDs();
}

