/*
 * ADCs.h
 *
 *  Created on: Jul 6, 2023
 *      Author: vaclav
 */

#ifndef INC_ADCS_H_
#define INC_ADCS_H_

#define	CURRENTS_BUFFER_SIZE	8
#define	VOLTAGES_BUFFER_SIZE	4
#define	ADC_BUFFER_SIZE	5
#define ADC_LOW_PASS_FILTER	0.9

#define TS_CAL1_CAL_ADDR     ((uint16_t*) (0x1FFFF7B8U))
#define TS_CAL2_CAL_ADDR     ((uint16_t*) (0x1FFFF7C2U))
#define VREFINT_CAL_ADDR     ((uint16_t*) (0x1FFFF7BAU))

// The offsets is subtracted automatically from the measured data (most likely) - no need to subtract them
#define READ_SDADC1_OFFSET     *((uint32_t*) (0x40016020U))&0xFFF
#define READ_SDADC2_OFFSET     *((uint32_t*) (0x40016420U))&0xFFF

#define CURRENT_MOTOR1_IDX		0
#define CURRENT_MOTOR2_IDX		1
#define CURRENT_MOTOR3_IDX		2
#define CURRENT_BATTERY1_IDX	3
#define CURRENT_BATTERY2_IDX	4
#define CURRENT_BATTERY3_IDX	5
#define CURRENT_NUC_IDX			6
#define CURRENT_ROBOT_IDX		7

#define VOLTAGE_BATTERY1_IDX	0
#define VOLTAGE_BATTERY2_IDX	1
#define VOLTAGE_BATTERY3_IDX	2
#define VOLTAGE_ROBOT_IDX		3

#include "main.h"
#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include "Buzzer.h"
#include "WS2812b.h"

extern ADC_HandleTypeDef hadc1;
extern SDADC_HandleTypeDef hsdadc1;
extern SDADC_HandleTypeDef hsdadc2;
extern TIM_HandleTypeDef htim4;

struct current_values {
	uint8_t size;
	int16_t ADC_value[CURRENTS_BUFFER_SIZE];
	int16_t SDADC_offset;
	int16_t sensor_offset[CURRENTS_BUFFER_SIZE];
	double current[CURRENTS_BUFFER_SIZE];
	double filter; // Averaging filter (should be between 0-1)
	SDADC_TypeDef* instance;
};

struct voltage_values {
	uint8_t size;
	int16_t ADC_value[VOLTAGES_BUFFER_SIZE];
	int16_t SDADC_offset;
	double voltage[VOLTAGES_BUFFER_SIZE];
	double filter; // Averaging filter (should be between 0-1)
	SDADC_TypeDef* instance;
};

struct temperature_values {
	double motors[3];
	double drivers[3];
	double robot;
};

void HAL_SDADC_InjectedConvCpltCallback(SDADC_HandleTypeDef* hsdadc);
void check_voltage_level();

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);

void start_ADC_readings();

#endif /* INC_ADCS_H_ */
