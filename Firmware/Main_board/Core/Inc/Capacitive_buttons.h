/*
 * Capacitive_buttons.h
 *
 *  Created on: Aug 2, 2023
 *      Author: vaclav
 */

#ifndef INC_CAPACITIVE_BUTTONS_H_
#define INC_CAPACITIVE_BUTTONS_H_

#define	NUMBER_OF_BUMPER_SENSORS		6
#define NUMBER_OF_CAPACITIVE_SENSORS	8
#define START_BUTTON_IDX				6
#define STOP_BUTTON_IDX					7
#define CAPACITANCE_FILTER_VALUE			0.95
#define CAPACITANCE_OFFSET_FILTER_VALUE		0.99995

#include "main.h"
#include <stdint.h>
#include <stdbool.h>
#include "Buzzer.h"
#include "WS2812b.h"

extern TSC_HandleTypeDef htsc;

struct capacitive_values {
	int16_t TSC_value[NUMBER_OF_CAPACITIVE_SENSORS];
	double capacitance[NUMBER_OF_CAPACITIVE_SENSORS];
	double capacitance_offset[NUMBER_OF_CAPACITIVE_SENSORS];
	double pressure[NUMBER_OF_CAPACITIVE_SENSORS];
	double filter; 			// Low-pass filter (should be between 0-1)
	double offset_filter;	// Low-pass filter (should be between 0-1)
};

void start_capacitance_readings();
void read_capacitance(TSC_HandleTypeDef *htsc);
void HAL_TSC_ConvCpltCallback(TSC_HandleTypeDef *htsc);
void HAL_TSC_ErrorCallback(TSC_HandleTypeDef *htsc);


#endif /* INC_CAPACITIVE_BUTTONS_H_ */
