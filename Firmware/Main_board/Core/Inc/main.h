/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

void turn_off_the_robot();

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CAPACITIVE_PIN7_SAMPLING_Pin GPIO_PIN_2
#define CAPACITIVE_PIN7_SAMPLING_GPIO_Port GPIOE
#define CAPACITIVE_PIN7_Pin GPIO_PIN_3
#define CAPACITIVE_PIN7_GPIO_Port GPIOE
#define SPI_CS1_Pin GPIO_PIN_4
#define SPI_CS1_GPIO_Port GPIOE
#define SPI_CS2_Pin GPIO_PIN_5
#define SPI_CS2_GPIO_Port GPIOE
#define NUC_CURRENT_FAULT_Pin GPIO_PIN_6
#define NUC_CURRENT_FAULT_GPIO_Port GPIOE
#define NUC_POWER_SWITCH_Pin GPIO_PIN_13
#define NUC_POWER_SWITCH_GPIO_Port GPIOC
#define NUC_FRONT_PANEL_LED_Pin GPIO_PIN_14
#define NUC_FRONT_PANEL_LED_GPIO_Port GPIOC
#define M3_CURRENT_FAULT_Pin GPIO_PIN_15
#define M3_CURRENT_FAULT_GPIO_Port GPIOC
#define M3_CURRENT_FAULT_EXTI_IRQn EXTI15_10_IRQn
#define M3_MOTOR_NSLEEP_Pin GPIO_PIN_9
#define M3_MOTOR_NSLEEP_GPIO_Port GPIOF
#define M3_MCU_FAULT_Pin GPIO_PIN_10
#define M3_MCU_FAULT_GPIO_Port GPIOF
#define M3_MCU_FAULT_EXTI_IRQn EXTI15_10_IRQn
#define WS2812B_LEDS_DATA_Pin GPIO_PIN_0
#define WS2812B_LEDS_DATA_GPIO_Port GPIOC
#define M1_THERMISTOR_Pin GPIO_PIN_1
#define M1_THERMISTOR_GPIO_Port GPIOC
#define M2_THERMISTOR_Pin GPIO_PIN_2
#define M2_THERMISTOR_GPIO_Port GPIOC
#define M3_THERMISTOR_Pin GPIO_PIN_3
#define M3_THERMISTOR_GPIO_Port GPIOC
#define POWER_ON_Pin GPIO_PIN_2
#define POWER_ON_GPIO_Port GPIOF
#define CAPACITIVE_PIN1_SAMPLING_Pin GPIO_PIN_0
#define CAPACITIVE_PIN1_SAMPLING_GPIO_Port GPIOA
#define CAPACITIVE_PIN1_Pin GPIO_PIN_1
#define CAPACITIVE_PIN1_GPIO_Port GPIOA
#define CAPACITIVE_PINS_INT_Pin GPIO_PIN_2
#define CAPACITIVE_PINS_INT_GPIO_Port GPIOA
#define CAPACITIVE_PINS_INT_EXTI_IRQn EXTI2_TSC_IRQn
#define USB_EN_Pin GPIO_PIN_3
#define USB_EN_GPIO_Port GPIOA
#define B3_CURRENT_FAULT_Pin GPIO_PIN_4
#define B3_CURRENT_FAULT_GPIO_Port GPIOF
#define CAPACITIVE_PIN2_SAMPLING_Pin GPIO_PIN_4
#define CAPACITIVE_PIN2_SAMPLING_GPIO_Port GPIOA
#define CAPACITIVE_PIN2_Pin GPIO_PIN_5
#define CAPACITIVE_PIN2_GPIO_Port GPIOA
#define BUZZER_Pin GPIO_PIN_6
#define BUZZER_GPIO_Port GPIOA
#define B2_CURRENT_FAULT_Pin GPIO_PIN_7
#define B2_CURRENT_FAULT_GPIO_Port GPIOA
#define CAPACITIVE_PIN3_SAMPLING_Pin GPIO_PIN_4
#define CAPACITIVE_PIN3_SAMPLING_GPIO_Port GPIOC
#define CAPACITIVE_PIN3_Pin GPIO_PIN_5
#define CAPACITIVE_PIN3_GPIO_Port GPIOC
#define B3_CURRENT_Pin GPIO_PIN_0
#define B3_CURRENT_GPIO_Port GPIOB
#define B2_CURRENT_Pin GPIO_PIN_1
#define B2_CURRENT_GPIO_Port GPIOB
#define B1_CURRENT_Pin GPIO_PIN_2
#define B1_CURRENT_GPIO_Port GPIOB
#define M3_CURRENT_Pin GPIO_PIN_7
#define M3_CURRENT_GPIO_Port GPIOE
#define ROBOT_CURRENT_Pin GPIO_PIN_8
#define ROBOT_CURRENT_GPIO_Port GPIOE
#define NUC_CURRENT_Pin GPIO_PIN_9
#define NUC_CURRENT_GPIO_Port GPIOE
#define M2_CURRENT_Pin GPIO_PIN_10
#define M2_CURRENT_GPIO_Port GPIOE
#define M1_CURRENT_Pin GPIO_PIN_11
#define M1_CURRENT_GPIO_Port GPIOE
#define ROBOT_VOLTAGE_Pin GPIO_PIN_12
#define ROBOT_VOLTAGE_GPIO_Port GPIOE
#define B3_VOLTAGE_Pin GPIO_PIN_13
#define B3_VOLTAGE_GPIO_Port GPIOE
#define B2_VOLTAGE_Pin GPIO_PIN_14
#define B2_VOLTAGE_GPIO_Port GPIOE
#define B1_VOLTAGE_Pin GPIO_PIN_15
#define B1_VOLTAGE_GPIO_Port GPIOE
#define B1_CURRENT_FAULT_Pin GPIO_PIN_10
#define B1_CURRENT_FAULT_GPIO_Port GPIOB
#define CAPACITIVE_PIN6_SAMPLING_Pin GPIO_PIN_14
#define CAPACITIVE_PIN6_SAMPLING_GPIO_Port GPIOB
#define CAPACITIVE_PIN6_Pin GPIO_PIN_15
#define CAPACITIVE_PIN6_GPIO_Port GPIOB
#define M2_MOTOR_NSLEEP_Pin GPIO_PIN_10
#define M2_MOTOR_NSLEEP_GPIO_Port GPIOD
#define M2_MCU_FAULT_Pin GPIO_PIN_11
#define M2_MCU_FAULT_GPIO_Port GPIOD
#define M2_MCU_FAULT_EXTI_IRQn EXTI15_10_IRQn
#define CAPACITIVE_PIN8_SAMPLING_Pin GPIO_PIN_12
#define CAPACITIVE_PIN8_SAMPLING_GPIO_Port GPIOD
#define CAPACITIVE_PIN8_Pin GPIO_PIN_13
#define CAPACITIVE_PIN8_GPIO_Port GPIOD
#define BRAKE_MOTORS_Pin GPIO_PIN_14
#define BRAKE_MOTORS_GPIO_Port GPIOD
#define MOTOR_SYNC_Pin GPIO_PIN_15
#define MOTOR_SYNC_GPIO_Port GPIOD
#define M1_SPI_SS_Pin GPIO_PIN_6
#define M1_SPI_SS_GPIO_Port GPIOC
#define M1_SPI_SCK_Pin GPIO_PIN_7
#define M1_SPI_SCK_GPIO_Port GPIOC
#define M1_SPI_MISO_Pin GPIO_PIN_8
#define M1_SPI_MISO_GPIO_Port GPIOC
#define M1_SPI_MOSI_Pin GPIO_PIN_9
#define M1_SPI_MOSI_GPIO_Port GPIOC
#define MOTOR_OSC_Pin GPIO_PIN_8
#define MOTOR_OSC_GPIO_Port GPIOA
#define CAPACITIVE_PIN4_SAMPLING_Pin GPIO_PIN_9
#define CAPACITIVE_PIN4_SAMPLING_GPIO_Port GPIOA
#define CAPACITIVE_PIN4_Pin GPIO_PIN_10
#define CAPACITIVE_PIN4_GPIO_Port GPIOA
#define M3_SPI_SS_Pin GPIO_PIN_6
#define M3_SPI_SS_GPIO_Port GPIOF
#define SWITCH_VALUE_Pin GPIO_PIN_15
#define SWITCH_VALUE_GPIO_Port GPIOA
#define M3_SPI_SCK_Pin GPIO_PIN_10
#define M3_SPI_SCK_GPIO_Port GPIOC
#define M3_SPI_MISO_Pin GPIO_PIN_11
#define M3_SPI_MISO_GPIO_Port GPIOC
#define M3_SPI_MOSI_Pin GPIO_PIN_12
#define M3_SPI_MOSI_GPIO_Port GPIOC
#define M2_SPI_SS_Pin GPIO_PIN_2
#define M2_SPI_SS_GPIO_Port GPIOD
#define M2_SPI_MISO_Pin GPIO_PIN_3
#define M2_SPI_MISO_GPIO_Port GPIOD
#define M2_SPI_MOSI_Pin GPIO_PIN_4
#define M2_SPI_MOSI_GPIO_Port GPIOD
#define M2_CURRENT_FAULT_Pin GPIO_PIN_5
#define M2_CURRENT_FAULT_GPIO_Port GPIOD
#define M2_CURRENT_FAULT_EXTI_IRQn EXTI9_5_IRQn
#define M1_CURRENT_FAULT_Pin GPIO_PIN_6
#define M1_CURRENT_FAULT_GPIO_Port GPIOD
#define M1_CURRENT_FAULT_EXTI_IRQn EXTI9_5_IRQn
#define M2_SPI_SCK_Pin GPIO_PIN_7
#define M2_SPI_SCK_GPIO_Port GPIOD
#define CAPACITIVE_PIN5_SAMPLING_Pin GPIO_PIN_3
#define CAPACITIVE_PIN5_SAMPLING_GPIO_Port GPIOB
#define CAPACITIVE_PIN5_Pin GPIO_PIN_4
#define CAPACITIVE_PIN5_GPIO_Port GPIOB
#define SWITCH_LED_Pin GPIO_PIN_5
#define SWITCH_LED_GPIO_Port GPIOB
#define M1_MOTOR_NSLEEP_Pin GPIO_PIN_6
#define M1_MOTOR_NSLEEP_GPIO_Port GPIOB
#define M1_MCU_FAULT_Pin GPIO_PIN_7
#define M1_MCU_FAULT_GPIO_Port GPIOB
#define M1_MCU_FAULT_EXTI_IRQn EXTI9_5_IRQn

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
