/*
 * Drivers_SPI.h
 *
 *  Created on: Jul 6, 2023
 *      Author: vaclav
 */

#ifndef INC_DRIVERS_SPI_H_
#define INC_DRIVERS_SPI_H_

#include "main.h"
#include <stdint.h>
#include "math.h"
#include "ADCs.h"

extern struct temperature_values temperatures;

struct SPI_main_board_message {
	float mode; // numbers from 0 to 100 will select the maximum power - coast, numbers over 100 will stop the robot - break
	float velocity;
	uint32_t acceleration_time; // [ms]
} __attribute__((packed));

struct SPI_driver_message {
	float position;
	float velocity;
	float temperature;
} __attribute__((packed));

union driver_message {
	struct SPI_main_board_message message;
	uint8_t message_content[sizeof(struct SPI_main_board_message)];
};

union main_board_message {
	struct SPI_driver_message message;
	uint8_t message_content[sizeof(struct SPI_driver_message)];
};

struct driver_SPI {
	uint8_t ID;
	SPI_HandleTypeDef* hspi;
	GPIO_TypeDef* SPI_SS_port;
	uint16_t SPI_SS_pin;
	union driver_message send_message;
	union main_board_message receive_message;
};

void Driver_TransmitReceive(struct driver_SPI* motor_driver_SPI);

#endif /* INC_DRIVERS_SPI_H_ */
