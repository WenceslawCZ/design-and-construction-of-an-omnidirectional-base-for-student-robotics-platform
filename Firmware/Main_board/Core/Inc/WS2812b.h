/*
 * WS2812b.h
 *
 *  Created on: Jun 6, 2023
 *      Author: vaclav
 */

#ifndef INC_WS2812B_H_
#define INC_WS2812B_H_

// Library created for controlling the programmable WS2812b LEDs


#define		NUMBER_OF_LEDS		27 // 3 + 24
#define		RESET_PERIOD		80
#define		WS2812B_ZERO		35
#define		WS2812B_ONE			90

#include "main.h"
#include <stdint.h>
#include <math.h>

extern TIM_HandleTypeDef htim5;

struct WS2812b_leds {
	uint16_t COLOR[NUMBER_OF_LEDS][3];
	uint32_t DATA[NUMBER_OF_LEDS*24 + RESET_PERIOD];
};

void RGB(uint16_t index, uint8_t R, uint8_t G, uint8_t B);

// Set RGB colors for all leds
void all_RGB(uint8_t R, uint8_t G, uint8_t B);

// Set RGB color
void show_LEDs();

// Set HSV colors based on index -> needs to run setColor()
void HSV(uint16_t index, double H, double S, double V);

// Set HSV colors for all leds
void all_HSV(double H, double S, double V);

#endif /* INC_WS2812B_H_ */
