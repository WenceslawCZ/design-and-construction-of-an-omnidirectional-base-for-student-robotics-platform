/*
 * NUC.h
 *
 *  Created on: Jul 20, 2023
 *      Author: vaclav
 */

#ifndef INC_NUC_H_
#define INC_NUC_H_

#include "main.h"
#include <stdlib.h>
#include "ADCs.h"
#include "Drivers_SPI.h"
#include "Capacitive_buttons.h"
#include <string.h>
#include <stdio.h>

#define	CURRENT_UPDATE_PERIOD	200
#define	VOLTAGE_UPDATE_PERIOD	500
#define	TEMPERATURE_UPDATE_PERIOD	1000
#define	CAPACITIVE_BUTTONS_UPDATE_PERIOD	100
#define MOTOR_POSITION_UPDATE_PERIOD	50
#define MOTOR_VELOCITY_UPDATE_PERIOD	20

#define USB_RECEIVE_BUFFER_SIZE	100
#define USB_SEND_BUFFER_SIZE	100


#define	USB_DISABLED 	0
#define USB_ENABLED		1

void USB_connection(uint8_t state);
void send_status();
void reset_NUC_connection();
int NUC_turned_on();


#endif /* INC_NUC_H_ */
