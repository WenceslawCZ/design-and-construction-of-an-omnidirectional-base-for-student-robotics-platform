# Software

This folder contains the software code and resources for the master's thesis project titled "Design and Construction of an Omnidirectional Base for Student Robotics Platform." The software code pertains to the high-level control of the robot, providing standardized interfaces and functionalities.
