#!/usr/bin/env python3

import rospy
from nav_msgs.msg import OccupancyGrid, Path
import tf
import numpy as np
from scipy import ndimage
from cras_srobot_exploration.srv import Generate_path, Generate_pathResponse
from geometry_msgs.msg import PoseStamped, Quaternion
import copy

class PathCreator():
    def __init__(self):
        # Initialize the node
        rospy.init_node("exploration")

        # Get some useful parameters
        self.mapFrame = rospy.get_param("~map_frame", "map")
        self.robotFrame = rospy.get_param("~robot_frame", "base_link")
        self.robotDiameter = float(rospy.get_param("~robot_diameter", 0.5))
        self.occupancyThreshold = int(rospy.get_param("~occupancy_threshold", 90))

        # Helper variable to determine if grid was received at least once
        self.gridReady = False

        self.tflistener = tf.TransformListener()

        # Subscribe to grid
        self.gridSubscriber = rospy.Subscriber('occupied_space', OccupancyGrid, self.grid_cb)
        self.pathPublisher = rospy.Publisher("finded_path", Path, queue_size=50)
        self.simplifiedPathPublisher = rospy.Publisher("simplified_path", Path, queue_size=50)
        

        self.received_occupancy_grid = OccupancyGrid()
        self.origin_position = None
        self.origin_orientation = None
        self.grid_info = None
        self.grid_resolution = None
        self.grid_width = None
        self.grid_height = None
        self.grid_data = None
        self.inflated_grid_data = None
        self.bfs_where_to_look = [(-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1)]

        self.grid_to_map = None

    
    def generatePath(self, goal):
        """ Method to plan the path from start to the goal pose on the grid
        Args:
            grid_map: OccupancyGrid - gridmap for obstacle growing
            start: Pose - robot start pose
            goal: Pose - robot goal pose
        Returns:
            path: Path - path between the start and goal Pose on the map
        """
        
        poses = []

        self.getRobotCoordinates()
        if self.robotPosition == None:
            rospy.logwarn("No robot position found")
            self.robotPosition = None

            goal_pose = PoseStamped()
            goal_pose.pose.position.x = goal.goal.x
            goal_pose.pose.position.y = goal.goal.y
            goal_pose.pose.orientation = Quaternion(*tf.transformations.quaternion_from_euler(0, 0, goal.goal.theta))
            poses.append(goal_pose)

            generated_path = Path()
            generated_path.header.frame_id = "map"
            generated_path.header.stamp = rospy.Time.now()
            generated_path.poses = poses
            
            self.pathPublisher.publish(generated_path)

            rospy.loginfo(generated_path)
            return Generate_pathResponse(generated_path)
        

        start = self.robotPosition
        MAX_VALUE = np.Infinity
        searched_grid = np.full_like(self.grid_data, MAX_VALUE, dtype=np.double)  #(self.grid_height, self.grid_width)

        new_start = copy.deepcopy(start)

        map_open_list = []
        map_close_list = []
        bfs_where_to_look = [(0,-1), (-1,0), (1,0), (0,1)]
        bfs_where_to_look_diagonal = [(-1,-1), (1,-1), (-1,1), (1,1)]


        start_position = self.robotPosition
        end_position = self.pointFromMap([goal.goal.x, goal.goal.y])
        
        # Removing goal from obstacles
        number_of_inflates = int(np.ceil((self.robotDiameter/2)/self.grid_resolution))
        robot_inflate = np.zeros_like(self.grid_data)
        robot_inflate[end_position[0], end_position[1]] = 1
        robot_inflate = ndimage.binary_dilation(robot_inflate, iterations = number_of_inflates)
        self.grid_data[robot_inflate >= 0.5] = 0

        map_open_list.append(start_position)

        searched_grid[start_position] = 0.0

        while map_open_list:
            x, y = map_open_list.pop(0)
            map_close_list.append([x, y])
            for index, (searched_x,searched_y) in enumerate(bfs_where_to_look):
                next_x = x + searched_x
                next_y = y + searched_y
                if self.out_of_bounds(next_x, next_y):
                    continue
                if self.grid_data[next_x, next_y] < 50 and self.grid_data[next_x, next_y] > -1:
                    if searched_grid[next_x, next_y] == MAX_VALUE:
                        # It's visited for the first time. Add it.
                        map_open_list.append([next_x, next_y])
                    price = searched_grid[x, y] + 1.0
                    if searched_grid[next_x, next_y] > price:
                        searched_grid[next_x, next_y] = price
                    # if next_x
                    # if [next_y, next_x] not in map_open_list and [next_y, next_x] not in map_close_list:
                        # map_open_list.append([next_y, next_x])

            for index, (searched_x,searched_y) in enumerate(bfs_where_to_look_diagonal):
                next_x = x + searched_x
                next_y = y + searched_y
                if self.out_of_bounds(next_x, next_y):
                    continue
                if self.grid_data[next_x, next_y] < 50 and self.grid_data[next_x, next_y] > -1:
                    if searched_grid[next_x, next_y] == MAX_VALUE:
                        # It's visited for the first time. Add it.
                        map_open_list.append([next_x, next_y])
                    price = searched_grid[x, y] + np.sqrt(2)
                    if searched_grid[next_x, next_y] > price:
                        searched_grid[next_x, next_y] = price
                    # if [next_y, next_x] not in map_open_list and [next_y, next_x] not in map_close_list:
                    #     map_open_list.append([next_y, next_x])

        path = Path()

        if searched_grid[end_position] != MAX_VALUE:
            price = searched_grid[end_position]
            x = end_position[0]
            y = end_position[1]
            best_x = x
            best_y = y
            while x != start_position[0] or y != start_position[1]:
                for index, (searched_x,searched_y) in enumerate(bfs_where_to_look):
                    next_x = x + searched_x
                    next_y = y + searched_y

                    if self.out_of_bounds(next_x, next_y):
                        continue
                    if searched_grid[next_x, next_y] < price:
                        best_x = next_x
                        best_y = next_y
                        price = searched_grid[next_x, next_y]

                for index, (searched_x,searched_y) in enumerate(bfs_where_to_look_diagonal):
                    next_x = x + searched_x
                    next_y = y + searched_y
                    if self.out_of_bounds(next_x, next_y):
                        continue
                    if searched_grid[next_x, next_y] < price:
                        best_x = next_x
                        best_y = next_y
                        price = searched_grid[next_x, next_y]
                
                x = best_x
                y = best_y
                # path.insert(0, [x, y])
                map_pose = self.pointToMap([x,y])

                pose = PoseStamped()
                pose.pose.position.x = map_pose[0]
                pose.pose.position.y = map_pose[1]
                pose.pose.orientation = Quaternion(*tf.transformations.quaternion_from_euler(0, 0, 0))
                poses.insert(0, pose)

            #add the start pose
            # path.poses.insert(0, start)
        
            #add the goal pose
            # path.poses.append(goal)
        else:
            goal_pose = PoseStamped()
            goal_pose.pose.position.x = goal.goal.x
            goal_pose.pose.position.y = goal.goal.y
            goal_pose.pose.orientation = Quaternion(*tf.transformations.quaternion_from_euler(0, 0, goal.goal.theta))
            poses.append(goal_pose)
        
            
        path.header.frame_id = "map"
        path.header.stamp = rospy.Time.now()
        path.poses = poses
        
        simplified_path = self.simplify_path(path)
        
        self.pathPublisher.publish(path)
        self.simplifiedPathPublisher.publish(simplified_path)

        rospy.loginfo(simplified_path.poses)
        return Generate_pathResponse(simplified_path)
    

    def simplify_path(self, path):
        """ Method to simplify the found path on the grid
        Args:
            path: Path - path to be simplified
        Returns:
            path_simple: Path - simplified path
        """
        
        print(len(path.poses))
        if (len(path.poses) < 3):
            return path

        path_simplified = Path()

        #add the start pose
        path_simplified.poses.append(path.poses[0])

        previous_point = self.pointFromMap([path.poses[0].pose.position.x, path.poses[0].pose.position.y])                         
        for index, pose in enumerate(path.poses):
            next_point = self.pointFromMap([pose.pose.position.x, pose.pose.position.y])
            line = np.array(self.bresenham_line(previous_point, next_point))
            if line.size != 0:
                points_on_line = self.grid_data[line[:, 0], line[:, 1]]
                if points_on_line[points_on_line > 50].size > 0:
                    # print(points_on_line[points_on_line>=0.8].size)
                    previous_point = self.pointFromMap([path.poses[index-1].pose.position.x, path.poses[index-1].pose.position.y])
                    path_simplified.poses.append(path.poses[index-1])

        #add the goal pose
        path_simplified.poses.append(path.poses[-1])

        path_simplified.header.frame_id = "map"
        path_simplified.header.stamp = rospy.Time.now()

        return path_simplified

    def bresenham_line(self, start, goal):
        """Bresenham's line algorithm
        Args:
            start: (float64, float64) - start coordinate
            goal: (float64, float64) - goal coordinate
        Returns:
            interlying points between the start and goal coordinate
        """
        (x0, y0) = start
        (x1, y1) = goal

        # print(x0, y0, x1, y1)
        line = []
        dx = abs(x1 - x0)
        dy = abs(y1 - y0)
        x, y = x0, y0
        sx = -1 if x0 > x1 else 1
        sy = -1 if y0 > y1 else 1
        if dx > dy:
            err = dx / 2.0
            while x != x1:
                line.append((x,y))
                err -= dy
                if err < 0:
                    y += sy
                    err += dx
                x += sx
        else:
            err = dy / 2.0
            while y != y1:
                line.append((x,y))
                err -= dx
                if err < 0:
                    x += sx
                    err += dy
                y += sy
        x = goal[0]
        y = goal[1]
        return line
    
    def publishPath(self, path):
        generated_path = Path()
        generated_path.header.frame_id = "map"
        generated_path.header.stamp = rospy.Time.now()
        generated_path.poses = path

        self.pathPublisher.publish(generated_path)

    def pointToMap(self, point):
        grid_coords = np.hstack((np.array([point[1], point[0]]) * self.grid_resolution, np.array([0, 1]))).T
        map_coords = self.grid_to_map @ grid_coords
        return map_coords[0], map_coords[1]
    
    def pointFromMap(self, point):
        robot_to_map_T = tf.transformations.quaternion_matrix(np.array([0, 0, 0, 1]))
        robot_to_map_T[0,3] = point[0]
        robot_to_map_T[1,3] = point[1]

        coords =  np.linalg.inv(self.grid_to_map) @ robot_to_map_T @ (np.array([0, 0, 0, 1]).T)
        coords = np.floor(coords[:2] / self.grid_resolution)
        return (int(coords[1]), int(coords[0]))

    def getRobotCoordinates(self):
        """ Get the current robot position in the grid """
        try:
            (robot_position, robot_rotation) = self.tflistener.lookupTransform(self.mapFrame, self.robotFrame, rospy.Time(0))

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            rospy.logwarn("Cannot get the robot position!")
            self.robotPosition = None
        else:

            robot_to_map_T = tf.transformations.quaternion_matrix(robot_rotation)
            robot_to_map_T[:3,3] = robot_position

            coords =  np.linalg.inv(self.grid_to_map) @ robot_to_map_T @ (np.array([0, 0, 0, 1]).T)
            coords = np.floor(coords[:2] / self.grid_resolution)
            self.robotPosition = (int(coords[1]), int(coords[0]))

            print("Position of the robot is [{}, {}] -> [{}, {}]".format(int(coords[1]), int(coords[0]), robot_position[0], robot_position[1]))

    def out_of_bounds(self, x, y):
        if x < 0 or x >= self.grid_height:
            return True
        if y < 0 or y >= self.grid_width:
            return True
        return False
    
    def extractGrid(self, msg):
        # TODO: extract grid from msg.data and other usefull information
        self.received_occupancy_grid = msg
        self.origin_position =  msg.info.origin.position
        self.origin_orientation =  msg.info.origin.orientation
        self.grid_info = msg.info
        self.grid_width = msg.info.width
        self.grid_height = msg.info.height
        self.grid_resolution = msg.info.resolution
        self.grid_data = np.array(msg.data).reshape((self.grid_height, self.grid_width))

        self.grid_to_map = tf.transformations.quaternion_matrix([
            self.origin_orientation.x, self.origin_orientation.y, self.origin_orientation.z, self.origin_orientation.w
        ])
        self.grid_to_map[0,3] = self.origin_position.x
        self.grid_to_map[1,3] = self.origin_position.y
        self.grid_to_map[2,3] = self.origin_position.z

    def grid_cb(self, msg):
        self.extractGrid(msg)
        
        if not self.gridReady:

            # Create services
            self.grf_service = rospy.Service('generate_path', Generate_path, self.generatePath)
            self.gridReady = True


if __name__ == "__main__":
    fe = PathCreator()
    rospy.spin()
