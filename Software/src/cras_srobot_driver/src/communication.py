#!/usr/bin/env python3

import rospy
import serial
from subprocess import run
from time import sleep
from cras_srobot_driver.msg import Robot_state, LEDs
from std_msgs.msg import Float64, Float64MultiArray

robot_reset = False

def velocity_controller_callback(message):
    if len(message.data) < 3:
        rospy.logerr("Velocity controller received wrong number of parameters!")
    else:
        send_messsage = "M " + "{:.6f}".format(message.data[0]) + ";" + "{:.6f}".format(message.data[1]) + ";" + "{:.6f}".format(message.data[2]) 
        robot_serial.write(send_messsage.encode('utf8'))

def buzzer_callback(message):
    send_messsage = "B " + str(message.data) 
    robot_serial.write(send_messsage.encode('utf8'))

def leds_callback(message):
    send_messsage = ""
    if message.header.frame_id == "all_LEDs_RGB_color":
        if message.all_LEDs_RGB_color > 0xFFFFFF:
            rospy.logerr("Color is in wrong format -> values should be between [0x000000 - 0xFFFFFF]")
        else:
            send_messsage = "L " + hex(message.all_LEDs_RGB_color)
            robot_serial.write(send_messsage.encode('utf8'))
        
    elif message.header.frame_id == "all_LEDs_HSV_color":
        if len(message.all_LEDs_HSV_color) == 3:
            if message.all_LEDs_HSV_color[1] < 0 or message.all_LEDs_HSV_color[1] > 1 or message.all_LEDs_HSV_color[2] < 0 or message.all_LEDs_HSV_color[2] > 1:
                rospy.logwarn("Saturation and value should be between [0-1]")
                if message.all_LEDs_HSV_color[0] < 0 or message.all_LEDs_HSV_color[0] > 360:
                    rospy.logwarn("Hue parameter should be between [0-360]")
                else:
                    send_messsage = "LH " + str(message.all_LEDs_HSV_color[0]) + ";1.0;1.0"
                    robot_serial.write(send_messsage.encode('utf8'))
            else:
                if message.all_LEDs_HSV_color[0] < 0 or message.all_LEDs_HSV_color[0] > 360:
                    rospy.logwarn("Hue parameter should be between [0-360]")
                else:
                    send_messsage = "LH " + str(message.all_LEDs_HSV_color[0]) + ";" + str(message.all_LEDs_HSV_color[1]) + ";" + str(message.all_LEDs_HSV_color[2])
                    robot_serial.write(send_messsage.encode('utf8'))
        else:
            rospy.logerr("Wrong number of HSV parameters ->  3 values required")
            rospy.logwarn("HUE - [0-360], SATURATION - [0-1], VALUE - [0-1]")

    elif message.header.frame_id == "LEDs_RGB_color":
        if len(message.LEDs_RGB_color) <= 24:
            if len(message.LEDs_RGB_color) < 24:
                rospy.logwarn("There are 24 LEDs on the robot. You are currently using only " + str(len(message.LEDs_RGB_color)) + " of them!")
            for i in range(len(message.LEDs_RGB_color)):
                if message.LEDs_RGB_color[i] > 0xFFFFFF:
                    rospy.logerr("Color of " + str(i+1) + ". LED is in wrong format -> values should be between [0x000000 - 0xFFFFFF]")
                else:
                    send_messsage = "LI " + str(i) + ";" + hex(message.LEDs_RGB_color[i])
                    robot_serial.write(send_messsage.encode('utf8'))
            send_messsage = "LI 25;0"
            robot_serial.write(send_messsage.encode('utf8'))
        else:
            rospy.logerr("Wrong number of parameters. There are only 24 leds on this robot. You want to use " + str(len(message.LEDs_RGB_color)) + " of them!")
    else:
        rospy.logerr("Wrong LED settings")
        rospy.logwarn("Frame_id of the header should be set to one of these options: {all_LEDs_RGB_color, all_LEDs_HSV_color, LEDs_RGB_color}")

    
def control():
    global robot_reset
    rospy.init_node('cras_robot_state', anonymous=True)
    rate = rospy.Rate(150)
    publisher = rospy.Publisher('/cras_robot/robot_state', Robot_state, queue_size=10)    
    rospy.Subscriber("/cras_robot/control/wheels_velocity_controller/command", Float64MultiArray, velocity_controller_callback)
    rospy.Subscriber("/cras_robot/control/buzzer/command", Float64, buzzer_callback)
    rospy.Subscriber("/cras_robot/control/leds/command", LEDs, leds_callback)
    
    msg = Robot_state()

    robot_serial.timeout = 0.1

    for i in range(255, 0, -4):
        send_messsage = "L " + hex((i)<<8)
        robot_serial.write(send_messsage.encode('utf8'))
        send_messsage = "B " + str(900-i*2) 
        robot_serial.write(send_messsage.encode('utf8'))
        sleep(0.01)

    robot_serial.write("L 0x000000".encode('utf8'))
    robot_serial.write("B 0".encode('utf8'))
    
    # send_messsage = "L 0x000000" + hex(message.all_LEDs_RGB_color)
    # robot_serial.write(send_messsage.encode('utf8'))
    # send_messsage = "B " + str(message.data) 
    # robot_serial.write(send_messsage.encode('utf8'))

    while not rospy.is_shutdown():
        received_message = robot_serial.readline()
        rospy.loginfo(received_message)
        received_message = received_message.decode('utf8').strip().split(";")
        rospy.loginfo(received_message)
        if received_message[0] == 'C':
            msg.currents = list(map(float, received_message[1:]))
            msg.header.frame_id = "Current_update"
        elif received_message[0] == 'V':
            msg.voltages = list(map(float, received_message[1:]))
            msg.header.frame_id = "Voltage_update"
        elif received_message[0] == 'T':
            msg.temperatures = list(map(float, received_message[1:]))
            msg.header.frame_id = "Temperature_update"
        elif received_message[0] == 'B':
            msg.capacitive_buttons = list(map(float, received_message[1:]))
            msg.header.frame_id = "Capacitive_buttons_update"
        elif received_message[0] == 'P':
            msg.motor_position = list(map(float, received_message[1:]))
            msg.header.frame_id = "Motor_position_update"
        elif received_message[0] == 'S':
            msg.motor_velocity = list(map(float, received_message[1:]))
            msg.header.frame_id = "Motor_velocity_update"
        elif received_message[0] == "RESET":
            rospy.logwarn("The robot initiated " + received_message[1] + " reset")
            robot_reset = True

        msg.header.stamp = rospy.Time.now()

        rospy.loginfo(msg)
        publisher.publish(msg)

        rate.sleep()
    rospy.logerr("The communication node was killed")
    robot_serial.write("M 0;0;0".encode('utf8'))

if __name__ == '__main__':
    robot_serial = serial.Serial()
    robot_serial.port = "/dev/ttyACM0"
    robot_serial.baudrate = 256000
    robot_serial.timeout = 1

    try: 
        robot_serial.open()
    except Exception as e:
        print("error open serial port: " + str(e))
        exit()

    if robot_serial.isOpen():
        try:
            control()
        except rospy.ROSInterruptException:
            pass
        except serial.serialutil.SerialException:

            if robot_reset:
                rospy.logwarn("The robot initiated RESET")
                run(['pkill', '-e', 'ros'])
                sleep(1)
                run(['pkill', '-9', '-f', 'ros'])
                # run('/home/robot/Desktop/design-and-construction-of-an-omnidirectional-base-for-student-robotics-platform/Software/cras_srobot_boot_bringup')
            else:
                rospy.logerr("The robot got disconnected")
