#!/usr/bin/env python3

import rospy
from cras_srobot_driver.msg import Robot_state

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np

col='rgbkmcyk'

def filled_arc(center,r,r2,theta1,theta2, intensity):

    # Range of angles
    phi=np.linspace(theta1,theta2,100)

    # x values
    x=center[0]+r*np.sin(np.radians(phi))

    # x2 values - inner radius
    x2=center[0]+r2*np.sin(np.radians(phi))

    # y values. need to correct for negative values in range theta=90--270
    yy = np.sqrt(r**2 - x**2)
    yy = [-yy[i] if phi[i] > 90 and phi[i] < 270 else yy[i] for i in range(len(yy))]

    y = center[1] + np.array(yy)

    # y2 values. need to correct for negative values in range theta=90--270
    yy2 = np.sqrt(r2**2 - x2**2)
    yy2 = [-yy2[i] if phi[i] > 90 and phi[i] < 270 else yy2[i] for i in range(len(yy2))]

    y2 = center[1] + np.array(yy2)

    plt.fill(np.concatenate((x, np.flip(x2))), np.concatenate((y, np.flip(y2))), facecolor=[1.0,0,0,intensity/1000.0], edgecolor=[1.0,0,0,1.0])

def bumper_callback(message):
    if message.header.frame_id == "Capacitive_buttons_update":
        plt.clf()
        # plt.plot(message.capacitive_buttons)

        x = 0
        y = 0
        intensity = 0
        for i in range(6):
            th1 = -30 + 60*i
            th2 = 30 + 60*i
            filled_arc([0,0],r=12, r2=10,theta1 = th1,theta2 = th2, intensity = message.capacitive_buttons[i])
            point_x = (message.capacitive_buttons[i]/1000.0)*9.6*np.sin(np.radians((th1 + th2)/2))
            point_y = (message.capacitive_buttons[i]/1000.0)*9.6*np.cos(np.radians((th1 + th2)/2))

            plt.plot([0, point_x], [0, point_y], color=[0.0, 0.0, 0.0, 0.2])

            x += point_x
            y += point_y

        dist = np.sqrt(x**2 + y**2)
        if (dist > 12):
            x *= (12.0/dist)
            y *= (12.0/dist)
            dist = 12.0
        plt.plot([0, x], [0, y], color=[0.0, 1.0, 0.0, 1.0])

        angle = np.rad2deg(np.arctan2(x,y))
        if angle < 0:
            angle = 360.0 + angle 
        plt.text(9, -10.5, "Angle: " + "{:.1f}".format(angle) + "°")
        plt.text(9, -12, "Pressure: " + "{:.0f}".format(1000.0*(dist/12.0)))

        plt.draw()
        # plt.show()
        plt.axis("equal")
        # fg.draw()
        plt.pause(0.00000000001)

    
def control():
    rospy.init_node('cras_robot_bumper_visualisation', anonymous=True)
    rate = rospy.Rate(25)
    rospy.Subscriber("/cras_robot/robot_state", Robot_state, bumper_callback)
    plt.ion()
    plt.show()
    # rospy.spin()
    

    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        control()
    except rospy.ROSInterruptException:
        pass
