#!/usr/bin/env python3

import rospy
from nav_msgs.msg import OccupancyGrid
from visualization_msgs.msg import MarkerArray, Marker
import tf
import numpy as np
from scipy import ndimage
from cras_srobot_frontier.srv import Generate_frontier, Generate_frontierResponse
from geometry_msgs.msg import Pose2D
import copy
import skimage.measure as skm


class FrontierExplorer():
    def __init__(self):
        # Initialize the node
        rospy.init_node("frontier_explorer")

        # Get some useful parameters
        self.mapFrame = rospy.get_param("~map_frame", "map")
        self.robotFrame = rospy.get_param("~robot_frame", "base_link")
        self.robotDiameter = float(rospy.get_param("~robot_diameter", 0.33))
        self.occupancyThreshold = int(rospy.get_param("~occupancy_threshold", 90))

        # Helper variable to determine if grid was received at least once
        self.gridReady = False

        self.tflistener = tf.TransformListener()

        # Subscribe to grid
        self.gridSubscriber = rospy.Subscriber('map', OccupancyGrid, self.grid_cb)
        self.gridPublisher = rospy.Publisher("occupied_space", OccupancyGrid, queue_size=50)
        self.frontiersPublisher = rospy.Publisher("finded_frontiers", OccupancyGrid, queue_size=50)
        self.frontiersMarkersPublisher = rospy.Publisher("finded_frontiers_markers", MarkerArray, queue_size=50)
        

        # Initialization of other variables
        self.received_occupancy_grid = OccupancyGrid()
        self.send_occupancy_grid = OccupancyGrid()
        self.send_frontiers_grid = OccupancyGrid()
        self.origin_position = None
        self.origin_orientation = None
        self.grid_info = None
        self.grid_resolution = None
        self.grid_width = None
        self.grid_height = None
        self.grid_data = None
        self.inflated_grid_data = None
        self.bfs_where_to_look = [(-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1)]

        self.grid_to_map = None
    
    def computeWFD(self):
        """ Run the Wavefront detector """
        map_open_list = []
        map_close_list = []
        frontier_close_list = []
        frontiers = []

        self.getRobotCoordinates()
        if self.robotPosition == None:
            rospy.logwarn("No robot position found")
            frontiers = None
            return frontiers

        map_open_list.append(self.robotPosition)

        number_of_inflates = int(np.ceil((self.robotDiameter/2)/self.grid_resolution))
        
        inflated_unknown = ndimage.binary_dilation(self.grid_data == -1, iterations = 2)*-1
        struct = ndimage.generate_binary_structure(2, 2)
        inflated_obstacles = ndimage.binary_dilation(self.grid_data > self.occupancyThreshold, structure=struct, iterations = number_of_inflates)*100
        inflated_obstacles = ndimage.binary_dilation(inflated_obstacles > self.occupancyThreshold, iterations = 2)*100

        self.inflated_grid_data = np.zeros_like(self.grid_data)
        self.inflated_grid_data = inflated_unknown + inflated_obstacles
        self.inflated_grid_data[self.inflated_grid_data>=100] = 100

        robot_inflate = np.zeros_like(self.inflated_grid_data)
        robot_inflate[self.robotPosition[0], self.robotPosition[1]] = 1
        robot_inflate = ndimage.binary_dilation(robot_inflate, iterations = number_of_inflates+1)
        self.inflated_grid_data[robot_inflate >= 0.5] = 0

        self.send_occupancy_grid = copy.deepcopy(self.received_occupancy_grid)
        self.send_occupancy_grid.data = self.inflated_grid_data.flatten()
        self.gridPublisher.publish(self.send_occupancy_grid)
    
        self.inflated_grid_data[(self.inflated_grid_data>self.occupancyThreshold)] = 100.0
        self.inflated_grid_data[(self.inflated_grid_data==-1)] = 20.0

        mask = np.array([[-1,-1,-1],[-1,10,-1],[-1,-1,-1]])
        # print(mask)
        data_c = ndimage.convolve(self.inflated_grid_data, mask, mode='constant', cval=100.0)

        finded_frontiers = np.zeros_like(data_c)
        finded_frontiers[(data_c > 50) & (data_c < 150)] = 1

        labeled_image, num_labels = skm.label(finded_frontiers, connectivity=2, return_num=True)

        for i in range(num_labels):
            point = np.mean(np.where(labeled_image == i+1), axis=1)
            frontiers.append([int(point[0]), int(point[1])])

        return frontiers
    
    def get_distances(self, points):
        if points == None:
            return None

        distances = []

        for point in points:
            vector = np.array([point[0]-self.robotPosition[0], point[1]-self.robotPosition[1]])
            distance = np.linalg.norm(vector)
            distances.append(distance)

        return distances
    
    def get_reward(self, points, radius = 15):
        if points == None:
            return None
        rewards = []
        reward_gridmap_data = copy.deepcopy(self.inflated_grid_data)

        reward_gridmap_data[reward_gridmap_data > self.occupancyThreshold] = -5.0
        reward_gridmap_data[reward_gridmap_data < 20] = -0.5
        reward_gridmap_data[reward_gridmap_data == 20] = 5.0

        X, Y =  np.ogrid[:self.grid_width, :self.grid_height]

        for point in points:
            new_point = copy.deepcopy(point)
            new_point[0] += self.grid_width*self.grid_resolution
            new_point[1] += self.grid_height*self.grid_resolution
            x, y = self.pointToMap(new_point)
            dist_from_point = np.sqrt((X-x)**2 + (Y-y)**2)
        
            reward = reward_gridmap_data[dist_from_point < radius].sum()
            rewards.append(reward)
        
        sort_indexes = np.argsort(np.array(rewards))
        for i, index in enumerate(sort_indexes):
            rewards[index] = (i+5)*1.5
        
        return rewards
    
    def pointToMap(self, point):
        grid_coords = np.hstack((np.array([point[1], point[0]]) * self.grid_resolution, np.array([0, 1]))).T
        map_coords = self.grid_to_map @ grid_coords
        return map_coords[0], map_coords[1]
    
    def getHighestRewardFrontier(self, request):
        """ Return highest reward frontier """
        frontiers = self.computeWFD()

        if not frontiers:
            return Generate_frontierResponse(Pose2D(0, 0, 0.0))

        distances = self.get_distances(frontiers)
        rewards = self.get_reward(frontiers)
        values = (10*np.array(rewards))/(np.array(distances)/5.0+0.5)
        max_value = np.max(values)

        highest_reward_frontier_idx = np.argmax(values)

        markers = MarkerArray()

        delete_marker = Marker()
        delete_marker.header.stamp = rospy.Time.now()
        delete_marker.header.frame_id = "map"
        delete_marker.id = 0
        delete_marker.action = Marker.DELETEALL
        markers.markers.append(delete_marker)

        for id, (frontier, frontier_value) in enumerate(zip(frontiers, values)):
            x, y = self.pointToMap(frontier)
            frontier_marker = Marker()
            frontier_marker.header.stamp = rospy.Time.now()
            frontier_marker.header.frame_id = "map"
            frontier_marker.id = id+1
            frontier_marker.type = Marker.SPHERE
            frontier_marker.action = Marker.ADD
            frontier_marker.pose.position.x = x
            frontier_marker.pose.position.y = y
            frontier_marker.pose.orientation.w = 1.0
            frontier_marker.scale.x = (frontier_value/max_value) * 0.4
            frontier_marker.scale.y = (frontier_value/max_value) * 0.4
            frontier_marker.scale.z = (frontier_value/max_value) * 0.8
            frontier_marker.color.a = 1.0
            if id == highest_reward_frontier_idx:
                 frontier_marker.color.b = 1.0
            else:
                frontier_marker.color.g = (1.0 - frontier_value/max_value)
                frontier_marker.color.r = frontier_value/max_value
            markers.markers.append(frontier_marker)

        self.frontiersMarkersPublisher.publish(markers)

        selected_frontier = frontiers[highest_reward_frontier_idx]

        self.publishFrontiers(frontiers, selected_frontier)

        x, y = self.pointToMap(selected_frontier)
        print("Position of the frontier is [{}, {}] -> [{}, {}]".format(selected_frontier[1], selected_frontier[0], x,y))
        response = Generate_frontierResponse(Pose2D(x, y, 0.0))
        return response

        

    def getRandomFrontier(self, request):
        """ Return random frontier """
        frontiers = self.computeWFD()

        if not frontiers:
            return Generate_frontierResponse(Pose2D(0, 0, 0.0))

        random_frontier_idx = np.random.choice(len(frontiers))
        print(random_frontier_idx)
        selected_frontier = frontiers[random_frontier_idx]

        self.showSelectedFrontier(frontiers, random_frontier_idx)

        self.publishFrontiers(frontiers, selected_frontier)

        grid_coords = np.hstack((np.array([selected_frontier[1], selected_frontier[0]]) * self.grid_resolution, np.array([0, 1]))).T
        map_coords = self.grid_to_map @ grid_coords

        x, y = map_coords[0], map_coords[1]

        print("Position of the frontier is [{}, {}] -> [{}, {}]".format(selected_frontier[1], selected_frontier[0], x,y))
        response = Generate_frontierResponse(Pose2D(x, y, 0.0))
        return response
    
    def getClosestFrontier(self, request):
        """ Return frontier closest to the robot """
        frontiers = self.computeWFD()

        if not frontiers:
            return Generate_frontierResponse(Pose2D(0, 0, 0.0))

        index = 0
        min = np.sqrt(self.grid_height**2 + self.grid_width**2)
        for idx in range(len(frontiers)):
            minimum = np.sqrt((frontiers[idx][0]-self.robotPosition[0])**2 + (frontiers[idx][1]-self.robotPosition[1])**2)
            if minimum < min:
                min = minimum
                index = idx
        bestFrontierIdx = index  # Compute the index of the best frontier
        selected_frontier = frontiers[bestFrontierIdx]

        self.showSelectedFrontier(frontiers, bestFrontierIdx)
        self.publishFrontiers(frontiers, selected_frontier)
        
        grid_coords = np.hstack((np.array([selected_frontier[1], selected_frontier[0]]) * self.grid_resolution, np.array([0, 1]))).T
        map_coords = self.grid_to_map @ grid_coords

        x, y = map_coords[0], map_coords[1] 

        print("Position of the frontier is [{}, {}] -> [{}, {}]".format(selected_frontier[1], selected_frontier[0], x,y))

        response = Generate_frontierResponse(Pose2D(x, y, 0.0))
        return response
    

    def showSelectedFrontier(self, frontiers, frontier_index):
        markers = MarkerArray()

        delete_marker = Marker()
        delete_marker.header.stamp = rospy.Time.now()
        delete_marker.header.frame_id = "map"
        delete_marker.id = 0
        delete_marker.action = Marker.DELETEALL
        markers.markers.append(delete_marker)

        for id, frontier in enumerate(frontiers):
            grid_coords = np.hstack((np.array([frontier[1], frontier[0]]) * self.grid_resolution, np.array([0, 1]))).T
            map_coords = self.grid_to_map @ grid_coords
            x, y = map_coords[0], map_coords[1] 
            frontier_marker = Marker()
            frontier_marker.header.stamp = rospy.Time.now()
            frontier_marker.header.frame_id = "map"
            frontier_marker.id = id+1
            frontier_marker.type = Marker.SPHERE
            frontier_marker.action = Marker.ADD
            frontier_marker.pose.position.x = x
            frontier_marker.pose.position.y = y
            frontier_marker.pose.orientation.w = 1.0
            frontier_marker.color.a = 1.0

            if id == frontier_index:
                frontier_marker.scale.x = 0.3
                frontier_marker.scale.y = 0.3
                frontier_marker.scale.z = 0.6
                frontier_marker.color.b = 1.0 
            else:
                frontier_marker.scale.x = 0.1
                frontier_marker.scale.y = 0.1
                frontier_marker.scale.z = 0.2
                frontier_marker.color.g = 1.0 


            markers.markers.append(frontier_marker)
        
        self.frontiersMarkersPublisher.publish(markers)
    
    def publishFrontiers(self, frontiers, the_frontier):
        self.send_frontiers_grid = self.received_occupancy_grid
        frontiers_data = np.zeros_like(self.inflated_grid_data)

        print("frontiers: ", frontiers)

        indexes = np.array(frontiers)       
        frontiers_data[indexes[:,0], indexes[:,1]] = -80.0
    
        frontiers_data[the_frontier[0], the_frontier[1]] = 127.0

        self.send_frontiers_grid.data = frontiers_data.flatten()
        self.frontiersPublisher.publish(self.send_frontiers_grid)

    def getRobotCoordinates(self):
        """ Get the current robot position in the grid """
        try:
            (robot_position, robot_rotation) = self.tflistener.lookupTransform(self.mapFrame, self.robotFrame, rospy.Time(0))

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            rospy.logwarn("Cannot get the robot position!")
            self.robotPosition = None
        else:

            robot_to_map_T = tf.transformations.quaternion_matrix(robot_rotation)
            robot_to_map_T[:3,3] = robot_position

            coords =  np.linalg.inv(self.grid_to_map) @ robot_to_map_T @ (np.array([0, 0, 0, 1]).T)
            coords = np.floor(coords[:2] / self.grid_resolution)
            self.robotPosition = [int(coords[1]), int(coords[0])]

            print("Position of the robot is [{}, {}] -> [{}, {}]".format(int(coords[1]), int(coords[0]), robot_position[0], robot_position[1]))

    def out_of_bounds(self, x, y):
        if x < 0 or x >= self.grid_height:
            return True
        if y < 0 or y >= self.grid_width:
            return True
        return False
    
    def extractGrid(self, msg):
        # Extract grid from msg.data and other usefull information
        self.received_occupancy_grid = msg
        self.origin_position =  msg.info.origin.position
        self.origin_orientation =  msg.info.origin.orientation
        self.grid_info = msg.info
        self.grid_width = msg.info.width
        self.grid_height = msg.info.height
        self.grid_resolution = msg.info.resolution
        self.grid_data = np.array(msg.data).reshape((self.grid_height, self.grid_width))

        self.grid_to_map = tf.transformations.quaternion_matrix([
            self.origin_orientation.x, self.origin_orientation.y, self.origin_orientation.z, self.origin_orientation.w
        ])
        self.grid_to_map[0,3] = self.origin_position.x
        self.grid_to_map[1,3] = self.origin_position.y
        self.grid_to_map[2,3] = self.origin_position.z

    def grid_cb(self, msg):
        self.extractGrid(msg)
        
        if not self.gridReady:

            # Create services
            self.grf_service = rospy.Service('get_random_frontier', Generate_frontier, self.getRandomFrontier)
            self.gcf_service = rospy.Service('get_closest_frontier', Generate_frontier, self.getClosestFrontier)
            self.gcf_service = rospy.Service('get_highest_reward_frontier', Generate_frontier, self.getHighestRewardFrontier)
            self.gridReady = True


if __name__ == "__main__":
    fe = FrontierExplorer()
    rospy.spin()
