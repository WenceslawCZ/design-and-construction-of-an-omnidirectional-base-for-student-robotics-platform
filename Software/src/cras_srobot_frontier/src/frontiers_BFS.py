#!/usr/bin/env python3

import rospy
from nav_msgs.msg import OccupancyGrid
import tf
import numpy as np
from scipy import ndimage
from cras_srobot_frontier.srv import Generate_frontier, Generate_frontierResponse
from geometry_msgs.msg import Pose2D


class FrontierExplorer():
    def __init__(self):
        # Initialize the node
        rospy.init_node("frontier_explorer")

        # Get some useful parameters
        self.mapFrame = rospy.get_param("~map_frame", "map")
        self.robotFrame = rospy.get_param("~robot_frame", "base_link")
        self.robotDiameter = float(rospy.get_param("~robot_diameter", 0.5))
        self.occupancyThreshold = int(rospy.get_param("~occupancy_threshold", 90))

        # Helper variable to determine if grid was received at least once
        self.gridReady = False

        self.tflistener = tf.TransformListener()

        # Subscribe to grid
        self.gridSubscriber = rospy.Subscriber('map', OccupancyGrid, self.grid_cb)
        self.gridPublisher = rospy.Publisher("occupied_space", OccupancyGrid, queue_size=50)
        self.frontiersPublisher = rospy.Publisher("finded_frontiers", OccupancyGrid, queue_size=50)
        

        # Initialization of other variables
        self.received_occupancy_grid = OccupancyGrid()
        self.send_occupancy_grid = OccupancyGrid()
        self.send_frontiers_grid = OccupancyGrid()
        self.origin_position = None
        self.origin_orientation = None
        self.grid_info = None
        self.grid_resolution = None
        self.grid_width = None
        self.grid_height = None
        self.grid_data = None
        self.inflated_grid_data = None
        self.bfs_where_to_look = [(-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1)]

        self.grid_to_map = None
    
    def computeWFD(self):
        """ Run the Wavefront detector """
        map_open_list = []
        map_close_list = []
        frontier_close_list = []
        frontiers = []

        self.getRobotCoordinates()
        if self.robotPosition == None:
            rospy.logwarn("No robot position found")
            frontiers = None
            return frontiers

        map_open_list.append(self.robotPosition)

        
        
        number_of_inflates = int(np.ceil((self.robotDiameter/2)/self.grid_resolution))
        
        inflated_unknown = ndimage.binary_dilation(self.grid_data == -1, iterations = number_of_inflates)*-1
        inflated_obstacles = ndimage.binary_dilation(self.grid_data > self.occupancyThreshold, iterations = number_of_inflates)*100

        self.inflated_grid_data = inflated_unknown + inflated_obstacles

        robot_inflate = np.zeros_like(self.inflated_grid_data)
        robot_inflate[self.robotPosition[1], self.robotPosition[0]] = 1
        robot_inflate = ndimage.binary_dilation(robot_inflate, iterations = number_of_inflates)
        self.inflated_grid_data[robot_inflate >= 0.5] = 0

        self.send_occupancy_grid = self.received_occupancy_grid
        self.send_occupancy_grid.data = self.inflated_grid_data.flatten()
        self.gridPublisher.publish(self.send_occupancy_grid)

        while map_open_list:
            x, y = map_open_list.pop(0)
            map_close_list.append([x,y])
            for index, (searched_x,searched_y) in enumerate(self.bfs_where_to_look):
                next_x = x + searched_x
                next_y = y + searched_y
                if self.out_of_bounds(next_x, next_y):
                    continue
                if self.inflated_grid_data[next_x, next_y] == -1:
                    #this is frontier
                    if [x, y] not in frontiers:
                        frontiers.append([x, y])

                elif self.inflated_grid_data[next_x, next_y] < self.occupancyThreshold:
                    #[next_x, next_y] is an empty cell
                    if [next_x, next_y] not in map_close_list and [next_x, next_y] not in map_open_list and [next_x, next_y] not in frontiers and [next_x, next_y] not in frontier_close_list:
                        map_open_list.append([next_x, next_y])
                else:
                    #[next_x, next_y] is an occupied cell
                    if [next_x, next_y] not in map_close_list:
                        map_close_list.append([next_x,next_y])


        # self.showGrid(self.inflated_grid_data, robot_position=self.robotPosition, frontiers=frontiers)

        return frontiers
    
    def showGrid(self, grid_data, robot_position = None, frontiers = None, frontierCenter = None):
        line = " "
        for i in range(self.grid_width+4): line += "-"
        print(line)
        for x in range(self.grid_height):
            line = " | "
            for y in range(self.grid_width):
                if robot_position != None:
                    if [x, y] == robot_position:
                        line = line + '\033[32m' + "O" + '\033[0m'
                        continue
                if frontierCenter != None:
                    if [x, y] in frontierCenter or [x, y] == frontierCenter :
                        line = line + '\033[32m' + "*" + '\033[0m'
                        continue
                if frontiers != None:
                    if [x, y] in frontiers:
                        line = line + '\033[31m' + "X" + '\033[0m'
                        continue
                if grid_data[x,y] == 0:
                    line = line + " "
                elif grid_data[x,y] == -1:
                    line = line + "\\"
                elif grid_data[x,y] > self.occupancyThreshold:
                    line = line + '\033[93m' + "#" + '\033[0m'
                else:
                    line = line + "-"
            line += " | "
            print(line)
        line = " "
        for i in range(self.grid_width+4): line += "-"
        print(line)

    def getRandomFrontier(self, request):
        """ Return random frontier """
        frontiers = self.computeWFD()

        if not frontiers:
            return Generate_frontierResponse(Pose2D(0, 0, 0.0))

        frontier = frontiers[np.random.choice(len(frontiers))]

        frontierCenter = self.get_center_frontier(frontier, frontiers)  # Compute center of the randomly drawn frontier here

        grid_coords = np.hstack((np.array([frontierCenter[1], frontierCenter[0]]) * self.grid_resolution, np.array([0, 1]))).T
        map_coords = self.grid_to_map @ grid_coords

        x, y = map_coords[0], map_coords[1]


        print("Position of the frontier is [{}, {}] -> [{}, {}]".format(frontierCenter[1], frontierCenter[0], x,y))
        response = Generate_frontierResponse(Pose2D(x, y, 0.0))
        return response
    
    def getClosestFrontier(self, request):
        """ Return frontier closest to the robot """
        frontiers = self.computeWFD()

        if not frontiers:
            return Generate_frontierResponse(Pose2D(0, 0, 0.0))

        index = 0
        min = np.sqrt(self.grid_height**2 + self.grid_width**2)
        for idx in range(len(frontiers)):
            minimum = np.sqrt((frontiers[idx][0]-self.robotPosition[0])**2 + (frontiers[idx][1]-self.robotPosition[1])**2)
            if minimum < min:
                min = minimum
                index = idx
        bestFrontierIdx = index  # Compute the index of the best frontier
        frontier = frontiers[bestFrontierIdx]

        frontierCenter = self.get_center_frontier(frontier, frontiers)  # Compute the center of the chosen frontier

        # self.showGrid(self.grid_data, robot_position=self.robotPosition, frontiers=frontiers, frontierCenter=frontierCenter)
        
        grid_coords = np.hstack((np.array([frontierCenter[1], frontierCenter[0]]) * self.grid_resolution, np.array([0, 1]))).T
        map_coords = self.grid_to_map @ grid_coords

        x, y = map_coords[0], map_coords[1] 

        print("Position of the frontier is [{}, {}] -> [{}, {}]".format(frontierCenter[1], frontierCenter[0], x,y))

        response = Generate_frontierResponse(Pose2D(x, y, 0.0))
        return response
    
    def get_center_frontier(self, frontier_to_center, frontiers):
        finded_frontiers = []
        frontier_open_list = []
        frontier_open_list.append(frontier_to_center)
        while frontier_open_list:
            x, y = frontier_open_list.pop(0)
            finded_frontiers.append([x,y])
            for index, (searched_x,searched_y) in enumerate(self.bfs_where_to_look):
                next_x = x + searched_x
                next_y = y + searched_y
                if self.out_of_bounds(next_x, next_y):
                    continue
                if [next_x, next_y] in frontiers and [next_x, next_y] not in finded_frontiers and [next_x, next_y] not in frontier_open_list:
                    frontier_open_list.append([next_x, next_y])

        values = abs(finded_frontiers - np.mean(finded_frontiers, axis=0))
        min = values.min(axis = 0)

        index = 0
        for idx in range(len(finded_frontiers)):
            if min[0] == values[idx,0] and min[1] == values[idx,1]:
                index = idx

        self.publishFrontiers(frontiers, finded_frontiers, finded_frontiers[index])

        return finded_frontiers[index]
    
    def publishFrontiers(self, frontiers, selected_frontiers, the_frontier):
        self.send_frontiers_grid = self.received_occupancy_grid
        frontiers_data = np.zeros_like(self.inflated_grid_data)
        

        print("frontiers: ", frontiers)
        print("selected_frontier: ", selected_frontiers)

        indexes = np.array(frontiers)       
        frontiers_data[indexes[:,0], indexes[:,1]] = -80.0

        indexes = np.array(selected_frontiers)       
        frontiers_data[indexes[:,0], indexes[:,1]] = 127.0
    
        frontiers_data[the_frontier[0], the_frontier[1]] = -127.0

        self.send_frontiers_grid.data = frontiers_data.flatten()
        self.frontiersPublisher.publish(self.send_frontiers_grid)

    def getRobotCoordinates(self):
        """ Get the current robot position in the grid """
        try:
            (robot_position, robot_rotation) = self.tflistener.lookupTransform(self.mapFrame, self.robotFrame, rospy.Time(0))

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            rospy.logwarn("Cannot get the robot position!")
            self.robotPosition = None
        else:

            robot_to_map_T = tf.transformations.quaternion_matrix(robot_rotation)
            robot_to_map_T[:3,3] = robot_position

            coords =  np.linalg.inv(self.grid_to_map) @ robot_to_map_T @ (np.array([0, 0, 0, 1]).T)
            coords = np.floor(coords[:2] / self.grid_resolution)
            self.robotPosition = [int(coords[1]), int(coords[0])]

            print("Position of the robot is [{}, {}] -> [{}, {}]".format(int(coords[1]), int(coords[0]), robot_position[0], robot_position[1]))

    def out_of_bounds(self, x, y):
        if x < 0 or x >= self.grid_height:
            return True
        if y < 0 or y >= self.grid_width:
            return True
        return False
    
    def extractGrid(self, msg):
        # Extract grid from msg.data and other usefull information
        self.received_occupancy_grid = msg
        self.origin_position =  msg.info.origin.position
        self.origin_orientation =  msg.info.origin.orientation
        self.grid_info = msg.info
        self.grid_width = msg.info.width
        self.grid_height = msg.info.height
        self.grid_resolution = msg.info.resolution
        self.grid_data = np.array(msg.data).reshape((self.grid_height, self.grid_width))

        self.grid_to_map = tf.transformations.quaternion_matrix([
            self.origin_orientation.x, self.origin_orientation.y, self.origin_orientation.z, self.origin_orientation.w
        ])
        self.grid_to_map[0,3] = self.origin_position.x
        self.grid_to_map[1,3] = self.origin_position.y
        self.grid_to_map[2,3] = self.origin_position.z

    def grid_cb(self, msg):
        self.extractGrid(msg)
        
        if not self.gridReady:

            # Create services
            self.grf_service = rospy.Service('get_random_frontier', Generate_frontier, self.getRandomFrontier)
            self.gcf_service = rospy.Service('get_closest_frontier', Generate_frontier, self.getClosestFrontier)
            self.gridReady = True


if __name__ == "__main__":
    fe = FrontierExplorer()
    rospy.spin()
