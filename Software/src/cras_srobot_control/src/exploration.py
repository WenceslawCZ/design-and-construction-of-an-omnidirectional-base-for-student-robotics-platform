#!/usr/bin/env python3

import rospy
import tf
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, PoseStamped, Vector3, Pose2D
from move_base_msgs.msg import MoveBaseActionGoal
from cras_srobot_driver.msg import Robot_state, LEDs
from std_msgs.msg import Float64, Float64MultiArray
from sensor_msgs.msg import Joy, JoyFeedback, JoyFeedbackArray
from visualization_msgs.msg import MarkerArray, Marker

from cras_srobot_frontier.srv import Generate_frontier
from cras_srobot_exploration.srv import Generate_path


def new_frontiers_callback(message):
    print(message)

rospy.init_node("exploration_publisher")
goal_publisher = rospy.Publisher("/cras_robot/move/goal", PoseStamped, queue_size=50)

rospy.Subscriber('/cras_robot/finded_frontiers_markers', MarkerArray, new_frontiers_callback)


listener = tf.TransformListener()

goal = PoseStamped()
goal.header.frame_id = "goal"
goal.pose.orientation.w = 1.0

selected_frontier = Pose2D()
new_frontier = Pose2D()
selected_point = Pose2D()

allFrontiers = []
looking_for_frontier = True

current_time = rospy.Time.now()
last_time = rospy.Time.now()
r = rospy.Rate(10.0)

print("Waiting for services")
rospy.wait_for_service('/cras_robot/get_closest_frontier')
rospy.wait_for_service('/cras_robot/get_highest_reward_frontier')
rospy.wait_for_service('/cras_robot/get_random_frontier')
# rospy.wait_for_service('/cras_robot/generate_path')
print("Services loaded")

highest_reward_frontier = rospy.ServiceProxy('/cras_robot/get_highest_reward_frontier', Generate_frontier)
random_frontier = rospy.ServiceProxy('/cras_robot/get_random_frontier', Generate_frontier)
generate_path = rospy.ServiceProxy('/cras_robot/generate_path', Generate_path)

while not rospy.is_shutdown():
    current_time = rospy.Time.now()

    # if looking_for_frontier:
    #     print("Looking for frontiers")
        
    #     selected_frontier = highest_reward_frontier().frontier_pose

    #     if selected_frontier.x == 0 and selected_frontier.y == 0:
    #         continue
    #     print(selected_frontier)

    #     path = generate_path(selected_frontier).path
    #     print(path)

    #     if len(path.poses) > 1:
    #         selected_point.x = path.poses[1].pose.position.x
    #         selected_point.y = path.poses[1].pose.position.y
    #         looking_for_frontier = False
    # else:
    #     if (current_time - last_time).to_sec() > 2.5:
    #         last_time = current_time
    #         allFrontiers = []
    #         new_frontier = highest_reward_frontier().frontier_pose
    #     else:
    #         if len(allFrontiers) > 0:
    #             # Check if the selected frontier exists, if not, select a new one


    if looking_for_frontier:
        print("Looking for frontiers")
        
        selected_frontier = highest_reward_frontier().frontier_pose

        if selected_frontier.x == 0 and selected_frontier.y == 0:
            continue
        print(selected_frontier)

        path = generate_path(selected_frontier).path
        print(path)

        if len(path.poses) > 1:
            goal.pose.position.x = path.poses[1].pose.position.x
            goal.pose.position.y = path.poses[1].pose.position.y
            goal_publisher.publish(goal)
            looking_for_frontier = False
        last_time = current_time
    else:
        if (current_time - last_time).to_sec() > 2.5:
            looking_for_frontier = True
     
    r.sleep()