#!/usr/bin/env python3

import rospy
import tf
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, PoseStamped, Vector3
from move_base_msgs.msg import MoveBaseActionGoal
from cras_srobot_driver.msg import Robot_state
from std_msgs.msg import Float64MultiArray

def goal_pose_callback(message):
    # print("GOAL_RECIEVED:", message)
    global goal_x, goal_y, goal_th, goal_received
    goal_x = message.pose.position.x
    goal_y = message.pose.position.y
    goal_th = tf.transformations.euler_from_quaternion([message.pose.orientation.x, message.pose.orientation.y, message.pose.orientation.z, message.pose.orientation.w])[2]
    goal_received = True

def wheels_velocity_callback(message):
    global real_wheels_velocity
    if message.header.frame_id == "Motor_velocity_update":
        real_wheels_velocity = message.motor_velocity


rospy.init_node("odometry_publisher_real_srobot")
odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
wheels_velocity_pub = rospy.Publisher("control/wheels_velocity_controller/command", Float64MultiArray, queue_size=50)

# rospy.Subscriber("/move_base/goal", MoveBaseActionGoal, goal_pose_callback)
rospy.Subscriber("/move_base_simple/goal", PoseStamped, goal_pose_callback)
rospy.Subscriber("/cras_robot/move/goal", PoseStamped, goal_pose_callback)
rospy.Subscriber('/cras_robot/robot_state', Robot_state, wheels_velocity_callback)  
odom_broadcaster = tf.TransformBroadcaster()

odom = Odometry()
odom.header.frame_id = "odom"
odom.child_frame_id = "base_link"

odom_x = 0.0
odom_y = 0.0
odom_th = 0.0

listener = tf.TransformListener()

v = 0.3
vx = 0.0
vy = 0.0
alpha = 0.0
omega = 0.0

distance = 0.0
time_to_goal = 0.0


goal_x = 0.0
goal_y = 0.0
goal_th = 0.0
goal_received = False

x_error = 0.0
y_error = 0.0
th_error = 0.0

IK = np.zeros((3, 3))
IK[0,0] = -np.sin(np.deg2rad(60))
IK[1,0] = -np.sin(np.deg2rad(300))
IK[2,0] = -np.sin(np.deg2rad(180))

IK[0,1] = -np.cos(np.deg2rad(60))
IK[1,1] = -np.cos(np.deg2rad(300))
IK[2,1] = -np.cos(np.deg2rad(180))

IK[:,2] = 0.108

real_wheels_velocity = Vector3()
wheels_velocity = Float64MultiArray()

current_time = rospy.Time.now()
last_time = rospy.Time.now()
r = rospy.Rate(10.0)
while not rospy.is_shutdown():
    current_time = rospy.Time.now()

    # compute odometry in a typical way given the velocities of the robot
    dt = (current_time - last_time).to_sec()
    delta_x = (vx * np.cos(odom_th) - vy * np.sin(odom_th)) * dt
    delta_y = (vx * np.sin(odom_th) + vy * np.cos(odom_th)) * dt
    delta_th = omega * dt

    odom_x += delta_x
    odom_y -= delta_y
    odom_th += delta_th


    odom_quaternion = tf.transformations.quaternion_from_euler(0, 0, odom_th)

    odom_broadcaster.sendTransform(
        [odom_x, odom_y, 0.0],
        odom_quaternion,
        current_time,
        "base_link",
        "odom"
    )

    odom.pose.pose = Pose(Point(odom_x, odom_y, 0.), Quaternion(*odom_quaternion))
    odom.header.stamp = current_time
    

    x_error = 0
    y_error = 0
    th_error = 0
    alpha = 0
    # get the position in map, hopefully
    try:
        if goal_received:
            (rob_pos, rob_rot) = listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            print("POSITION", rob_pos, rob_rot)
            print("GOAL", goal_x, goal_y, goal_th)
            x_error = goal_x - rob_pos[0]
            y_error = goal_y - rob_pos[1]
            rob_rotation_z = tf.transformations.euler_from_quaternion(rob_rot)[2]
            th_error = goal_th - rob_rotation_z
            alpha = np.arctan2(x_error, -y_error) - rob_rotation_z
    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
        pass
    
    # x_error = goal_x - odom_x
    # y_error = goal_y - odom_y
    # alpha = np.arctan2(x_error, -y_error)
    distance = np.sqrt(x_error*x_error + y_error*y_error)

    time_to_goal = distance / v
    # th_error = goal_th - odom_th

    # choose the shorter rotation
    if th_error > np.pi:
        th_error -= np.pi
    if th_error < -np.pi:
        th_error += np.pi

    if time_to_goal <= 0.1: 
        omega = th_error
    else:
        omega = th_error / time_to_goal

    if distance <= 0.1:
        vx = 0
        vy = 0
        omega = 0
        goal_received = False

    elif distance <= v:
        vx = distance * np.sin(alpha)
        vy = distance * np.cos(alpha)
    else:
        vx = v * np.sin(alpha)
        vy = v * np.cos(alpha)
    
    # omega = 0.0

    motor_linear_velocity = np.dot(IK, np.array([vx, vy, omega]))

    print(motor_linear_velocity)
    print(motor_linear_velocity/0.041)

    wheels_velocity.data = motor_linear_velocity/0.041

    print(alpha)
    print(distance)
    print("Errors: ", x_error, y_error, th_error)
    print("Omega: ", omega)
    # print(motor_linear_velocity)
    # print(motor_angular_velocity)

    print(wheels_velocity)
    # print(goal_th)

    odom.twist.twist.linear.x = vx
    odom.twist.twist.linear.y = vy
    odom.twist.twist.angular.y = omega

    odom_pub.publish(odom)
    wheels_velocity_pub.publish(wheels_velocity)
    last_time = current_time
    r.sleep()