#!/usr/bin/env python3

import rospy
import tf
from nav_msgs.msg import Odometry
from gazebo_msgs.srv import GetModelState, GetModelStateRequest

rospy.init_node("odometry_publisher_simulation")
odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
odom_broadcaster = tf.TransformBroadcaster()

rospy.wait_for_service("/gazebo/get_model_state")
get_model_srv = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

odom = Odometry()
odom.header.frame_id = "odom"

model = GetModelStateRequest()
model.model_name = "cras_srobot_bringup"

r = rospy.Rate(100.0)

while not rospy.is_shutdown():
    result = get_model_srv(model)

    current_time = rospy.Time.now()

    odom_broadcaster.sendTransform(
        [result.pose.position.x, result.pose.position.y, result.pose.position.z],
        [result.pose.orientation.x, result.pose.orientation.y, result.pose.orientation.z, result.pose.orientation.w],
        current_time,
        "base_link",
        "odom"
    )

    odom.pose.pose = result.pose
    odom.twist.twist = result.twist

    odom.header.stamp = current_time
    odom_pub.publish(odom)

    r.sleep()